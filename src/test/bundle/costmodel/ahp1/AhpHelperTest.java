/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ahp1;

import junit.framework.TestCase;

/**
 *
 * @author kutila2
 */
public class AhpHelperTest extends TestCase {
    
    public AhpHelperTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of insertionSort method, of class AhpHelper.
     */
    public void testSortDescending() {
        System.out.println("testSortDescending()");
        Comparable[] a = {5, 3, 6, 2, 9, 18};
        Object[] b = {"five", "three", "six", "two", "nine", "eighteen"};

        AhpHelper.sortDescending(a, b);

        for (int i = 0; i < a.length; i++) {
//            System.out.println(b[i] + " " + a[i]);
            if (i < a.length - 1) {
                assertTrue(a[i].compareTo(a[i+1]) > 0);
            }
        }
        
        a = new Comparable[]{5.00f, 3.894f, 3.8939f, 2.00009f, 9f, 2.0001f};
        b = new Object[]{"5f", "3.894f", "3.8939f", "2.00009f", "9f", "2.0001f"};

        AhpHelper.sortDescending(a, b);
        for (int i = 0; i < a.length; i++) {
//            System.out.println(b[i] + " " + a[i]);
            if (i < a.length - 1) {
                assertTrue(a[i].compareTo(a[i+1]) > 0);
            }
        }
    }

}
