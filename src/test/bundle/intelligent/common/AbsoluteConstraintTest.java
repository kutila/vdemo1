/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import bundle.intelligent.common.AbsoluteConstraint;
import bundle.intelligent.common.CostValue.CRITERION;
import bundle.intelligent.common.ConstraintIF.TYPE;
import junit.framework.TestCase;

/**
 *
 * @author kutila2
 */
public class AbsoluteConstraintTest extends TestCase {
    
    public AbsoluteConstraintTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }


    /**
     * Test of satisfy method, of class AbsoluteConstraint.
     */
    public void testSatisfy() {
        System.out.println("testSatisfy()");
        
        AbsoluteConstraint constraint = new AbsoluteConstraint();
        constraint.setMin(new CostValue(CRITERION.LOAD, 40));
        constraint.setMax(new CostValue(CRITERION.LOAD, 80));
        constraint.setType(TYPE.ABSOLUTE);
        constraint.setCriterion1(CRITERION.LOAD);

        CostValue sampleValue = new CostValue(CRITERION.LOAD, 50);
        assertEquals(true, constraint.satisfy(sampleValue));

        sampleValue.setValue(10);
        assertEquals(false, constraint.satisfy(sampleValue));

        sampleValue.setValue(-90);
        assertEquals(false, constraint.satisfy(sampleValue));

        sampleValue.setValue(4561);
        assertEquals(false, constraint.satisfy(sampleValue));

        sampleValue.setValue(40);
        assertEquals(true, constraint.satisfy(sampleValue));
        sampleValue.setValue(80);
        assertEquals(true, constraint.satisfy(sampleValue));

        sampleValue.setValue(89743483943849238L);
        sampleValue.setCriterion(CRITERION.TIME);
        assertEquals(false, constraint.satisfy(sampleValue));
    }

}
