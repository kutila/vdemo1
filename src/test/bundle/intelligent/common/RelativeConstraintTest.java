/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import bundle.intelligent.common.ConstraintIF.TYPE;
import bundle.intelligent.common.CostValue.CRITERION;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * @author kutila2
 */
public class RelativeConstraintTest extends TestCase {
    
    public RelativeConstraintTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }


    /**
     * Test of normalize method, of class RelativeConstraint.
     */
    public void testNormalize() {
        System.out.println("testNormalize()");
        RelativeConstraint relCons = new RelativeConstraint();
        relCons.setPriority(CRITERION.TIME, 6f);
        relCons.setPriority(CRITERION.LOAD, 4f);
        relCons.setPriority(CRITERION.ACCURACY, 3f);
        relCons.setPriority(CRITERION.CPU, 2f);
        relCons.setPriority(CRITERION.HEAP, 1f);

        relCons.normalize();
        float total = relCons.getPriority(CRITERION.TIME) + relCons.getPriority(CRITERION.LOAD) +
                relCons.getPriority(CRITERION.ACCURACY) + relCons.getPriority(CRITERION.CPU) +
                relCons.getPriority(CRITERION.HEAP);

//        System.out.println("TIME " + relCons.getPriority(CRITERION.TIME));
//        System.out.println("LOAD " + relCons.getPriority(CRITERION.LOAD));
//        System.out.println("ACCURACY " + relCons.getPriority(CRITERION.ACCURACY));
//        System.out.println("CPU " + relCons.getPriority(CRITERION.CPU));
//        System.out.println("HEAP " + relCons.getPriority(CRITERION.HEAP));

        assertTrue("total was not 1", total == 1f);
        assertTrue("LOAD priority was not 0.25", 0.25 == relCons.getPriority(CRITERION.LOAD));

        //--try a not so easy set of numbers
        relCons.reset();
        relCons.setPriority(CRITERION.TIME, 3f);
        relCons.setPriority(CRITERION.LOAD, 2f);
        relCons.setPriority(CRITERION.ACCURACY, 1f);
        relCons.normalize();
        total = relCons.getPriority(CRITERION.TIME) + relCons.getPriority(CRITERION.LOAD) +
                relCons.getPriority(CRITERION.ACCURACY);
        assertTrue("total was not 1", total == 1f);
    }

}
