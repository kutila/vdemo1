/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.requester;

import bundle.intelligent.common.Endpoint;
import bundle.intelligent.common.Message;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

/**
 *
 * @author kutila2
 * created on 31/05/2010
 */
public class StandaloneRequester {

    public static void main(String[] args) {
        Packet packet = new Packet();
        packet.setFrom("kutila");
        packet.setTo("standalone");
        packet.setContent(new Integer(5678));
        
        Vector<Endpoint> endpoints = new Vector<Endpoint>();
        endpoints.add(new Endpoint("localhost", 1230));
        
        StandaloneRequester requester = new StandaloneRequester();
        requester.sendRequestsAndGetResults(packet, endpoints);
    }


        /**
     * Sends the request Message to all the Peers and retrieves their response
     * messages. The method returns after all responses have been received or
     * the wait for responses has timed out.
     *
     * @param searchRequest
     * @param endpoints
     * @return A Vector of response Messages
     */
    public Vector<Message> sendRequestsAndGetResults(Packet searchRequest, Vector<Endpoint> endpoints) {
        //TODO
        Vector<Message> vector = new Vector<Message>();

        for (Endpoint endpoint : endpoints) {
            Socket sock = null;
            try {
                sock = new Socket(endpoint.getAddress(), endpoint.getPort());
                sock.setSoTimeout(12 * 1000); //ensure that the read() block only for 12 secs max

                //send request to the Server
                ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
                out.writeObject(searchRequest);
                out.flush();

                //read response
                ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
                Message response = (Message) in.readObject();
                response.setFrom(new Endpoint(endpoint.getAddress(), endpoint.getPort()));
                vector.add(response);
                System.out.println("#####Response = " + response.toString());

                out.close();
                in.close();
            } catch (Exception e) {
                System.out.println("ok, that didn't work " + e.toString());
                //ignore and continue searching in the next agent
            } finally {
                if (sock != null) {
                    try {
                        sock.close();
                    } catch (IOException ex) {
                    } //ignore
                }
            }
        }

        return vector;
    }

}
