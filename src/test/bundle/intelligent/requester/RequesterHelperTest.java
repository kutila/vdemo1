/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.requester;

import bundle.intelligent.common.Endpoint;
import bundle.intelligent.common.Message;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import junit.framework.TestCase;

import mma2.capability.CapabilityDetails.AGENT_PLATFORMS;
import mma2.capability.CapabilitySpec;

/**
 *
 * @author kutila2
 */
public class RequesterHelperTest extends TestCase {
    
    public RequesterHelperTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getProviders method, of class Helper.
     */
    public void testGetProviders() {
        System.out.println("testGetProviders()");
        
        String[] addressList = {"localhost:4000", "localhost:5000"};
        Vector<Endpoint> results = new RequesterHelper().getProviders(addressList);
        assertEquals(0, results.size());

        //start a test Peer
        HelperTestServer tServer1 = new HelperTestServer(4000);
        tServer1.start();

        results = new RequesterHelper().getProviders(addressList);
        assertEquals(1, results.size());

        //start another test Peer
        HelperTestServer tServer2 = new HelperTestServer(RequesterHelper.PORT_START + 7);
        tServer2.start();
        addressList = new String[]{"localhost:4000", "localhost:5000", "localhost"};

        results = new RequesterHelper().getProviders(addressList);
        assertEquals(2, results.size());

        tServer1.stopRequest();
        tServer2.stopRequest();
    }

    public void testBuildCapabilitySpecs() {
        System.out.println("testBuildCapabilitySpecs()");

        RequesterHelper instance = new RequesterHelper();
        String[] specsList = {"spec", "const;Blah:0;bla:3", "SPEC1;function1:function2;GH2_2:JADE2:JADE3_5", "SPEC2;function3;JADE2:JADE4"};

        CapabilitySpec[] specs = instance.buildCapabilitySpecs(specsList);

        // - validate results
        assertEquals(2, specs.length);
        String[] functions = (String[]) specs[0].getFunctions();
        assertEquals(2, functions.length);
        assertEquals("SPEC1", specs[0].getAlias());
        assertEquals("function1", functions[0]);
        assertEquals("function2", functions[1]);
        AGENT_PLATFORMS[] platforms = specs[0].getAgentPlatforms();
        assertEquals(3, platforms.length);
        assertEquals(AGENT_PLATFORMS.GH2_2, platforms[0]);
        assertEquals(AGENT_PLATFORMS.JADE2, platforms[1]);
        assertEquals(AGENT_PLATFORMS.JADE3_5, platforms[2]);

        functions = (String[]) specs[1].getFunctions();
        assertEquals(1, functions.length);
        assertEquals("SPEC2", specs[1].getAlias());
        assertEquals("function3", functions[0]);
        platforms = specs[1].getAgentPlatforms();
        assertEquals(2, platforms.length);
        assertEquals(AGENT_PLATFORMS.JADE2, platforms[0]);
        assertEquals(AGENT_PLATFORMS.JADE4, platforms[1]);
    }

    public void testSendRequestAndGetResponse() {
        System.out.println("testSendRequestAndGetResponse()");

        HelperTestServer testServer = new HelperTestServer(2333);
        testServer.start();
        Endpoint peer = new Endpoint("localhost", 2333);

        Message request = new Message();
        request.setType(Message.REQUEST_CAPABILITY);
        request.setContent("alpha_capability");

        RequesterHelper instance = new RequesterHelper();
        Message response = instance.sendRequestAndGetResponse(request, peer);

        assertNotNull(response);
        assertEquals(request.getType(), response.getType());
        assertEquals(request.getContent(), response.getContent());
    }

    public void a_testMessageSize() {
        System.out.println("testMessageSize()");
        Message request = new Message();
        request.setType(Message.REQUEST_CAPABILITY);
        request.setContent("alpha_capability");

        try {
        java.io.ByteArrayOutputStream bos = new java.io.ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(request);

            System.out.println("message object in bytes: " + bos.toByteArray().length);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}

class HelperTestServer extends Thread {

    private boolean stop = false;
    private int port;

    public HelperTestServer(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            ServerSocket server = new ServerSocket(port);
            while (!stop) {
//                System.out.println(this.hashCode() + " listening on " + port);
                Socket socket = server.accept();
                try {
                    ObjectInputStream is = new ObjectInputStream(socket.getInputStream());
                    ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());
                    Message in = (Message) is.readObject();

                    Message response = new Message();
                    response.setType(in.getType());
                    response.setContent(in.getContent());

                    os.writeObject(response);
                    os.flush();
//                    System.out.println(" Sent response " + response.toString());
                } catch (Exception e) {
                    //ignore
//                    System.out.println("Error while handling request " + e.toString());
                } finally {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (Exception e) {
                        }
                    }
                }

            }
        } catch (Exception ex) {
            System.out.println("Exception thrown in TestServer: " + port);
            ex.printStackTrace();
        }
//        System.out.println(this.hashCode() + " ending on " + port);

    }

    public void stopRequest() {
        stop = true;
    }
}