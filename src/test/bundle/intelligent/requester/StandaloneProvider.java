/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.requester;

import bundle.intelligent.common.Message;
import jade.util.Logger;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 *
 * @author kutila2
 * created on 30/05/2010
 */
public class StandaloneProvider {

    private int port = 1230;
    private ServerSocket svr;
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private int me = 0;


    public static void main(String[] args) {
        StandaloneProvider s = new StandaloneProvider();
        s.work();
    }

    public void work() {
        me = this.hashCode();
        try {
            ServerSocketChannel svrChannel = ServerSocketChannel.open();
            svr = svrChannel.socket();
            try {
                svr.bind(new InetSocketAddress(port));
                System.out.println(me + " Started listening on " + port);
            } catch (Exception e) {
                System.out.println(me + " Failed to start iProvider on port " + port + e.toString());
            }
            while (true) {
                SocketChannel socketChannel = svrChannel.accept();
                final Socket socket = socketChannel.socket(); //got a request
                System.out.println(me + " Serving client: " + socket.getRemoteSocketAddress().toString());

                //create a new thread to serve the request
                HelperThread helper = new HelperThread(socket);
                helper.start();
            }
        } catch (ClosedByInterruptException e) {
            //expected when the ProviderThread is to be stopped
            //System.out.println("cbie received!!!");
        } catch (Exception e) {
            System.out.println(me + " Stopping due to " + e.toString());
        } finally {
            if (svr != null && !svr.isClosed()) {
                try {
                    svr.close();
                } catch (Exception e) {
                }
            }
        }

    }
}

class HelperThread extends Thread {

    private final Socket socket;
    private int h;

    public HelperThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        h = this.hashCode();
        System.out.println(h + " HelperThread started");
        try {

            ObjectInputStream is = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());
            Object in = is.readObject();

            if (in == null || (in instanceof Message == false)) {
                System.out.println(h + " Did not receive a Message from client. :-(");
                return;
            }
            Message msg = (Message) in;
            Message response = new Message();

            if (msg.getType() == Message.REQUEST_SEARCH) {
                //search for matching capability instances
                Serializable inContent = msg.getContent();
                System.out.println(h + " Received content " + inContent);
                response.setContent(h + " This is the response");
                response.setType(Message.RESPONSE_SEARCH);

            } else if (msg.getType() == Message.REQUEST_CAPABILITY) {
                //prepare capability instance
                Object o = msg.getContent();
                if (o instanceof String) {
                    String capabilityId = (String) o;
                    response.setContent("");
                } else {
                    response.setContent("ERROR: Invalid content " + o);
                }
                response.setType(Message.RESPONSE_CAPABILITY);

            } else {
                response.setType(Message.RESPONSE_ERROR);
                response.setContent("ERROR: Unsupported request");
            }

            os.writeObject(response);
            os.flush();
            System.out.println(" Sent response " + response.toString());
        } catch (Exception e) {
            System.out.println(h + "WARNING: exception was thrown");
            e.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {}
            }
        }
    }
}
