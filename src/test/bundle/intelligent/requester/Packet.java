/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.requester;

import java.io.Serializable;

/**
 *
 * @author kutila2
 * created on 31/05/2010
 */
public class Packet implements Serializable {

    private String from;
    private String to;
    private Serializable content;

    public Serializable getContent() {
        return content;
    }

    public void setContent(Serializable content) {
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    

}
