/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.selector;

import bundle.intelligent.common.Group;
import bundle.intelligent.common.Match;
import java.util.HashMap;
import java.util.Vector;
import junit.framework.TestCase;
import mma2.capability.CapabilityDetails;
import mma2.capability.CapabilityDetails.AGENT_PLATFORMS;
import mma2.capability.CapabilitySpec;

/**
 *
 * @author kutila2
 */
public class SelectorHelperTest extends TestCase {
    
    public SelectorHelperTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test buildGroups method, of class Helper.
     */
    public void testBuildGroups() {
        System.out.println("testBuildGroups()");
        HashMap<CapabilitySpec, Vector<Match>> data = new HashMap<CapabilitySpec, Vector<Match>>();

        String[] funcs1 = {"function1"};
        AGENT_PLATFORMS[] platforms1 = {AGENT_PLATFORMS.JADE3_5, AGENT_PLATFORMS.JADE3_7};
        String[] funcs2 = {"function4"};
        String[] funcs3 = {"function5"};

        doOneSpec(data, funcs1, platforms1, "local", new String[]{"f1_a", "f1_b"});
        doOneSpec(data, funcs2, platforms1, "localhost:1230", new String[]{"f4_a", "f4_b", "f4_c"});
        doOneSpec(data, funcs3, platforms1, "localhost:1231", new String[]{"f5_a", "f5_b"});

        SelectorHelper helper = new SelectorHelper();
        Vector<Group> groups = helper.buildGroups(data);
        assertNotNull(groups);
        assertEquals(12, groups.size());
        for (Group group : groups) {
            assertEquals(3, group.getContents().length);
//            for (int j = 0; j < group.length; j++) {
//                System.out.print(" " + group[j].details.getId());
//            }
//            System.out.println("");
        }
    }

 private void doOneSpec(HashMap data,
            String[] funcs,
            AGENT_PLATFORMS[] platforms,
            String provider,
            String[] ids) {

            //create Spec
            CapabilitySpec spec = new CapabilitySpec();
            spec.setAgentPlatforms(platforms);
            spec.setFunctions(funcs);
            Vector v = new Vector();

            for (int i = 0; i < ids.length; i++) {
                //create Match
                Match m1 = new Match();
                m1.provider = provider;
                m1.details = new CapabilityDetails();
                m1.details.setId(ids[i]);
                m1.details.setAgentPlatforms(platforms);
                v.add(m1); //add to Vector
            }
            data.put(spec, v);
    }
}
