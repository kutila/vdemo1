/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.txt;

import bundle.ir.common.FileObject;
import bundle.ir.common.FileObject.FILE_TYPE;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import junit.framework.TestCase;

/**
 * Tests the info-retrieval capabilities' TxtReader.java
 * 
 * @author kutila
 * created 23/01/2009
 */
public class ReaderTest extends TestCase {
    private String filename = null;
    
    public void testReadNullFilename() {
        System.out.println("testReadNullFileName()");
        TxtReader reader = new TxtReader();
        FileObject obj = reader.read(filename, ".");
        Vector<Throwable> errs = obj.getErrors();
        
        assertTrue("Wrong number of errors", errs.size() > 0);
        assertTrue("Wrong type of error", errs.get(0) instanceof FileNotFoundException);
    }

    public void testReadNonExistentFile() {
        System.out.println("testNonExistentFileName()");
        filename = "";
        TxtReader reader = new TxtReader();
        FileObject obj = reader.read(filename, ".");
        Vector<Throwable> errs = obj.getErrors();
        
        assertTrue("Wrong number of errors", errs.size() > 0);
        assertTrue("Wrong type of error", errs.get(0) instanceof FileNotFoundException);
    }

    public void testSampleFile() {
        System.out.println("testSampleFile()");
        filename = "simple.txt";
        String body = "The quick brown fox jumped over the fence and went home.";

        //--create a temporary a file
        try {
            FileWriter out = new FileWriter(filename);
            out.write(body);
            out.close();
        } catch (IOException ex) {
            fail("Failed to create test file due to: " + ex.toString());
        }

        //--test the Reader
        TxtReader reader = new TxtReader();
        FileObject obj = reader.read(filename, ".");
        
        assertTrue("Expected 0 errors", obj.getErrors().size() == 0);
        assertEquals("Wrong file body", body, obj.getText());
        assertEquals("Wrong file name", filename, obj.getName());
        assertEquals("Wrong file type", FILE_TYPE.TXT, obj.getFileType());
        
        //--delete temporary file
        (new File(filename)).delete();
    }
    
    public void testSampleFile2() {
        System.out.println("testSampleFile2()");
        filename = "simple.txt";
        String body = "The quick brown fox jumped over the fence and went home.";

        //--create a temporary a file
        try {
            FileWriter out = new FileWriter(filename);
            out.write(body);
            out.close();
        } catch (IOException ex) {
            fail("Failed to create test file due to: " + ex.toString());
        }

        //--test the Reader
        TxtReader reader = new TxtReader();
        FileObject obj = reader.read(new File(filename));
        
        assertTrue("Expected 0 errors", obj.getErrors().size() == 0);
        assertEquals("Wrong file body", body, obj.getText());
        assertEquals("Wrong file name", filename, obj.getName());
        assertEquals("Wrong file type", FILE_TYPE.TXT, obj.getFileType());
        
        //--delete temporary file
        (new File(filename)).delete();
    }
}
