/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma;

import junit.framework.TestCase;

/**
 *
 * Spike test class, no need to run regularly.
 *
 * @author kutila
 * created 22/01/2009
 */
public class Test2Utils extends TestCase {

    public void testOne() {
        String[] items = {"new file.txt", "someOther.pdf"};
        for (String item : items) {
            String fileType = item.substring(item.lastIndexOf('.') + 1);
            System.out.println(fileType);    
        }
        
    }
    
    public void testTwo() {
        String text = "The quick brown fox jumped over the fence.\n" +
                "Unfortunately it fell into a well.";
        String[] words = text.split("\\s");
        assertEquals("Wrong number of words counted", 14, words.length);
    }

    public void testDivision() {
        long latency = 2000;
        long totalTime = 0;
        int requestSize = 300;
        int msgOverheadSize = 50;
        int capSize = 234100;
        int bandwidth = 64000;
        //2*2000 + (300 + 2*50 + 234100)/64000
        totalTime = 2 * latency + (requestSize + 2 * msgOverheadSize + capSize) / bandwidth;
        System.out.println("Total Time = " + totalTime);
    }
    
}
