/*
 * TestAll.java
 *
 * Created on 5 December 2007, 16:43
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mma;

import junit.framework.*;
import bundle.ir.txt.ReaderTest;

/**
 *
 * @author kutila
 */
public class TestAll extends TestCase {
    
    /** Creates a new instance of TestAll */
    public TestAll() {
    }

    public static void main(String[] args) {
        TestSuite suite = new TestSuite("All vdemo unit tests");
        
        //bundle related tests
        suite.addTestSuite(ReaderTest.class);
        suite.addTestSuite(bundle.intelligent.requester.RequesterHelperTest.class);
        suite.addTestSuite(bundle.intelligent.selector.SelectorHelperTest.class);
        suite.addTestSuite(bundle.intelligent.common.AbsoluteConstraintTest.class);
        suite.addTestSuite(bundle.intelligent.common.RelativeConstraintTest.class);
        suite.addTestSuite(bundle.costmodel.ahp1.AhpHelperTest.class);

        junit.textui.TestRunner.run(suite);
    }
    
}
