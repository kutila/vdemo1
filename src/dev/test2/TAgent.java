package test2;

import jade.content.lang.sl.SLCodec;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.domain.mobility.MobilityOntology;

import java.io.File;
import java.io.FileInputStream;
import java.util.Hashtable;

/**
 * Normal (i.e. non-VERSAG) agent for "Test2"
 * 
 * @author kutila
 * created 27/01/2009
 */
public class TAgent extends AbstractAgent {


    private Behaviour behaviour1;
    private Behaviour behaviour2;
    
    private Hashtable repository = new Hashtable();
    private Hashtable<String, String> itineraryTable = new Hashtable<String, String>();
    private transient Hashtable<String, TCapabilityIF> capabilityTable;
    
    
    /**
     * Setup is called only once when the agent is being created. 
     */
    @Override
    public void setup() {
        jadeSetup();
        //loadJars4Carrying();
        
        
        //--add Behaviours
        behaviour1 = new TItineraryBehaviour(this); 
        behaviour2 = new TMsgListeningBehaviour(this);
        addBehaviour(behaviour1);
        addBehaviour(behaviour2);
    }

    
    @Override
    protected void jadeSetup() {
        // Register SL0 content language and JADE mobility ontology
        getContentManager().registerLanguage(new SLCodec(), FIPANames.ContentLanguage.FIPA_SL0);
        getContentManager().registerOntology(MobilityOntology.getInstance());
        loadCapabilities();
    }

    public Hashtable getItinerary() {
        return itineraryTable;
    }

    public synchronized String getItineraryLine(String loc) {
        return itineraryTable.get(loc);
    }

    public synchronized void setItineraryLine(String loc, String cmds) {
        itineraryTable.put(loc, cmds);
    }
    
    public synchronized void setItinerary(String itinerary) {
        itineraryTable.clear();

        String[] itineraryLines = itinerary.split("\n");//split according to lines
        for (int i = 0; i < itineraryLines.length; i++) {
            //E.g.: locx=fetch...#exec...#move...
            String[] tmp = itineraryLines[i].split("="); //E.g. {locx, fetch...#exec...}
            String location = tmp[0];
            itineraryTable.put(location, tmp[1]);
        }
    }

    public Object getParameter(String key) {
        return repository.get(key);
    }
    
    public void setParameter(String key, Object value) {
        repository.put(key, value);
    }

    public Object removeParameter(String key) {
        return repository.remove(key);
    }
    
    public TCapabilityIF getCapability(String capabilityName) {
        return capabilityTable.get(capabilityName);
    }
    
    private void loadCapabilities() {
        capabilityTable = new Hashtable<String, TCapabilityIF>();
        TMatcher matcher = new TMatcher(this);
        capabilityTable.put("matcher", matcher);
        
        PdfReader pdfReader = new PdfReader();
        capabilityTable.put("pdf", pdfReader);
        
        TxtReader txtReader = new TxtReader();
        capabilityTable.put("txt", txtReader);
        capabilityTable.put("resultHandler", matcher);
        
        ResultHandler resultHandler = new ResultHandler(this);
        capabilityTable.put("result", resultHandler);
    }

    /**
     * NOTE: This is done to represent the jar files being carried around
     * by the agent
     */
    private void loadJars4Carrying() {
        String jarfilename = "pdflibrary.jar";
        String path = "lib" + File.separatorChar + jarfilename;
        byte[] b = readFileIntoByteArray(path);
        repository.put(jarfilename, b);
        logger.info("Loaded jar file has size (bytes): " + b.length);
    }
    
        /**
     * Read a file's content into a byte array.
     * 
     * @param fileName Name of file to be read
     * @return a byte array filled with the file contents
     * @throws java.io.IOException If a failure occurs
     */
    private byte[] readFileIntoByteArray(String fileName) {
        byte[] b = new byte[1];
        try {
            FileInputStream in = new FileInputStream(fileName);
            int size = in.available();
            b = new byte[size];
            in.read(b);
        } catch (Exception ex) {
            System.out.println(fileName + " could not read into a byte array. " + ex.toString());
        }
        return b;
    }

}
