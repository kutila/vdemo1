/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package test2;

import java.io.File;
import jade.util.Logger;

/**
 *
 * @author kutila
 * created 28/01/2009
 */
public class TMatcher implements TCapabilityIF {

    public static final String MATCHING_CRITERIA = "test2.MATCHING_CRITERIA";
    public static final String SEARCH_LOCATION = "test2.SEARCH_LOCATION";
    public static final String RESULT = "test2.RESULT";
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private String matchingCriteria;
    //private Hashtable<String, AbstractReader> readers = new Hashtable<String, AbstractReader>();
    private boolean stopRequest = false;
    private String resultString = "";
    private TAgent agent;
    
    public TMatcher(TAgent agent) {
        this.agent = agent;
    }
    
    public void start() {
        doWork();
    }

    public void stop() {
        stopRequest = true;
    }

    public void doWork() {
        if (stopRequest) {
            log("TMatcher can't run because of stop request.");
            return;
        }
        
        //--get parameters 
        matchingCriteria = (String) agent.getParameter(MATCHING_CRITERIA);
        String searchLocation = (String) agent.getParameter(SEARCH_LOCATION);

        File dir = new File(searchLocation);
        File[] fileList = dir.listFiles();
        for (File f : fileList) {
            String name = f.getName();
            String type = name.substring(name.lastIndexOf('.') + 1);
            if (!isSupportedFileType(type)) {
                log(name + " is not of a supported type");
                continue;
            }
            AbstractReader reader = (AbstractReader) agent.getCapability(type);
            if (reader == null) {
                log("Could not find reader");
                continue;
            }

            TFileObject fObj = reader.read(f);
            log("Parsed: " + fObj.toString());
            if (isMatch(fObj)) {
                process(fObj);
            }
        }
        //--update agent's result
        resultString = agent.getParameter(RESULT) + resultString;
        agent.setParameter(RESULT, resultString);
        log("Updated result is " + resultString);
        
        log("TMatcher finishing");
    }
    
    private boolean isSupportedFileType(String item) {
        if ((item != null && item.trim().length() > 2) &&
                (item.equalsIgnoreCase("pdf") || 
                item.equalsIgnoreCase("doc") || item.equalsIgnoreCase("txt"))) {
            return true;
        }
        return false;
    }

    /**
     * Compares a FileObject against the "matchingCriteria"
     * 
     * @param fObj The FileObject to be matched
     * @return true if it is a match, false otherwise
     */
    private boolean isMatch(TFileObject fObj) {
        String text = fObj.getText().toLowerCase();
        //TODO - add support for more detailed comparisons
        //SIMPLY SEES IF THE GIVEN TEXT IS IN THE FILE!
        if (text != null && text.contains(matchingCriteria)) {
            return true;
        }
        return false;
    }

    private void process(TFileObject fObj) {
        //TODO: some meaningful processing here
        String text = fObj.getText();
        int wordCount = 0;
        if (text != null && text.length() > 0) {
            String[] words = text.split("\\W");
            wordCount = words.length;
        }
        resultString = resultString + "\n" + fObj.getName() + ":" + wordCount;
    }

    
    private void log(Object o) {
        //System.out.println(o.toString());
        logger.info(o.toString());
    }
    
}
