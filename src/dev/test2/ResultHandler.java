/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package test2;

import java.util.Hashtable;
import jade.util.Logger;


/**
 * Requestes the agent to move to "home" (if not already there) and
 * displays the result from the Repository
 * 
 * @author kutila
 * created 27/01/2009
 */
public class ResultHandler implements TCapabilityIF {

    public static final String RESULT = "test2.RESULT";
    public static final String HOME = "test2.HOME";

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    private TAgent agent;
    
    
    public ResultHandler(TAgent agent) {
        this.agent = agent;
    }
    
    public void doResultDisplay() {

        String homeLoc = (String) agent.getParameter(HOME);
        String currentLoc = (String) agent.here().getName();
        logger.fine("Home: " + homeLoc + ", Now at: " + currentLoc);
        
        if (!homeLoc.equalsIgnoreCase(currentLoc)) {
            String itinerary = currentLoc + "=move " + homeLoc + "\n" + 
                    homeLoc + "=start result";
            agent.setItinerary(itinerary);
            logger.fine("Updated itinerary: " + itinerary);
        } else {
            //Display the result
            System.out.println("*******************************");
            System.out.println((String) agent.getParameter(RESULT));
            System.out.println("*******************************");
        }
    }

    public void start() {
        doResultDisplay();
    }

    public void stop() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}
