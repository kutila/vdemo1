/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package test2;

import java.io.File;

/**
 *
 * @author kutila
 * created 22/01/2009
 */
public abstract class AbstractReader {

    public abstract TFileObject read(String filename, String path);
    public abstract TFileObject read(File file);

}
