package test2;


import jade.core.Location;
import jade.core.behaviours.CyclicBehaviour;

import java.io.File;
import java.io.FileInputStream;
import jade.util.Logger;


/**
 * For "Test2" non-VERSAG agent
 * 
 * @author kutila
 * created 27/01/2009
 */
public class TItineraryBehaviour extends CyclicBehaviour {

    private TAgent agent;
    private transient Logger logger = null;

    private transient boolean initialized = false;
    
    public TItineraryBehaviour(TAgent agent) {
        super(agent);
        this.agent = agent;
        logger = Logger.getMyLogger(this.getClass().getName());

        logger.info("Created");
    }
    
    public void action() {
        reInitialize();
        String cmd = getItineraryCommand();
        if (cmd == null || cmd.length() == 0) {
            return;
        }
        logger.fine("Executing Command: " + cmd);
        
        if (cmd.startsWith("move")) {
            String destination = cmd.split("\\s")[1];
            Location dest = new jade.core.ContainerID(destination, null);    
            agent.doMove(dest);
        } else if (cmd.startsWith("terminate")) {
            agent.doDelete();
            logger.fine("terminating");

        } else if (cmd.startsWith("stop")) {
            String capability = cmd.split("\\s")[1];
            logger.fine("stop " + capability);
            TCapabilityIF cap = agent.getCapability(capability);
            if (cap != null) {
                cap.stop();
            }
            
        } else if (cmd.startsWith("start")) {
            String capabilityName = cmd.split("\\s")[1];
            logger.fine("start " + capabilityName);
            TCapabilityIF cap = agent.getCapability(capabilityName);
            if (cap != null) {
                cap.start();//blocks until task finishes
                logger.fine("finished " + capabilityName);
            }
            
        } else if (cmd.startsWith("remove")) {
            String[] params = cmd.split("\\s");
            logger.fine("remove " + params[1] + " " + params[2]);

            String jarfilename = params[1];
            int count = Integer.parseInt(params[2]);
            int removedCount = 0;
            for (int i = 0; i < count; i++) {
                Object obj = agent.removeParameter(jarfilename+i);
                if (obj != null) {
                    removedCount++;
                }
            } 
            logger.fine("removed " + removedCount + " objects");

        } else if (cmd.startsWith("add")) {
            String[] params = cmd.split("\\s");
            logger.fine("add " + params[1] + " " + params[2]);

            String jarfilename = params[1];
            String path = "lib" + File.separatorChar + jarfilename;
            int count = Integer.parseInt(params[2]);
            byte[] b1 = readFileIntoByteArray(path);
            for (int i = 0; i < count; i++) {
                byte[] b2 = b1.clone();
                System.arraycopy(b1, 0, b2, 0, b1.length);
                agent.setParameter(jarfilename + i, b2);
            }
            logger.fine("added " + count + " objects of length " + b1.length);
        } else {
            logger.warning("Unknown command: " + cmd);
        }
        
        
    }

    /**
     * Stop the underlying services and relase any resources (e.g. before moving)
     */
    public void stop() {
    }

    /**
     * Get the next itinerary command to execute and update the 
     * itinerary without the retrieved command
     * 
     * @return
     */
    private String getItineraryCommand() {
        String here = agent.here().getName();
        String itineraryLine = agent.getItineraryLine(here);
        if (itineraryLine != null && itineraryLine.length() > 0) {
            String[] cmds = itineraryLine.split("#", 2);
            if (cmds.length == 2) {
                agent.setItineraryLine(here, cmds[1]);
                logger.fine("Updated the itinerary line to: " + cmds[1]);
                return cmds[0];
            } else if (cmds.length > 0) {
                agent.setItineraryLine(here, "");
                logger.fine("Updated the itinerary line to: ");
                return cmds[0];
            }
        }
        return "";
    }
    /**
     * Re-initialize, possibly after agent migration
     *  - recreate transient objects (Logger, ExecutorService...)
     */
    private void reInitialize() {
        if (initialized) {
            //System.out.print("."); //DEBUG
            return; //already initialized, no need to do again
        }
        if (logger == null) {
            logger = Logger.getMyLogger(this.getClass().getName());
        }
        logger.fine("Initialized");
        initialized = true;
    }

    
        /**
     * Read a file's content into a byte array.
     * 
     * @param fileName Name of file to be read
     * @return a byte array filled with the file contents
     * @throws java.io.IOException If a failure occurs
     */
    private byte[] readFileIntoByteArray(String fileName) {
        byte[] b = new byte[1];
        try {
            FileInputStream in = new FileInputStream(fileName);
            int size = in.available();
            b = new byte[size];
            in.read(b);
        } catch (Exception ex) {
            System.out.println(fileName + " could not read into a byte array. " + ex.toString());
        }
        return b;
    }
    
}
