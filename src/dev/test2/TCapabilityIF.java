/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package test2;

/**
 *
 * @author kutila
 * created 27/01/2009
 */
public interface TCapabilityIF {

    public void start();
    
    public void stop();
    
}
