/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package test2;

import java.io.File;
import java.io.FileReader;

/**
 *
 * @author kutila
 * created 23/01/2009
 */
public class TxtReader extends AbstractReader implements TCapabilityIF {

    /**
     * Read a file into a FileObject
     * 
     * @param filename
     * @return
     */
    @Override
    public TFileObject read(String filename, String path) {
        TFileObject fObj = new TFileObject();
        fObj.setName(filename);
        fObj.setFileType(TFileObject.FILE_TYPE.TXT);
        try {
            FileReader in = new FileReader(path + File.separatorChar + filename);
            StringBuffer sbuf = new StringBuffer();
            char[] cbuf = new char[1024];
            int count = in.read(cbuf);
            while (count > 0) {
                sbuf.append(cbuf, 0, count);
                count = in.read(cbuf);
            }
            in.close();

            fObj.setText(sbuf.toString());
        } catch (Exception e) {
            fObj.setError(e);
        }
        return fObj;
    }

    @Override
    public TFileObject read(File file) {
        TFileObject fObj = new TFileObject();
        fObj.setName(file.getName());
        fObj.setFileType(TFileObject.FILE_TYPE.TXT);
        try {
            FileReader in = new FileReader(file);
            StringBuffer sbuf = new StringBuffer();
            char[] cbuf = new char[1024];
            int count = in.read(cbuf);
            while (count > 0) {
                sbuf.append(cbuf, 0, count);
                count = in.read(cbuf);
            }
            in.close();

            fObj.setText(sbuf.toString());
        } catch (Exception e) {
            fObj.setError(e);
        }
        return fObj;
    }

    public void start() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void stop() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
