/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package test2;

import jade.core.Agent;
import jade.util.Logger;

/**
 *
 * @author kutila
 * created 27/01/2009
 */
public abstract class AbstractAgent extends Agent {

    protected transient Logger logger = Logger.getMyLogger(this.getClass().getName());

    @Override
    protected void afterClone() {
        System.out.println(getLocalName() + " has cloned itself.");
        afterMove();
    }

    public void afterLoad() {
        afterClone();
    }

    @Override
    protected void afterMove() {
        logger = Logger.getMyLogger(this.getClass().getName());
        logger.info(getLocalName() + " just arrived to this location.");
        jadeSetup();
    }

    public void afterReload() {
        afterMove();
    }

    public void afterThaw() {
        afterMove();
    }

    @Override
    protected void beforeClone() {
        System.out.println(getLocalName() + " is now cloning itself.");
    }

    public void beforeFreeze() {
        beforeMove();
    }

    @Override
    protected void beforeMove() {
        logger.info(getLocalName() + " is now moving elsewhere.");
    }

    public void beforeReload() {
        beforeMove();
    }

    @Override
    public void takeDown() {
        logger.info("takeDown()");
    }

    protected abstract void jadeSetup();

}
