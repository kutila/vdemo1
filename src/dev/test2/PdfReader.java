/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package test2;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;

/**
 *
 * @author kutila
 * created 26/01/2009
 */
public class PdfReader extends AbstractReader implements TCapabilityIF {

    /**
     * Read a file into a FileObject
     * 
     * @param filename
     * @return
     */
    @Override
    public TFileObject read(String filename, String path) {
        TFileObject fObj = new TFileObject();
        fObj.setName(filename);
        fObj.setFileType(TFileObject.FILE_TYPE.PDF);
        File f = new File(path + File.separatorChar + filename);
        return internalRead(fObj, f);
    }

    @Override
    public TFileObject read(File file) {
        TFileObject fObj = new TFileObject();
        fObj.setName(file.getName());
        fObj.setFileType(TFileObject.FILE_TYPE.PDF);
        return internalRead(fObj, file);
    }
    
    private TFileObject internalRead(TFileObject fObj, File file) {
        try {
            PDDocument pdfDoc = PDDocument.load(
                    new BufferedInputStream(new FileInputStream(file)));
            PDFTextStripper textStripper = new PDFTextStripper();
            System.out.println("2");
            StringWriter text = new StringWriter();
            textStripper.writeText(pdfDoc, text);
            pdfDoc.close();
            fObj.setText(text.toString());
        } catch (Exception e) {
            fObj.setError(e);
        }
        return fObj;
    }

    public void start() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void stop() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
}
