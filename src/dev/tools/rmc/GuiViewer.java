/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package tools.rmc;

import java.awt.Color;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 * This class updates the GUI's Documents with styled messages obtained
 * from the Queue.
 * 
 * @author kutila2
 */
public class GuiViewer extends Thread {

    private static final String STYLE_ITALIC = "italics";
    private static final String STYLE_BOLD = "bold";
    private static final String STYLE_HIGHLIGHT = "background";
    private static final String STYLE_RED_FONT = "redfont";
    private static final String STYLE_GRAY_FONT = "grayfont";

    private GuiFrame frame;

    private final StyledDocument mainDoc = new DefaultStyledDocument();
    private final StyledDocument errorDoc = new DefaultStyledDocument();

    
    Style defaultStyle;
    Style grayFontStyle;
    Style boldStyle;
    Style highlightStyle;
    Style errorStyle;

    public GuiViewer() {

        //create Document and Styles
        defaultStyle = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

        StyleConstants.setItalic(mainDoc.addStyle(STYLE_ITALIC, defaultStyle), true);
        StyleConstants.setBold(mainDoc.addStyle(STYLE_BOLD, defaultStyle), true);
        StyleConstants.setBackground(mainDoc.addStyle(STYLE_HIGHLIGHT, defaultStyle), Color.YELLOW);
        StyleConstants.setForeground(mainDoc.addStyle(STYLE_GRAY_FONT, defaultStyle), Color.GRAY);

        boldStyle = mainDoc.getStyle(STYLE_BOLD);
        highlightStyle = mainDoc.getStyle(STYLE_HIGHLIGHT);
        grayFontStyle = mainDoc.getStyle(STYLE_GRAY_FONT);

        StyleConstants.setForeground(errorDoc.addStyle(STYLE_RED_FONT, defaultStyle), Color.RED);
        errorStyle = errorDoc.getStyle(STYLE_RED_FONT);

        //start Graphical UI
        Runnable r = new Runnable() {

            public void run() {
                frame = new GuiFrame(mainDoc, errorDoc);
                frame.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(r);
    }

    @Override
    public void run() {
        System.out.println("GuiViewer starting...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {}

        try {
            Main.queue.put("VERSAG Remote Monitoring Console started\r\n");
        } catch (Exception e) {}

        while (true) {
            try {
                String logRecord = Main.queue.take();
                insertStyledRecord(logRecord);
            } catch (InterruptedException ex) {
            //ignore
            }
        }
    }

    // 13/02/2011 8:11:27 PM [14] INFO mma2.jade.JadeAgent  - BOB is being take down.
    // [0]        [1]     [2] [3] [4]  [5]                 [6] [7] ... 
    private void insertStyledRecord(String logRecord) {
        if (logRecord == null || logRecord.trim().length() == 0) {
            insertString(logRecord + "\r\n", null, mainDoc);
            return;
        }
        String[] tokens = logRecord.split(" ", 7);
        if (tokens.length < 7) {
            //not in expected log statement format. Insert in Gray font
            insertString(logRecord + "\r\n", grayFontStyle, mainDoc);
        } else {

            //display only INFO messages
            if (!(tokens[4].startsWith("INFO") || tokens[4].startsWith("WARNING") || tokens[4].startsWith("SEVERE"))) {
                return;
            }

            String time = tokens[1] + " ";
            String message = tokens[6] + "\r\n";

            //insert tokens
            if (tokens[4].startsWith("WARNING") || tokens[4].startsWith("SEVERE")) {
                insertString(time, grayFontStyle, errorDoc);
                insertString(message, errorStyle, errorDoc);
            } else {
                insertString(time, grayFontStyle, mainDoc);
                insertString(message, defaultStyle, mainDoc);
            }
        }
    }

    private void insertString(String str, Style style, StyledDocument doc) {
        try {
            doc.insertString(doc.getLength(), str, style);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
