/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package tools.rmc;

import java.util.concurrent.ArrayBlockingQueue;

/**
 *
 * @author kutila2
 */
public class Main {

    public static ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(100);
        
    public static void main(String[] args) {
        System.out.println("Starting VERSAG Remote Monitoring Console...");
        
        //start listening for log messages from remote handlers
        SocketListener s = new SocketListener(5000);
        s.start();
        
        //start Console Viewer
        //ConsoleLogViewer viewer = new ConsoleLogViewer();
        //viewer.start();
        
        //start Swing GUI Viewer
        GuiViewer updater = new GuiViewer();
        updater.start();
    }
}
