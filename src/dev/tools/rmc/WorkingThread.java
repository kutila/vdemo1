/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package tools.rmc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Handles incoming log messages from a single socket, adding them to the shared Queue.
 * 
 * @author kutila2
 * @since 2011/02/13
 */
public class WorkingThread extends Thread {

    private Socket sock;

    public WorkingThread(Socket s) {
        this.sock = s;
    }

    public void setSocket(Socket s) {
        this.sock = s;
    }

    @Override
    public void run() {
        OutputStream out = null;
        try {
            InputStream is = sock.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "US-ASCII"));
            String line = null;
            while ((line = br.readLine()) != null) {
                Main.queue.put(line);
                //System.out.println(line);
            }

            
            try {Thread.sleep(4000); } catch (InterruptedException e) {}
        } catch (Exception e) {
            System.out.println("Error while handling request " + e.toString());
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (sock != null) {
                    sock.close();
                }
            } catch (IOException ioe) {/* ignore */}
        }
    }

}
