/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package tools.rmc;


/**
 * Remote Monitoring of Log Messages on the Console
 * 
 * @author kutila2
 */
public class ConsoleLogViewer extends Thread {

    
    public void run() {
        System.out.println("ConsoleLogViewer starting...");
        while (true) {
            try {
                String logRecord = process(Main.queue.take());
                if (logRecord != null) {
                    System.out.println(">" + logRecord);
                }
            } catch (InterruptedException ex) {
                //ignore
            }
        }
    }
    // 13/02/2011 8:11:27 PM [14] INFO mma2.jade.JadeAgent  - BOB is being take down.
    // [0]        [1]     [2] [3] [4]  [5]                 [6] [7] ... 
    private String process(String logRecord) {
        if (logRecord == null || logRecord.trim().length() == 0) {
            return null;
        }
        String[] tokens = logRecord.split(" ", 7);
        if (tokens.length < 7) {
            //seems something is wrong. Lets just return the whole thing
            return logRecord;
        }
        StringBuilder msgToDisplay = new StringBuilder();
        msgToDisplay.append(tokens[1]); //time
        
        //display only INFO messages
        if (!tokens[4].startsWith("INFO")) {
            return null;
        }

        //append the message
        msgToDisplay.append(tokens[6]);
        
        
        return msgToDisplay.toString();
    }
    
}
