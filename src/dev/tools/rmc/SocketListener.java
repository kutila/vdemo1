/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package tools.rmc;



import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Socket server class listening for incoming log messages from remote Socket Handlers
 * 
 * @author kutila2
 * @since 2011/02/13
 */
public class SocketListener extends Thread {
    
    
    private boolean stop = false;
    private ServerSocket socket;

    private int port;
    static int bufSize = 1024*64; //64KB
    static byte[] responseBuf = new byte[bufSize];

    public SocketListener(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        Socket sock = null;

        try {
            ServerSocketChannel svrChannel = ServerSocketChannel.open();
            ServerSocket server = svrChannel.socket();
            server.bind(new InetSocketAddress(port));

            System.out.println("Listening for incoming log messages on port " + port);
            //work in an infinite loop
            while (!isStopRequested()) {
                WorkingThread wp = new WorkingThread(null);

                SocketChannel socketChannel = svrChannel.accept();
                sock = socketChannel.socket(); //got a request

                //hand over serving the client to a new thread.
                wp.setSocket(sock);
                wp.start();
            }
        } catch (ClosedByInterruptException e) {
            //expected when the Thread is to be stopped
            //System.out.println("cbie received!!!");
        } catch (Exception e) {
            System.out.println("Stop listening for incoming log messages due to " + e.toString());
        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (Exception e) {
                }
            }
        }
        System.out.println("Socket Listener shutting down");
    }

    public synchronized boolean isStopRequested() {
        return stop;
    }

    public synchronized void requestStop() {
        stop = true;
        this.interrupt();
    }


}
