package tools.ca;

import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.ContainerID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPANames;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.KillAgent;
import jade.domain.JADEAgentManagement.KillContainer;
import jade.domain.JADEAgentManagement.ShutdownPlatform;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

/**
 * This Cyclic Behaviour waits for the user to type in a command which it then 
 * executes. 
 * The supported commands are:
 *  create NAME FQN container args[] - start a new Agent
 *  killagent NAME                   - kill the named Agent
 *  killcontainer NAME               - kill the named Container
 *  quit                             - kill the Controller Agent
 *  pause n                          - pause the Controller Agent for n seconds
 *  shutdown                         - terminates the Agent Platform
 *  move NAME ORIGIN DESTINATION     - move named agent via itinerary request
 *  status NAME                      - request internal status of named agent
 *  run {filename}                   - run a script with commands
 *
 * A script file can have the same set of commands plus messages to be sent
 * to agents as ACL REQUEST messages. Each command should be in a single line
 * except messaging requests which are multi-line and should be separated
 * from subsequent commands by a blank line.
 *
 * @author kutila
 * created on 18/08/2009
 */
public class ConsoleInputBehaviour extends CyclicBehaviour {
    public static final String SEPARATOR = " ";
    
    private static final String CMD_COMMENT = "#";
    private static final String CMD_CREATE = "create";
    private static final String CMD_STATUS = "status";
    private static final String CMD_MOVE = "move";
    private static final String CMD_QUIT = "quit";
    private static final String CMD_RUN = "run";
    private static final String CMD_KILL_AGENT = "killagent";
    private static final String CMD_KILL_CONTAINER = "killcontainer";
    private static final String CMD_SHUTDOWN = "shutdown";
    private static final String CMD_PAUSE = "pause";
    private static final String PROMPT = ">";

    private String usageMsg = "Enter \n"
            + " \"run <file name>\" to run a script containing commands \n"
            + " \"create NAME fully.qualified.class.name container-name arg1 arg2...\" "
            + "to start an agent \n"
            + " \"status NAME\" to retrieve current stats of an agent \n"
            + " \"move NAME ORIGIN DESTINATION\" to move named agent to destination \n"
            + " \"killagent NAME\" to kill an agent \n"
            + " \"killcontainer NAME\" to kill a container \n"
            + " \"shutdown\" to shut down the JADE platform \n"
            + " \"pause n\" to pause Controller Agent for n seconds \n"
            + " \"quit\" to terminate Controller Agent \n"
            + "";

    public ConsoleInputBehaviour(Agent a) {
        super(a);
        System.out.println("****************************");
        System.out.println("Controller Agent starting...");
        System.out.println("****************************");
        System.out.println(usageMsg);
    }

    /**
     * Handle the commands input by user.
     */
    @Override
    public void action() {
        System.out.print(PROMPT);
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        String cmd = null;
        try {
            cmd = buff.readLine();
        } catch (Exception e) {
            System.out.println("     Error reading user input: " + e.toString());
        }
        if (cmd != null) {
            cmd = cmd.trim();
            if (cmd.startsWith(CMD_CREATE)) {
                createAgent(cmd);

            } else if (cmd.startsWith(CMD_STATUS)) {
                getStatus(cmd.split(SEPARATOR)[1]);

            } else if (cmd.startsWith(CMD_MOVE)) {
                moveAgent(cmd);

            } else if (cmd.startsWith(CMD_KILL_AGENT)) {
                killAgent(cmd.split(SEPARATOR)[1]);

            } else if (cmd.startsWith(CMD_KILL_CONTAINER)) {
                killContainer(cmd.split(SEPARATOR)[1]);

            } else if (cmd.startsWith(CMD_RUN)) {
                readScriptAndExecuteCommand(cmd.split(SEPARATOR)[1]);

            } else if (cmd.equalsIgnoreCase(CMD_QUIT)) {
                quitControllerAgent();

            } else if (cmd.startsWith(CMD_PAUSE)) {
                pauseControllerAgent(cmd.split(SEPARATOR)[1]);
                
            } else if (cmd.equalsIgnoreCase(CMD_SHUTDOWN)) {
                shutDownPlatformViaAMS();

            } else {
                System.out.println("      Unknown command: " + cmd);
            }
        }
    }

    private void readScriptAndExecuteCommand(String filename) {
        System.out.println("     Running script...");
        try {
            BufferedReader r = new BufferedReader(new FileReader(filename));
            String cmd = r.readLine();
            while (cmd != null) {
                cmd = cmd.trim();
                if (cmd.startsWith(CMD_CREATE)) {
                    createAgent(cmd);

                } else if (cmd.startsWith(CMD_PAUSE)) {
                    pauseControllerAgent(cmd.split(SEPARATOR)[1]);

                } else if (cmd.equalsIgnoreCase(CMD_QUIT)) {
                    quitControllerAgent();

                } else if (cmd.startsWith(CMD_STATUS)) {
                    getStatus(cmd.split(SEPARATOR)[1]);

                } else if (cmd.startsWith(CMD_MOVE)) {
                    moveAgent(cmd);

                } else if (cmd.startsWith(CMD_KILL_AGENT)) {
                    killAgent(cmd.split(SEPARATOR)[1]);

                } else if (cmd.startsWith(CMD_KILL_CONTAINER)) {
                    killContainer(cmd.split(SEPARATOR)[1]);

                } else if (cmd.equalsIgnoreCase(CMD_SHUTDOWN)) {
                    shutDownPlatformViaAMS();

                } else if (cmd.startsWith("to:")) {
                    String to = cmd.split(":")[1];
                    StringBuilder msgBody = new StringBuilder();
                    String line = r.readLine();
                    while (line != null && line.trim().length() > 0) {
                        msgBody.append(line);
                        msgBody.append("\n");
                        line = r.readLine();
                    } //blank line or EOF encountered
                    msgBody.append("\n");
                    sendACLRequestMessage(to, msgBody.toString(), null);
                    if (line == null) { //EOF
                        return;
                    }
                } else if (cmd.startsWith(CMD_COMMENT)) {
                    //comment line, nothing to do
                } else {
                    System.out.println("     Unknown command: " + cmd);
                }
                cmd = r.readLine();
            }
        } catch (Exception e) {
            System.out.println("     Error running script: " + e.getMessage());
        }
    }

    private void createAgent(String cmd) {
        System.out.println("     Creating new agent...");
        String[] parts = cmd.split(" ");
        String[] args = null;
        if (parts.length < 4) {
            System.out.println("     Incorrect number of parameters");
        } else {
            if (parts.length > 4) {
                args = new String[parts.length - 4];
                System.arraycopy(parts, 4, args, 0, parts.length - 4);
            }
            createAgentViaAMS(parts[1], parts[2], parts[3], args);
        }
    }

    private void getStatus(String cmd) {
        if (cmd == null || cmd.length() < 1) {
            System.out.println("     Please specify name of agent");
        } else {
            System.out.println("     Retrieving status of agent " + cmd + "...");
            String conversationId = "Monitor-" + System.currentTimeMillis();
            sendACLRequestMessage(cmd.trim(), "status-request", conversationId);
            String response = receiveACLMessage(conversationId);
            System.out.println("**************************************************");
            System.out.println(response);
            System.out.println("**************************************************");
        }
    }

    private void moveAgent(String cmd) {
        System.out.println("     Moving agent...");
        String[] parts = cmd.split(" ");
        
        if (parts.length != 4) { //i.e. move NAME ORIGIN DESTINATION
            System.out.println("     Incorrect number of parameters");
        } else {
            String to = parts[1];
            String msgBody = "itinerary\n" +
                    parts[2] + "=move " + parts[3] + "\n\n";
            sendACLRequestMessage(to, msgBody, null);
        }
    }
    
    private void killAgent(String agentName) {
        System.out.println("     Killing agent...");
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.addReceiver(myAgent.getAMS());
        request.setOntology(JADEManagementOntology.NAME);
        request.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
        request.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        Action act = new Action();
        KillAgent killAgent = new KillAgent();
        killAgent.setAgent(new AID(agentName, AID.ISLOCALNAME));
        act.setAction(killAgent);
        act.setActor(myAgent.getAMS());
        try {
            myAgent.getContentManager().fillContent(request, act);
        } catch (Exception e) {
            e.printStackTrace();
        }
        myAgent.send(request);
    }

    private void killContainer(String containerId) {
        System.out.println("     Killing container...");
        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.addReceiver(myAgent.getAMS());
        request.setOntology(JADEManagementOntology.NAME);
        request.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
        request.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        Action act = new Action();
        KillContainer killContainer = new KillContainer();
        killContainer.setContainer(new ContainerID(containerId, null));
        act.setAction(killContainer);
        act.setActor(myAgent.getAMS());
        try {
            myAgent.getContentManager().fillContent(request, act);
        } catch (Exception e) {
            e.printStackTrace();
        }
        myAgent.send(request);
    }


    private void shutDownPlatformViaAMS() {
        System.out.println("     Shutting down Agent Platform...");

        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.addReceiver(myAgent.getAMS());
        request.setOntology(JADEManagementOntology.NAME);
        request.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
        request.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        Action act = new Action();
        act.setAction(new ShutdownPlatform());
        act.setActor(myAgent.getAMS());
        try {
            myAgent.getContentManager().fillContent(request, act);
        } catch (Exception e) {
            e.printStackTrace();
        }
        myAgent.send(request);
    }

    /**
     * NOTE:
     * The "pause" is implemented via a sleep. This has the side effect
     * that the Controller Agent is completely blocked for the specified sleep
     * time. If Behaviour.block(long) method is to be used, the mechanism of
     * executing scripts has to be changed to happen over multiple agent
     * execution cycles.
     */
    private void pauseControllerAgent(String seconds) {
        System.out.println("     Pausing for a maximum of " + seconds + " seconds...");
        try {
            long secs = Long.parseLong(seconds);
            Thread.sleep(secs * 1000);
        } catch (Exception e) {
            System.out.println("     Incorrect time to pause: " + seconds);
        }
    }

    private void quitControllerAgent() {
        System.out.println("     Controller Agent terminating...");
        myAgent.doDelete();
    }

    private void sendACLRequestMessage(String to, String body, String convId) {
        System.out.println("     Sending ACL message...");
        ACLMessage message = new ACLMessage(ACLMessage.REQUEST);
        message.addReceiver(new AID(to, AID.ISLOCALNAME));
        if (convId != null) {
            message.setConversationId(convId);
        }
        message.setContent(body.toString());
        myAgent.send(message);
    }

    private String receiveACLMessage(String conversationId) {
        MessageTemplate template = MessageTemplate.MatchConversationId(conversationId);
        ACLMessage response = myAgent.blockingReceive(template, 500);
        if (response != null) {
            return response.getContent();
        }
        return null;
    }
    
    /**
     * Note: There is an issue when sending arguments: commons-codec is needed
     * and JADE tries to encode the arguments in Base64, which are actually
     * simple strings, and ends up sending garbled values.
     *
     * Therefore, as a workaround hard-coded messages are sent to the AMS asking
     * it to create a new agent. [2010-09-21]
     *
     * @param agentName nick name of the agent
     * @param agentClass FQ class name of the agent
     * @param container where the agent should be created
     * @param args any arguments to be passed into the agent
     */
    private void createAgentViaAMS(
            String agentName,
            String agentClass,
            String container,
            String[] args) {

        ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
        request.addReceiver(myAgent.getAMS());
        request.setOntology(JADEManagementOntology.NAME);
        request.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
        request.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);

        StringBuilder b = new StringBuilder();
        b.append(" ((action "
                + "(agent-identifier "
                + ":name " + myAgent.getAMS().getName()
                + " :addresses (sequence http://hppc:7778/acc)) "
                + "(create-agent "
                + ":agent-name ");
        b.append(agentName);

        b.append(" :class-name ");
        b.append(agentClass);

        if (args != null) {
            b.append(" :arguments (sequence ");
            for (int i = 0; i < args.length; i++) {
                b.append(args[i]);
                if (i < args.length - 1) {
                    b.append(',');
                }
            }
            b.append(" ) ");
        }
        b.append(" :container (container-ID :name ");
        b.append(container);

        b.append(" "
                + ":protocol JADE-IMTP "
                + ":address \"<Unknown Host>\" "
                + ":protocol JADE-IMTP))))");

        request.setContent(b.toString());
        myAgent.send(request);
    }

//            " ((action " +
//            "(agent-identifier " +
//            ":name ams@130.194.70.98:1099/JADE " +
//            ":addresses (sequence http://hppc:7778/acc)) " +
//            "(create-agent " +
//            ":agent-name TIM " +
//            ":class-name mma2.jade.JadeAgent " +
//            ":arguments (sequence agent1.properties) " +
//            ":container " +
//            "(container-ID " +
//            ":name loc1 " +
//            ":protocol JADE-IMTP " +
//            ":address \"<Unknown Host>\" " +
//            ":protocol JADE-IMTP))))";
}
