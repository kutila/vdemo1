/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package tools.ca;

import jade.content.lang.sl.SLCodec;
import jade.core.Agent;
import jade.domain.FIPANames;
import jade.domain.JADEAgentManagement.JADEManagementOntology;

/**
 * Simple Agent to allow sending of pre-written messages in text files to agents
 * via a text based UI.
 *
 * @author kutila
 * created on 08/09/2010
 */
public class ControllerAgent extends Agent {

    @Override
    public void setup() {
        getContentManager().registerLanguage(new SLCodec(), FIPANames.ContentLanguage.FIPA_SL);
        getContentManager().registerOntology(JADEManagementOntology.getInstance());
        addBehaviour(new ConsoleInputBehaviour(this));
    }

}
