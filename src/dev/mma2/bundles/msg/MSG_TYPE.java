/*
 * VERSAG project
 */
package mma2.bundles.msg;

/**
 * An enum for the types of ACL messages that are supported by this bundle.
 * 
 * @author kutila
 */
public enum MSG_TYPE {
    REQUEST, INFORM, PROPOSE, REJECT
}
