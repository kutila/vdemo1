/*
 * VERSAG project
 */
package mma2.bundles.msg;

import mma2.bundles.common.ServiceIF;

/**
 * Bundle interface for reading incoming messages.
 *
 * @author kutila
 * created on 4/11/2008
 */
public interface MessageHandler extends ServiceIF {

    /**
     * Checks the agent's message queue for any matching messages.
     * Does not block waiting for messages.
     * 
     * @param type The type of message expected
     * @param conversationId An optional identifier for messages if it is part of
     *        an ongoing conversation
     * @return a matching Message or null if none found
     */
    public Message receive(MSG_TYPE type, String conversationId) throws Exception;

    /**
     * Send the passed in message as an ACL message
     * 
     * @param msg
     */
    public void send(Message msg) throws Exception;

}
