/*
 * VERSAG project
 */
package mma2.bundles.msg.impl;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.Iterator;
import jade.util.Logger;
import org.osgi.framework.BundleContext;

import mma2.bundles.msg.*;
import org.osgi.framework.ServiceReference;

/**
 *
 *
 * @author kutila
 * created on 4/11/2008
 */
public class ACLMessageHandler implements MessageHandler {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private BundleContext context;
    //private boolean stopRequest = false;
    private Agent agent;
    
    public ACLMessageHandler(BundleContext context) {
        this.context = context;
        ServiceReference ref = context.getServiceReference(Agent.class.getName());
        agent = (Agent) context.getService(ref);
    }

    @Override
    public void requestStop() {
        //stopRequest = true;
    }

    @Override
    public Message receive(MSG_TYPE type, String conversationId) throws Exception {
        int aclMsgType = getJadeACLType(type);
        if (aclMsgType < 0 || type == MSG_TYPE.REQUEST) {//an unknown or disallowed message type
            throw new Exception("Cannot receive message type " + type);
        }
        MessageTemplate mt = MessageTemplate.MatchPerformative(aclMsgType);
        if (conversationId != null) {
            mt = MessageTemplate.and(mt, MessageTemplate.MatchConversationId(conversationId));
        }
        
        ACLMessage msg = agent.receive(mt);   //non-blocking message reception
        if (msg != null) {
            return convertFromJadeACLMessage(msg, type);
        } else {
            return null; //no matching message
        }
    }

    @Override
    public void send(Message msg) throws Exception {
        int aclMsgType = getJadeACLType(msg.getType());
        if (aclMsgType < 0) {
            throw new Exception("Unsupported message type");
        }
        ACLMessage aclMsg = convert2JadeACLMessage(msg, aclMsgType);
        agent.send(aclMsg);
    }
    
    /* ----- private methods ----- */
    
    private Message convertFromJadeACLMessage(ACLMessage aclMsg, MSG_TYPE type) {
        Message message = new Message();
        message.setContent(aclMsg.getContent());
        message.setSender(aclMsg.getSender().getName());
        message.setType(type);
        //sets the first receiver as the recipient. Should support multiple values
        Iterator it = aclMsg.getAllReceiver();
        if (it.hasNext()) {
            message.setRecipient(((AID) it.next()).getName());
        }
        return message;
    }

    private ACLMessage convert2JadeACLMessage(Message msg, int type) {
        ACLMessage aclMsg = new ACLMessage(type);
        aclMsg.addReceiver(new AID(msg.getRecipient(), false));
        aclMsg.setSender(new AID(msg.getSender(), false));
        
        ACLMessage t = (ACLMessage) msg.getOriginalMessage();
        if (t != null) {
            aclMsg.setInReplyTo(t.getConversationId());
        }
        
        aclMsg.setContent(msg.getContent());
        return aclMsg;
    }
    
    private int getJadeACLType(MSG_TYPE type) {
        int ACLMsgType;
        switch (type) {
            case REQUEST:
                ACLMsgType = ACLMessage.REQUEST;
            case INFORM:
                ACLMsgType = ACLMessage.INFORM;
                break;
            case PROPOSE:
                ACLMsgType = ACLMessage.PROPOSE;
                break;
            default:
                ACLMsgType = ACLMessage.REFUSE;
                break;
        }
        return ACLMsgType;
    }

}
