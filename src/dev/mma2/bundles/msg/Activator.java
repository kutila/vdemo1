/*
 * VERSAG project
 */
package mma2.bundles.msg;

import mma2.bundles.msg.impl.ACLMessageHandler;
import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the Simple Message Handler
 *
 * @author kutila
 * created on 27/10/2008
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private MessageHandler theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new ACLMessageHandler(context);
        serviceRegistration =context.registerService(
                MessageHandler.class.getName(),
                theService, null);
        logger.fine("Service started: " + MessageHandler.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        theService.requestStop();

        logger.fine("Service stopped: " + MessageHandler.class.getName());
    }

}
