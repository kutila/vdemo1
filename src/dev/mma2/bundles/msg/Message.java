/*
 * VERSAG project
 */
package mma2.bundles.msg;

/**
 *
 *
 * @author kutila
 * created on 4/11/2008
 */
public class Message {

    private String content;
    private String sender;
    private String recipient;
    private MSG_TYPE type;
    private Object originalMessage;
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public MSG_TYPE getType() {
        return type;
    }
    
    public void setType(MSG_TYPE type) {
        this.type = type;
    }

    public Object getOriginalMessage() {
        return originalMessage;
    }

    public void setOriginalMessage(Object originalMessage) {
        this.originalMessage = originalMessage;
    }
    
    
    @Override
    public String toString() {
        return "{" + sender + ":" + recipient + ":" + type + ":" + content + "}";
    }
}
