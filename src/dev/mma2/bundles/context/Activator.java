/*
 * VERSAG project
 */
package mma2.bundles.context;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
//import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the Context service
 *
 * @author kutila
 * modified on 27/06/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
//    private ServiceRegistration serviceRegistration;
    private MyContextService theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new MyContextService(context);
//        serviceRegistration =context.registerService(
//                MyContextService.class.getName(),
//                theService, null);

        theService.start();
        logger.fine("Service started: " + MyContextService.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        theService.requestStop();
//        serviceRegistration.unregister();

        logger.fine("Service stopped: " + MyContextService.class.getName());
    }

}
