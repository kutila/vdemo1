/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.bundles.context;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author kutila
 * created on 27/06/2010
 */
public class MySensor {

    public static void main(String[] args) {
        String host = "hercules.infotech.monash.edu.au";// "66.102.11.104";
        MySensor sensor = new MySensor();
        sensor.ping(host, 4);
        sensor.ping2(host);
    }

    public void calculateLatency(String remoteHost) {

    }

    public long ping(String remoteHost, int loopCount) {
        int timeOut = 3000; //3 seconds
        try {
            boolean status = false;
            long startTime = System.nanoTime();
            for (int i = 0; i < loopCount; i++) {
                status = InetAddress.getByName(remoteHost).isReachable(timeOut);
            }
            long rtt = (System.nanoTime() - startTime)/3000000;

            System.out.println(remoteHost + " ping status " + status + " Time (ms) = " + rtt);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 03;
    }

    public long ping2(String host) {
        try {
            Socket t = new Socket(host, 7);
            DataInputStream dis = new DataInputStream(t.getInputStream());
            PrintStream ps = new PrintStream(t.getOutputStream());
            ps.println("Hello");
            String str = dis.readLine();
            if (str.equals("Hello")) {
                System.out.println("Alive!");
            } else {
                System.out.println("Dead or echo port not responding");
            }
            t.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
