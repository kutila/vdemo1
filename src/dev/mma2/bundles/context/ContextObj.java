/*
 * VERSAG project
 */
package mma2.bundles.context;

/**
 * Represents a simple Context object.
 * 
 * @author kutila
 * created on 27/10/2008
 */
public class ContextObj {

    /** Indicates resource level (say, power, memory, CPU etc.) */
    private ResourceLevel resources;

    public ResourceLevel getResources() {
        return resources;
    }

    public void setResources(ResourceLevel resources) {
        this.resources = resources;
    }
    
    
    
}
