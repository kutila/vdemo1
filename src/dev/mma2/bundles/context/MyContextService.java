/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package mma2.bundles.context;

import bundle.context.daemon.PingDaemon;
import bundle.context.daemon.BwDaemon;
import jade.util.Logger;
import mma2.bundles.common.ServiceIF;
import mma2.capability.Repository;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author kutila2
 * created on 27/06/2010
 */
public class MyContextService extends Thread implements ServiceIF {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    private BundleContext context;
    private boolean stop = false;
    private long sleepInterval = 10 * 1000; //10 secs

    private PingDaemon pingDaemon;
    private BwDaemon bwDaemon;

    public MyContextService(BundleContext context) {
        super("MyContextService");
        this.context = context;
    }

    @Override
    public void run() {
        logger.fine("Started running Context sensing thread");

        pingDaemon = new PingDaemon(1111);
        pingDaemon.start();

        bwDaemon = new BwDaemon(1112);
        bwDaemon.start();

//        while (!isStopRequested()) {
//            int requestSize = 100;
//            int msgOverheadSize = 300;
//            long time2Start = 10;
//            long latency = 5;
//            float bandwidth = 1650.4f;
//
//            //TODO - sense values
//
//            //write parameters to repository
//            Repository repository = getRepository();
//            repository.set("REQUEST_SIZE", requestSize); //bytes
//            repository.set("MSG_OVERHEAD_SIZE", msgOverheadSize); //ms
//            repository.set("TIME_TO_START", time2Start); //ms
//
//            //temporarily read context info from repository
//            repository.set("LATENCY", latency); //ms
//            repository.set("BANDWIDTH", bandwidth); //bytes per ms
//
//            //sleep
//            try {Thread.sleep(sleepInterval);} catch (InterruptedException e) {}
//        }
        logger.fine("Exiting Context sensing thread");
    }

    //TODO
    public ContextObj getContext() {
        ContextObj ctx = new ContextObj();

        //--temporary way of generating context data!!!
        if (System.currentTimeMillis() % 2 == 0) {
            ctx.setResources(ResourceLevel.OK);
        } else {
            ctx.setResources(ResourceLevel.HIGH);
        }
        logger.fine("returning context value: " + ctx.getResources());
        return ctx;
    }

    public synchronized boolean isStopRequested() {
        return stop;
    }

    public synchronized void requestStop() {
        pingDaemon.requestStop();
        bwDaemon.requestStop();
        this.interrupt();
        stop = true;
    }

        /**
     * Get a reference to the Repository
     *
     * @return reference to Repository
     */
    private Repository getRepository() {
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        Repository repository = (Repository) context.getService(ref);
        return repository;
    }

}
