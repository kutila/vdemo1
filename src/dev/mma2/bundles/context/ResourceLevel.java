/*
 * VERSAG project
 */
package mma2.bundles.context;

/**
 * Indicates various resource levels used by the test context service
 * 
 * @author kutila
 * created on 27/10/2008
 */
public enum ResourceLevel {
    LOW, OK, HIGH
}
