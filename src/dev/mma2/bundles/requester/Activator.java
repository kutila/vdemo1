/*
 * VERSAG project
 */
package mma2.bundles.requester;

import jade.util.Logger;
import mma2.bundles.common.RequesterIF;
import mma2.bundles.requester.impl.SimpleRequesterImpl;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the Simple Capability Requester
 *
 * @author kutila
 * created on 27/10/2008
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private RequesterIF theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new SimpleRequesterImpl(context);
        serviceRegistration =context.registerService(
                RequesterIF.class.getName(),
                theService, null);
        logger.fine("Service started: " + RequesterIF.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        theService.requestStop();

        logger.fine("Service stopped: " + RequesterIF.class.getName());
    }

}
