/*
 * VERSAG project
 */
package mma2.bundles.requester.impl;

import jade.util.Logger;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

import java.util.Vector;
import mma2.AgentIF;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import mma2.bundles.common.RequesterIF;
import mma2.capability.Capability;
import mma2.capability.Repository;

/**
 * A simple implementation of the <i>RequesterIF<i>. Uses simple direct capability
 * requests over TCP.
 * 
 * @author kutila
 * updated on 18/12/2008
 */
public class SimpleRequesterImpl implements RequesterIF {

    private static int PORT_START = 1230;
    private static int PORT_END = 1240;
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private Repository repository;
    private BundleContext context;
    private Vector<String> providers = new Vector<String>();

    /** Constructor */
    public SimpleRequesterImpl(BundleContext context) {
        this.context = context;
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        repository = (Repository) context.getService(ref);
        providers.add("130.194.70.98:1230"); //PC
        //providers.add("115.70.17.81:1230"); //Laptop on 3G
    }

    @Override
    public void findById(final String capabilityId) {
        repository.set("STARTEDAT", System.currentTimeMillis()); //XXX - for AAMAS 2 experiment
        logger.fine("Find " + capabilityId);
        //updateProviders(); COMMENTED OUT FOR UIC TEST

        new Thread() {
            @Override
            public void run() {
                if (!repository.containsEntry(capabilityId)) {
                    try {
                        long startTime = System.currentTimeMillis(); //STATS
                        boolean result = fetch(capabilityId);
                        long fetchTime = (System.currentTimeMillis() - startTime); //STATS
                        logger.info("Found = " + result + ", Time (ms) = " + fetchTime); 
                    } catch (Exception e) {
                        logger.severe("Failed to fetch " + capabilityId + e.toString());
                    }
                } else {
                    logger.info(capabilityId + " is already available");
                }
                doCleanup();
            }
        }.start();
    }

    @Override
    public void findBySpec(String[] specs) {
        logger.warning("This Requester does not support finding Capabilities by their specs!!!");
        repository.set(Repository.COMMAND_PREFIX + "FIND", "EXECUTED");
    }

    /**
     * Update the list of provider agents, removing any previous entries.
     * 
     *  1. add provider agents specified in Repository
     *  2. add providers at current location (localhost)
     *  3. [NOT IMPLEMENTED] look for providers in nearby locations 
     *  4. [NOT IMPLEMENTED] ask from a broker (e.g. DF agent)
     */
    private void updateProviders() {
        Vector<String> v = new Vector<String>();
        
        //--providers specified in Repository
        String[] strArray = (String[]) repository.get(Repository.KEY_PROVIDER_LIST);
        if (strArray == null) {
            strArray = new String[0];
        }
        for (int i = 0; i < strArray.length; i++) {
            v.add(strArray[i]);
        }
        
        //--providers at current location
        String localhost = "localhost";
        try {
            localhost = InetAddress.getLocalHost().getHostName();
        } catch (Exception e) { //ignore
        }
        for (int i = PORT_START; i <= PORT_END; i++) {
            try {
                Socket s = new Socket(localhost, i);
                s.close();
                String entry = localhost + ":" + i;
                v.add(entry);
            } catch (IOException e) { //ignore
            }
        }
        
        //--other possible providers [NOT IMPLEMENTED]
        
        providers = v;
        logger.fine("Updated provider list: " + providers.toString());
    }
    
    /**
     * Fetch the given Capability from a remote agent. First it is checked
     * whether the Capability is already available. If not, requests are sent
     * to the remote agents specified until one of them responds with the
     * Capability.
     *
     * @id Identifier of the Capability to fetch
     */
    private boolean fetch(String id) {
        //check if capability already available, if not continue below
        if (providers == null || providers.size() < 1) {
            return false;
        }
        Capability capability = null;
        for (String currentAgentAddr : providers) {
            Socket sock = null;
            try {
                byte[] req = Util.toBytes(id);
                sock = Util.getSocket(currentAgentAddr);
                
                //send the request to the Server
                OutputStream out = sock.getOutputStream();
                out.write(req);
                out.flush();
                
                capability = Util.decodeResponse(sock.getInputStream()); //try to read the response
                out.close();
                break;  //no need to search further
            } catch (Exception e) {
                //logger.debug("ok, that didn't work " + e.toString());
                //ignore and continue searching in the next agent
            } finally {
                if (sock != null) {
                    try {
                        sock.close();
                    } catch (IOException ex) {} //ignore
                }
            }
        }
        if (capability != null) {
            repository.setCapability(id, capability);
            return true;
        } else {
            repository.set(Repository.STATE_PREFIX + id, Repository.UNAVAILABLE);
            return false;
        }
    }


    public void requestStop() {
        //nothing to stop
    }

    /**
     * Indicates the completion of the "find" by adding an entry to the
     * repository and sends a wake up signal to the kernel.
     *
     * Fixes BUG 010: "Find command blocks itinerary execution"
     */
    private void doCleanup() {
        repository.set(Repository.COMMAND_PREFIX + "FIND", "EXECUTED");

        ServiceReference ref = context.getServiceReference(AgentIF.class.getName());
        AgentIF agent = (AgentIF) context.getService(ref);
        agent.wakeUpKernel();
    }
}
