/*
 * Util.java
 *
 * Created on 6 December 2007, 11:18
 *
 */
package mma2.bundles.requester.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import mma2.capability.Capability;

/**
 * A Utility class
 *
 * @author kutila
 */
public class Util {
    
    public static String ENCODING      = "UTF-16BE";

    /* ----- utility methods ----- */
    
    public static Socket getSocket(String agentAddr) throws IOException {
        int idxColon = agentAddr.indexOf(':');
        String address = agentAddr.substring(0, idxColon);
        int port = Integer.parseInt(agentAddr.substring(idxColon + 1, agentAddr.length()));
        
        Socket sock = new Socket(address, port);
        sock.setSoTimeout(12 * 1000); //ensure that the read() block only for 12 secs max
        return sock;
    }


    public static byte[] toBytes(String param) {
        try {
            return param.getBytes(ENCODING);
        } catch (UnsupportedEncodingException e) {
            System.err.println("unsupported encoding: " + ENCODING);
            return null;
        }
    }
    
    public static Capability decodeResponse(InputStream in) throws Exception {
        Capability c = null;
        ObjectInputStream is = new ObjectInputStream(in);
        try {
            c = (Capability) is.readObject();
        } catch (Exception e) {
            System.out.println("Error in deserialization");
            e.printStackTrace();
            throw e;       
        }
        return c;
    }

    public static byte[] read(InputStream in) throws Exception {
        byte[] b1 = new byte[1024 * 1024]; //max data size of 1MB
        int noOfBytes = in.read(b1);
        if (noOfBytes < 1) {
            throw new Exception("Could not read from inputstream");
        }
        byte[] buf = new byte[noOfBytes];
        System.arraycopy(b1, 0, buf, 0, noOfBytes);
        return buf;
    }
    
}
