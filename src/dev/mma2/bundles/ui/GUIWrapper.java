/*
 * VERSAG project
 */
package mma2.bundles.ui;

import java.io.Serializable;
import java.util.Set;

import mma2.AgentIF;
import mma2.bundles.common.ServiceIF;
import mma2.capability.Capability;
import mma2.capability.Repository;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * A simple agent GUI bundle. This class is a wrapper around the actual Swing
 * UI classes.
 * 
 * @author kutila
 * created 19/12/2008
 */
class GUIWrapper implements ServiceIF {

    private AgentGUI gui;
    private BundleContext context;
    private Repository repository;
    private AgentIF agent;
    
    public GUIWrapper(BundleContext context) {
        this.context = context;
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        repository = (Repository) context.getService(ref);
        ref = context.getServiceReference(AgentIF.class.getName());
        agent = (AgentIF) context.getService(ref);

        gui = new AgentGUI(this);
        gui.setVisible(true);
        System.out.println("<<<<<<<<<Started the SimpleGUI bundle");
    }

    public void requestStop() {
        gui.dispose();
        System.out.println("<<<<<<<<<<Stopped the SimpleGUI bundle");
    }

    /**
     * Generates a list of contents that are in this agent's Repository.
     * @return
     */
    public String getRepositoryContents() {
        StringBuffer capBuf = new StringBuffer();

        Bundle[] bundles = context.getBundles();
        
        Set<String> keySet = repository.keySet();
        
        for (String key : keySet) {
            Serializable val = repository.get(key);
            if (val instanceof Capability) {
                String id = ((Capability) val).details.getId();
                capBuf.append(id);
                for (Bundle b: bundles) {
                    String bundleName = (String) b.getHeaders().get("Bundle-Name");
                    if (id.equals(bundleName)) {
                        capBuf.append(" (LOADED)");
                        break;
                    }
                }
                capBuf.append('\n');
            }
        }
        
        return capBuf.toString();
    }

    /**
     * Get the parameters currently contained in the agent's repository as a 
     * String
     */
    public String getParameters() {
        StringBuffer paramBuf = new StringBuffer();

        Set<String> keySet = repository.keySet();
        for (String key : keySet) {
            Serializable val = repository.get(key);
            if (val instanceof String) {
                paramBuf.append(key);
                paramBuf.append('=');
                paramBuf.append(val);
                paramBuf.append('\n');
            }
        }
        return paramBuf.toString();
    }
    
    void setParameters(String newParams) {
        repository.setParameters(newParams);
    }
    
    public String getName() {
        return agent.getAgentName();
    }
    
    public String getItinerary() {
        return agent.getItineraryService().getItinerary();
    }
    
    public void setItinerary(String s) {
        agent.getItineraryService().setItinerary(s);
    }

    public String getLocation() {
        return agent.isAt();
    }
    
    public void stopBundle() {
        //try to uninstall oneself
        try {
            context.getBundle().uninstall();
        } catch (Exception e) {
            System.out.println("failed manual uninstall: " + e.toString());
        }
        System.out.println("<<<<<<<<<<Stopped the SimpleGUI bundle>>>>>");
    }

    
}
