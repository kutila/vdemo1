/*
 * VERSAG project
 */
package mma2.bundles.ui;


import jade.util.Logger;
import mma2.bundles.common.ServiceIF;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for the Connsumer bundle
 *
 * @author kutila
 * @created on 27/10/2008
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private ServiceIF gui;
    
    @Override
    public void start(BundleContext context) throws Exception {
        gui = new GUIWrapper(context);
        logger.fine("Started GUI bundle");
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        gui.requestStop();
        logger.fine("Stopped GUI bundle");
    }

}
