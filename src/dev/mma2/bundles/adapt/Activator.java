/*
 * VERSAG project
 */
package mma2.bundles.adapt;

import mma2.bundles.adapt.impl.AdaptationServiceImpl;
import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the Adaptation Service bundle
 *
 * @author kutila
 * created on 29/10/2008
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private AdaptationService theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new AdaptationServiceImpl(context);
        theService.start();
        serviceRegistration =context.registerService(
                AdaptationService.class.getName(),
                theService, null);

        logger.fine("Service started: " + AdaptationService.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        theService.requestStop();

        logger.fine("Service stopped: " + AdaptationService.class.getName());
    }
}
