/*
 * VERSAG project
 */
package mma2.bundles.adapt;

import mma2.bundles.common.ServiceIF;

/**
 *
 * @author kutila
 */
public interface AdaptationService extends ServiceIF {

    public void start();
    
}
