/*
 * VERSAG project
 */
package mma2.bundles.adapt.impl;

import mma2.bundles.adapt.AdaptationService;
import mma2.capability.Repository;
import jade.util.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * OSGi bundle representing the Adaptation Service.
 * Tasks allocated to this service are as follows:
 *  - monitor context and stop/unload other services if resources are low
 *  - kill hung processes
 *  - switch between different implementations of services based on resource levels
 *
 * @author kutila
 * created on 29/10/2008
 */
public class AdaptationServiceImpl extends Thread implements AdaptationService {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private BundleContext context;
    private boolean stopRequest = false;

    /** Constructor */
    public AdaptationServiceImpl(BundleContext context) {
        this.context = context;
    }

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    @Override
    public synchronized void requestStop() {
        stopRequest = true;
    }
    
    @Override
    public void run() {
        while (!isStopRequested()) {
            String name = "mma2.ContextService";

            //--DEBUG-- install a Bundle
            Bundle b = installBundle(name);
            if (b != null) {
                logger.info("Successfully installed " + name);
            } else {
                logger.info("Failed to install " + name);
            }

            //--DEBUG-- start a Bundle
            boolean started = startBundle(name);
            if (started) {
                logger.info("Successfully started " + name);
            } else {
                logger.info("Failed to start " + name);
            }
            
            //--DEBUG-- access a running service
            Repository repository = getRepository();
            logger.info("Repository size = " + repository.getSize());
            logger.info("Repository      = " + repository.toString());

            Bundle[] bundles = context.getBundles();
            System.out.println("Number of bundles: " + bundles.length);
            for (Bundle bundle : bundles) {
               String symbolicName = (String) bundle.getHeaders().get("Bundle-Name");

                System.out.println("  Id: " + bundle.getBundleId() + " Name: " +
                        symbolicName + " State: " +
                        bundle.getState());
            }

            //sleep
            try {
                sleep(20000);
            } catch (InterruptedException e) {
            }

            //--DEBUG-- stop Bundle
            boolean stopped = stopBundle(name, false);
            if (stopped) {
                logger.info("Successfully stopped " + name);
            } else {
                logger.info("Failed to stop " + name);
            }

            //--see whether uninstalled properly
            bundles = context.getBundles();
            System.out.println("Number of bundles: " + bundles.length);
            for (Bundle bundle : bundles) {
               String symbolicName = (String) bundle.getHeaders().get("Bundle-Name");
                System.out.println("  Id: " + bundle.getBundleId() + " Name: " +
                        symbolicName + " State: " +
                        bundle.getState());
            }

            
            //sleep
            try {
                sleep(20000);
            } catch (InterruptedException e) {
            }

        }//end while
    }

    /**
     * Get a reference to the Repository
     * 
     * @return reference to Repository
     */
    private Repository getRepository() {
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        Repository repository = (Repository) context.getService(ref);
        return repository;
    }

    /**
     * Install a bundle from the Repository
     * 
     * @param bundleName The name of the bundle used in the Repository
     * @return a reference to the installed Bundle or null in case of failure
     */
    private Bundle installBundle(String bundleName) {
        //getCapability jar from repository and start
        Repository repository = getRepository();
        byte[] buf = repository.getCapability(bundleName).getContents();
        try {
            return context.installBundle(
                    "reference:file:bundle/" + bundleName + ".jar",
                    new java.io.ByteArrayInputStream(buf));
        } catch (Exception ex) {
            logger.log( Logger.WARNING, "Failed to install bundle " + bundleName, ex);
        }
        return null;
    }
    
    
    /**
     * Start the bundle with the given name. It should be already installed.
     * 
     * @param bundleName The name of bundle to start
     * @return true if successfully started, false otherwise
     */
    private boolean startBundle(String bundleName) {
        Bundle[] installedBundles = context.getBundles();
        for (Bundle bundle : installedBundles) {
            String symbolicName = (String) bundle.getHeaders().get("Bundle-Name");
            if (bundleName.equals(symbolicName)) {
                try {
                    bundle.start();
                    return true;
                } catch (Exception e) {
                    logger.log( Logger.WARNING, "Error starting bundle ", e);
                    return false;
                }
            }
        }
        return false;
    }
    
    /**
     * Stop the named bundle 
     * 
     * @param stopName name of bundle to stop
     * @param uninstall whether to uninstall bundle also
     * @return true if successful, false if an error occurs
     */
    private boolean stopBundle(String stopName, boolean uninstall) {
        Bundle bundleToStop = null;
        Bundle[] installedBundles = context.getBundles();
        for (Bundle bundle : installedBundles) {
            String symbolicName = (String) bundle.getHeaders().get("Bundle-Name");
            if (stopName.equals(symbolicName)) {
                bundleToStop = bundle;
                break;
            }
        }
        return stopBundle(bundleToStop, uninstall);
    }    
    
    /**
     * Stop the passed in bundle.
     * 
     * @param bundleToStop the OSGi bundle to stop
     * @param uninstall whether to uninstall bundle also
     * @return true if successful, false if an error occurs
     */
    private boolean stopBundle(Bundle bundleToStop, boolean uninstall) {
        boolean result = true;
        if (bundleToStop == null) {
            return result;
        }
        try {
            if (bundleToStop.getState() == Bundle.ACTIVE) {
                bundleToStop.stop();
            }
            if (uninstall && bundleToStop.getState() != Bundle.UNINSTALLED) {
                bundleToStop.uninstall();
            }
        } catch (Exception e) {
            logger.log( Logger.WARNING, "Error while stopping/uninstalling bundle ", e);
            result = false;
        }
        return result;
    }
    
}
