/*
 * VERSAG project
 */
package mma2.bundles.consumer;

import mma2.bundles.context.MyContextService;
import mma2.bundles.msg.*;
import mma2.capability.Repository;

import jade.util.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * A simple thread representing a service consuming OSGi bundle.
 * This is NOT registered as a service with OSGi.
 *
 * @author kutila
 * created on 27/10/2008
 */
public class Consumer extends Thread {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private BundleContext context;
    private boolean stopRequest = false;
    private Repository repository;

    /** Constructor */
    public Consumer(BundleContext context) {
        this.context = context;
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        repository = (Repository) context.getService(ref);
    }

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    public synchronized void requestStop() {
        stopRequest = true;
    }
    
    @Override
    public void run() {
        ServiceReference ref = context.getServiceReference(MyContextService.class.getName());
        MyContextService contextService = (MyContextService) context.getService(ref);
        
        ref = context.getServiceReference(MessageHandler.class.getName());
        MessageHandler msgHandler = (MessageHandler) context.getService(ref);
        
        while (!isStopRequested()) {
            logger.info("Resource level  = " + contextService.getContext().getResources());
            logger.info("Repository  (" + repository.getSize() + ") = " + repository.toString());

            //-- try to receive a message using the MessageHandler
            try {
                Message received = msgHandler.receive(MSG_TYPE.INFORM, null);
                if (received != null) {
                    System.out.println("Incoming message: " + received.toString());
                }
            } catch (Exception e) {
                logger.log(Logger.WARNING, "Failed to receive message", e);
            }
/*            
            //-- try to send a message using the MessageHandler
            Message toSend = new Message();
            toSend.setContent("Hello there");
            toSend.setType(MSG_TYPE.REQUEST);
            toSend.setSender("BOB");
            toSend.setRecipient("da0");
            try {
                msgHandler.send(toSend);
                System.out.println("Sent message: " + toSend.toString());
            } catch (Exception e) {
                logger.warn("Failed to send message", e);
            }
 */ 
            //sleep
            try {
                sleep(15000);
            } catch (InterruptedException e) {
            }
            
        }
    }
}
