/*
 * VERSAG project
 */
package mma2.bundles.consumer;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for the Connsumer bundle
 *
 * @author kutila
 * @created on 27/10/2008
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private Consumer consumer;
    
    @Override
    public void start(BundleContext context) throws Exception {
        consumer = new Consumer(context);
        consumer.start();
        logger.fine("Started Consumer bundle");
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        consumer.requestStop();
        logger.fine("Stopped Consumer bundle");
    }

}
