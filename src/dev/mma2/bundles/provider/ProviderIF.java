/*
 * VERSAG project
 */
package mma2.bundles.provider;

import mma2.bundles.common.ServiceIF;

/**
 * Interface for Capability provider service.
 *
 * @author kutila
 * created on 3/11/2008
 */
public interface ProviderIF extends ServiceIF {

    /**
     * Start the Capability provider service.
     */
    public void start();

}
