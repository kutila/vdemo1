/*
 * Util.java
 *
 * Created on 6 December 2007, 11:18
 *
 */
package mma2.bundles.uicprovider;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * A Utility class
 *
 * @author kutila
 */
public class Util {
    
    public static String ENCODING      = "UTF-16BE";

    /* ----- utility methods ----- */
    
    public static String toString(byte[] b) {
        try {
            return new String(b, ENCODING);
        } catch (UnsupportedEncodingException ex) {
            System.err.println("unsupported encoding: " + ENCODING);
            return null;
        }
    }

    public static byte[] read(InputStream in) throws Exception {
        byte[] b1 = new byte[1024 * 1024]; //max data size of 1MB
        int noOfBytes = in.read(b1);
        if (noOfBytes < 1) {
            throw new Exception("Could not read from inputstream");
        }
        byte[] buf = new byte[noOfBytes];
        System.arraycopy(b1, 0, buf, 0, noOfBytes);
        return buf;
    }
    

}
