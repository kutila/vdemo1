/*
 * VERSAG project
 */
package mma2.bundles.uicprovider;

import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import jade.util.Logger;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import mma2.bundles.provider.ProviderIF;
import mma2.capability.Capability;
import mma2.capability.Repository;

/**
 * A provider implementation which always returns the same capability irrespective
 * of what is requested.
 * 
 * @author kutila
 */
public class AlwaysSameProviderImpl extends Thread implements ProviderIF {

    private static int PORT_START = 1230;
    private static int PORT_END = 1240;

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private BundleContext context;
    private int port;
    private ServerSocket svr;
    private boolean stopRequest = false;

    /** Constructor */
    public AlwaysSameProviderImpl(BundleContext context, int port) {
        this.context = context;
        this.port = port;
    }

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    @Override
    public synchronized void requestStop() {
        stopRequest = true;
        this.interrupt();
    }

    @Override
    public void run() {
        logger.info("Starting");
        try {
            ServerSocketChannel svrChannel = ServerSocketChannel.open();
            svr = svrChannel.socket();
            for (int i = PORT_START; i <= PORT_END; i++) {
                try {
                    svr.bind(new InetSocketAddress(i));
                    port = i;
                    logger.info("Started listening on " + port);
                    break;
                } catch (Exception e) {
                    logger.log(Logger.FINE, "Failed to start Provider on port " + port, e);
                    if (i == PORT_END) {
                        logger.warning("Failed to start Provider");
                        return;
                    }
                }
            }
            final Repository repository = getRepository();
            while (!isStopRequested()) {
                SocketChannel socketChannel = svrChannel.accept();
                final Socket socket = socketChannel.socket(); //got a request
                logger.fine("Serving client: " + socket.getRemoteSocketAddress().toString());
                
                //create a new thread to serve the request
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            byte[] b = Util.read(socket.getInputStream());
                            String capabilityId = Util.toString(b);
                            logger.fine("Serving a request for " + capabilityId);
                            capabilityId = "mma2.UIC";
                            Capability c = repository.getCapability(capabilityId);
                            OutputStream out = socket.getOutputStream();
                            ObjectOutputStream os = new ObjectOutputStream(out);
                            os.writeObject(c);
                            os.flush();
                            os.close();
                            logger.fine("Served " + capabilityId);
                        } catch (Exception e) {
                            logger.log(Logger.WARNING, "", e);
                        } finally {
                            if (socket != null) {
                                try {
                                    socket.close();
                                } catch (Exception ex) {
                                } //ignore
                            }
                        }
                    }
                }.start();
            }
        } catch (ClosedByInterruptException e) {
            //expected when the ProviderThread is to be stopped
            //System.out.println("cbie received!!!");
        } catch (Exception e) {
            logger.log(Logger.INFO, "Stopping due to", e);
        } finally {
            if (svr != null && !svr.isClosed()) {
                try { 
                    svr.close();
                } catch (Exception e) {}
            }
        }
    }

    
    /**
     * Get a reference to the Repository
     * 
     * @return reference to Repository
     */
    private Repository getRepository() {
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        Repository repository = (Repository) context.getService(ref);
        return repository;
    }
    
}