/*
 * VERSAG project
 */
package mma2.bundles.uicprovider;

import mma2.bundles.provider.ProviderIF;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the UIC Capability provider
 *
 * @author kutila
 * created on 09/01/2009
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private ProviderIF theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new AlwaysSameProviderImpl(context, 1234);
        serviceRegistration =context.registerService(
                ProviderIF.class.getName(),
                theService, null);
        theService.start();
        logger.fine("Service started: " + ProviderIF.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        theService.requestStop();

        logger.fine("Service stopped: " + ProviderIF.class.getName());
    }

}
