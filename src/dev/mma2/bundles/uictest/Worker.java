/*
 * VERSAG project
 */
package mma2.bundles.uictest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import mma2.AgentIF;
import mma2.bundles.common.ServiceIF;
import jade.util.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Bundle representing the agent's task to be done for the UIC experiment.
 * - It reads file X’s first token which represents a Capability name.
 * - Update the agent’s itinerary to fetch and execute the new capability.
 * - Unload this capability
 * - Rewrite file X without the first token.
 * 
 * @author kutila
 */
public class Worker extends Thread implements ServiceIF {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    private BundleContext context;
    private AgentIF agent;

    private String workFileName = "task.txt";
    
    public Worker(BundleContext context) {
        this.context = context;
        ServiceReference ref = context.getServiceReference(AgentIF.class.getName());
        agent = (AgentIF) context.getService(ref);
    }
    
    @Override
    public void run() {
        try {
            String content = readFileAsString(workFileName);

            String[] array = content.split(":", 2);
            String capName = array[0].trim();
            if (capName.length() > 0) {
                
                String newItinerary = agent.isAt() +
                        //"=unload mma2.UIC" +
                        "=find " + capName +
                        "#start mma2.UIC";
                agent.getItineraryService().setItinerary(newItinerary);
                
                String writeBack = "";
                if (array.length == 2) {
                    writeBack = array[1];
                }
                writeToFile(workFileName, writeBack); //write back remainder
                logger.fine("Need help to process " + capName);
            } else {
                logger.fine("Process completed. I'm done!");
                writeToFile(workFileName, ""); //write back an empty file
            }
        } catch (Exception ex) {
            logger.log(Logger.WARNING, "Failed to complete worker's task", ex);
        } finally {
            //completed, now stop itself
            try {
                context.getBundle().uninstall();
            } catch (Exception e) {
                logger.log(Logger.WARNING, "Exception when trying to stop/uninstall bundle.", e);
            }
        }
    }
    
    public void requestStop() {
        //what to do here?
    }

    private String readFileAsString(String filePath) {
        BufferedReader reader = null;
        try {
            StringBuffer fileData = new StringBuffer(1000);
            reader = new BufferedReader(new FileReader(filePath));
            char[] buf = new char[1024];
            int numRead = 0;
            while ((numRead = reader.read(buf)) != -1) {
                fileData.append(buf, 0, numRead);
            }
            reader.close();
            return fileData.toString();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                reader.close();
            } catch (Exception ex) {
            }
        }
        return "";
    }

    public void writeToFile(String filename, String content) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(filename));
            writer.write(content);
            writer.flush();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {
            }
        }
    }
}
