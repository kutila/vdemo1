/*
 * VERSAG project
 */
package mma2.bundles.uictest;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for the Connsumer bundle
 *
 * @author kutila
 * @created on 27/10/2008
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private Worker worker;
    
    @Override
    public void start(BundleContext context) throws Exception {
        worker = new Worker(context);
        worker.start();
        logger.fine("Started UIC test bundle");
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        worker.requestStop();
        logger.fine("Stopped UIC test bundle");
    }

}
