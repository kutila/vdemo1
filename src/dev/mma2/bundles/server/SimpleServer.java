/*
 * VERSAG project
 */
package mma2.bundles.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import mma2.bundles.common.ServiceIF;
import jade.util.Logger;

/**
 *
 * @author kutila
 */
public class SimpleServer extends Thread implements ServiceIF {

    private transient Logger logger = null;
    private boolean stopRequest = false;
    private final int port = 2000;
    private ServerSocket srv = null;

    @Override
    public void run() {
        logger = Logger.getMyLogger(this.getClass().getName());
        while (!isStopRequested()) {
            try {
                srv = new ServerSocket(port);
                System.out.println("Server waiting for incoming connection...");
                Socket socket = srv.accept();
                
                //--handle the incoming connection
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String inCmd = in.readLine();
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                out.write("Received: " + inCmd + " from " + socket.getInetAddress().getHostAddress());
                out.flush();
                
                in.close();
                out.close();
                logger.fine("Served request");
            } catch (SocketException e) {
                logger.info("A SocketException was thrown (" + e.getMessage() + ")");
            } catch (IOException e) {
                logger.log(Logger.WARNING, "Exception in Server bundle", e);
            } finally {
                try {
                    if (srv != null) {
                        srv.close();
                    }
                } catch (IOException e) {/*ignore*/
                }
            }
        }
        logger.info("SimpleServer finishing");
    }

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    public synchronized void requestStop() {
        stopRequest = true;
        try {
            if (srv != null) {
                srv.close();
            }
        } catch (IOException e) {/*ignore*/
        }
    }
}
