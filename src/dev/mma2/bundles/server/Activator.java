/*
 * VERSAG project
 */
package mma2.bundles.server;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for the Simple server service.
 * This is is NOT registered as a service with OSGi
 * 
 * @author kutila
 */
public class Activator implements BundleActivator {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private SimpleServer theService;

    public void start(BundleContext arg0) throws Exception {
        theService = new SimpleServer();
        theService.start();

        logger.fine("Service started: " + SimpleServer.class.getName());
    }

    public void stop(BundleContext arg0) throws Exception {
        theService.requestStop();
        
        logger.fine("Service stopped: " + SimpleServer.class.getName());
    }

}
