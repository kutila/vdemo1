/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mma2.bundles.sort.insertion;

/**
 * From: http://www.java-tips.org
 */
public class InsertionSort {

    /**
     * Simple insertion sort.
     * 
     * @param a an array of Comparable items.
     */
    public void insertionSort(Comparable[] a) {
        for (int p = 1; p < a.length; p++) {
            Comparable tmp = a[p];
            int j = p;

            for (; j > 0 && tmp.compareTo(a[j - 1]) < 0; j--) {
                a[j] = a[j - 1];
            }
            a[j] = tmp;
        }
    }
}
