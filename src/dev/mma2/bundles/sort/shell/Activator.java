/*
 * VERSAG project
 */
package mma2.bundles.sort.shell;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for the Shell sort service
 *
 * @author kutila
 * created on 27/10/2008
 */
public class Activator implements BundleActivator  {
    
    private SortService theService;
    private long startTime = 0;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new SortService(context);
        theService.start();
        startTime = System.currentTimeMillis();
        System.out.println("Service started: " + SortService.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        theService.requestStop();
        System.out.println("Service stopped: " + SortService.class.getName());
        System.out.println("Time taken (ms):" + (System.currentTimeMillis() - startTime));
    }

}
