package mma2.bundles.sort.shell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;
import mma2.AgentIF;
import mma2.bundles.common.ServiceIF;
import mma2.capability.Repository;
import mma2.util.Utility;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Shell sort service runs once and then terminates.
 * 
 * @author kutila
 */
public class SortService extends Thread implements ServiceIF  {
    
    private static final String SORT_FILE_NAME = "sort.file.name";
    
    private boolean stopRequest = false;
    private Repository repository;
    private BundleContext context;
    
    /** Constructor */
    public SortService(BundleContext context) {
        this.context = context;
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        repository = (Repository) context.getService(ref);
    }

    @Override
    public synchronized void requestStop() {
        stopRequest = true;
    }
    
    @Override
    public void run() {
        long t1 = System.currentTimeMillis();
        if (!stopRequest) {
            String fileName = (String) repository.get(SORT_FILE_NAME);
            boolean result = sortAFile(fileName);
            
            System.out.println(fileName + " sorted: " + result);
        }
        stopOneShotCapability();
        Utility.log("Shell Sort took (ms) = " + (System.currentTimeMillis() - t1));
    }

    private Object getOSGiService(String className) {
        ServiceReference ref = context.getServiceReference(className);
        return context.getService(ref);
    }

    /**
     * A OneShot type capability when finishing should:
     *  1. stop itself (i.e. the OSGi bundle)
     *  2. wake up the kernel behaviour (call agent.wakeUpKernel() )
     */
    private void stopOneShotCapability() {
        Object agent = getOSGiService(AgentIF.class.getName());
        try {
            context.getBundle().stop();
        } catch (Exception e) {
            System.out.println("Exception when trying to stop/uninstall bundle. <" +
                    e.toString() + ">");
        }
        if (agent != null) {
            ((AgentIF) agent).wakeUpKernel();
        }
    }

    /**
     * Read the given file, sort its lines in ascending order and write 
     * to a new file
     * @param fileName
     * @return true if the sorting succeeded, false otherwise
     */
    private boolean sortAFile(String fileName) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String line = in.readLine();
            Vector v = new Vector();
            while (line != null) {
                v.add(line);
                line = in.readLine();
            }
            in.close();
            String[] array = new String[v.size()];
            System.arraycopy(v.toArray(), 0, array, 0, v.size());


            (new ShellSort()).shellsort((Comparable[]) array);

            //write to new file
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new FileWriter(System.currentTimeMillis() + fileName)));
            for (int i = 0; i < array.length; i++) {
                out.write(array[i] + "\r\n");
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
