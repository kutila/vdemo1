/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mma2.bundles.sort.shell;

/**
 * From: http://www.java-tips.org
 */
public class ShellSort {

    /**
     * Shellsort, using a sequence suggested by Gonnet.
     * @param a an array of Comparable items.
     */
    public void shellsort(Comparable[] a) {
        for (int gap = a.length / 2; gap > 0;
                gap = gap == 2 ? 1 : (int) (gap / 2.2)) {
            for (int i = gap; i < a.length; i++) {
                Comparable tmp = a[i];
                int j = i;

                for (; j >= gap && tmp.compareTo(a[j - gap]) < 0; j -= gap) {
                    a[j] = a[j - gap];
                }
                a[j] = tmp;
            }
        }
    }
}
