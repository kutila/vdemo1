/*
 * VERSAG project
 */
package bundle.intelligent.provider;

import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for intelligent Capability provider
 *
 * @author kutila
 * created on 30/05/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private IntelligentProvider theProvider;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theProvider = new IntelligentProvider(context, 1234);
        theProvider.start();
        logger.fine("Service started: " + IntelligentProvider.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        theProvider.requestStop();
        logger.fine("Service stopped: " + IntelligentProvider.class.getName());
    }

}
