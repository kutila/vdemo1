/*
 * VERSAG project
 */
package bundle.intelligent.provider;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;

import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import java.util.Collection;
import java.util.Vector;

import jade.util.Logger;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import mma2.AgentIF;
import mma2.capability.Capability;
import mma2.capability.CapabilityDetails;
import mma2.capability.CapabilitySpec;
import mma2.capability.Repository;

import bundle.intelligent.common.Message;
import bundle.intelligent.common.ConstantsIF;

/**
 * A Provider Thread acts as a provider of capabilities to other agents. 
 * It will listen on the specified port for capability requests. The Provider
 * is capable of serving multiple requests simultaneously.
 *
 * The Intelligent Provider uses an Object based communication protocol.
 * Two types of requests are supported:
 *  1) requests for details of capabilities matching a set of specifications
 *  2) request for a capability instance, specified by its identifier
 *
 * 
 * @author kutila
 * created on 30/05/2010
 */
public class IntelligentProvider extends Thread implements ConstantsIF {

    private static int PORT_START = 1230;
    private static int PORT_END = 1250;

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    private BundleContext context;
    private final Repository repository;
    private int port;
    private ServerSocket svr;
    private volatile boolean stopRequest = false;
    private String ownerName = null;
    
    /** Constructor */
    public IntelligentProvider(BundleContext context, int port) {
        this.context = context;
        this.port = port;
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        repository = (Repository) context.getService(ref);
        ownerName = (String) repository.get("NAME");
    }

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    public synchronized void requestStop() {
        deregisterInPlatformYellowPages();
        stopRequest = true;
        this.interrupt();
    }

    @Override
    public void run() {
        try {
            ServerSocketChannel svrChannel = ServerSocketChannel.open();
            svr = svrChannel.socket();
            for (int i = PORT_START; i <= PORT_END; i++) {
                try {
                    svr.bind(new InetSocketAddress(i));
                    port = i;
                    logger.info(ownerName + " (iProvider) started listening on " + port);
                    registerInPlatformYellowPages();
                    break;
                } catch (Exception e) {
                    logger.fine("Failed to start iProvider on port " + port + e.toString());
                    if (i == PORT_END) {
                        logger.warning("Failed to start iProvider");
                        return;
                    }
                }
            }

            while (!isStopRequested()) {
                SocketChannel socketChannel = svrChannel.accept();
                final Socket socket = socketChannel.socket(); //got a request
                logger.fine("Serving client: " + socket.getRemoteSocketAddress().toString());

                //create a new thread to serve the request
                HelperThread helper = new HelperThread(socket, repository);
                helper.start();
            }
        } catch (ClosedByInterruptException e) {
            //expected when the ProviderThread is to be stopped
            //System.out.println("cbie received!!!");
        } catch (Exception e) {
            logger.info(ownerName + " (iProvider) stopping due to " + e.toString());
        } finally {
            if (svr != null && !svr.isClosed()) {
                try {
                    svr.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private void registerInPlatformYellowPages() {
        try {
            String address = InetAddress.getLocalHost().getHostAddress() + ":" + port;
            ServiceReference ref = context.getServiceReference(AgentIF.class.getName());
            AgentIF agent = (AgentIF) context.getService(ref);
            agent.registerInYellowPages(YELLOWP_TYPE_PROVIDER, address);
        } catch (Exception e) {
            logger.warning("Failed to register Provider in YellowPages " + e.toString());
        }
    }

    private void deregisterInPlatformYellowPages() {
        ServiceReference ref = context.getServiceReference(AgentIF.class.getName());
        AgentIF agent = (AgentIF) context.getService(ref);
        agent.deregisterFromYellowPages();
    }
}

class HelperThread extends Thread {

    private final Socket socket;
    private final Repository repository;
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private String ownerName = null;
    
    public HelperThread(Socket socket, Repository repository) {
        this.socket = socket;
        this.repository = repository;
        ownerName = (String) repository.get("NAME");
    }

    @Override
    public void run() {
        try {
            ObjectInputStream is = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());
            Object in = is.readObject();

            Message response = buildResponse(in);

            os.writeObject(response);
            os.flush();
            logger.info(ownerName + " (iProvider) sent response of type " + response.getType());
        } catch (Exception e) {
            logger.warning("Error while handling request" + e.toString());
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private Message buildResponse(Object in) {
        Message response = new Message();

        if (in instanceof Message == false) {
            response.setType(Message.RESPONSE_ERROR);
            response.setContent("ERROR: Unsupported request");
            return response;
        }
        Message requestMsg = (Message) in;

        if (requestMsg.getType() == Message.REQUEST_SEARCH) {
            //search for matching capability instances
            CapabilitySpec[] specsToSearchFor = (CapabilitySpec[]) requestMsg.getContent();

            //logger.fine("Request: " + specsToSearchFor.toString());

            Vector<CapabilityDetails> vector = new Vector<CapabilityDetails>();

            Collection<Capability> coll = repository.getCapabilities();

            for (CapabilitySpec spec : specsToSearchFor) {
                //logger.fine("Looking for matches to : " + spec.toString());
                for (Capability localCapability : coll) {
                    if (localCapability.details.match(spec)) {
                        vector.add(localCapability.details);
                    } else {
                        //logger.fine("Do not match:" + localCapability + " , "+ spec);
                    }
                }
            }
            logger.fine("Total matches found " + vector.size());
            response.setContent(vector);
            response.setType(Message.RESPONSE_SEARCH);

        } else if (requestMsg.getType() == Message.REQUEST_CAPABILITY) {
            //prepare capability instance
            Object o = requestMsg.getContent();
            if (o instanceof String) {
                String capabilityId = (String) o;
                if (repository.getCapability(capabilityId) == null) {
                    response.setContent("ERROR: " + capabilityId + " not found");
                } else {
                    Capability c = repository.getCapability(capabilityId);
                    response.setContent(c);
                }
            } else {
                response.setContent("ERROR: Invalid content " + o);
            }
            response.setType(Message.RESPONSE_CAPABILITY);

        } else {
            response.setType(Message.RESPONSE_ERROR);
            response.setContent("ERROR: Unsupported request");
        }

        return response;
    }
}
