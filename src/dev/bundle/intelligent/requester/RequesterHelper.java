/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.requester;

import bundle.intelligent.common.Message;
import bundle.intelligent.common.Endpoint;
import jade.util.Logger;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;
import mma2.capability.CapabilityDetails.AGENT_PLATFORMS;
import mma2.capability.CapabilitySpec;

/**
 *
 * @author kutila2
 * created on 26/05/2010
 */
public class RequesterHelper {

    /** Range of port numbers used */
    public static int PORT_START = 1230;
    private static int PORT_END = 1240;

    private static String GH2_2 = "GH2_2";
    private static String JADE2 = "JADE2";
    private static String JADE3_5 = "JADE3_5";
    private static String JADE3_7 = "JADE3_7";
    private static String JADE4 = "JADE4";

    /** Socket timeout in seconds */
    private static int MAX_TIMEOUT = 12 * 1000;

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    /**
     * Get the available Peer capability providers based on the provided address
     * list. If a port is not provided, searches through the default list of ports.
     * A Peer is checked to be "alive" before it is added to the list.
     *
     * NOTES:
     *  - Searching through ports for active servers is time consuming
     *
     *
     * @param addressList
     * @return
     */
    protected Vector<Endpoint> getProviders(String[] addressList) {
        //long startTime = System.currentTimeMillis(); //STATS

        Vector<Endpoint> providers = new Vector<Endpoint>();

        for (int i = 0; i < addressList.length; i++) {
            String[] parts = addressList[i].split(":");
            String host = parts[0];
            if (parts.length > 1) { //port specified
                int portNo = PORT_START;
                try {
                    portNo = Integer.parseInt(parts[1]);
                } catch (NumberFormatException nfe) {
                }
                Endpoint peer = new Endpoint(parts[0], portNo);
                if (peer.isAlive()) {
                    providers.add(peer);
                }
            } else { //no port given
                for (int j = PORT_START; j <= PORT_END; j++) {
                    Endpoint peer = new Endpoint(host, j);
                    if (peer.isAlive()) {
                        providers.add(peer);
                    }
                }
            }
        }

        //--other possible providers [NOT IMPLEMENTED]
        //long time = (System.currentTimeMillis() - startTime); //STATS
        //System.out.println("Time taken (ms): " + time);
        return providers;
    }

    /**
     * Sequentially sends the request Message to all the Peers and retrieves their
     * responses. The method returns after all responses have been received or
     * the wait for responses has timed out.
     *
     * With a maximum Socket read timeout of 12 seconds, for 5 non-existent
     * peers, this method could take 12*5 = 60 seconds.
     *
     * @param requestMsg
     * @param peers
     * @return A Vector of response Messages
     */
    protected  Vector<Message> sendRequestsAndGetResponses(
            Message searchRequest,
            Vector<Endpoint> peers) {

        Vector<Message> vector = new Vector<Message>();
        for (Endpoint peer : peers) {
            Message response = sendRequestAndGetResponse(searchRequest, peer);
            vector.add(response);
        }
        return vector;
    }

    /**
     * Send the specified request to the specified Peer, and retrieve any response
     * from the peer.
     *
     * This method blocks until the peer replies or a timeout occurs.
     *
     * @param requestMsg
     * @param peer
     * @return The response Message or null
     */
    protected  Message sendRequestAndGetResponse(Message requestMsg, Endpoint peer) {

        Message replyMsg = null;
        Socket sock = null;
        try {
            sock = new Socket(peer.getAddress(), peer.getPort());
            sock.setSoTimeout(MAX_TIMEOUT); //set maximum read block
            ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(sock.getInputStream());

            //send request to the Server
            out.writeObject(requestMsg);
            out.flush();
            logger.fine("Sent request: " + requestMsg);

            //read response
            Object responseObject = in.readObject();
            logger.fine("Received response: " + responseObject.toString());

            if (responseObject instanceof Message) {
                Message response = (Message) responseObject;
                response.setFrom(peer);
                replyMsg = response;
            }
            out.close();
            in.close();
        } catch (Exception e) {
            logger.fine(peer + " gave an error. (" + e.getMessage() + ")");
        } finally {
            if (sock != null) {
                try {
                    sock.close();
                } catch (IOException ex) {} //ignore
            }
        }

        return replyMsg;
    }

    /**
     * Construct <code>CapabilitySpec</code> objects from the given String
     * representations.
     *
     * array[0] = spec
     * array[1] = const;TIME:453.4;LOAD:343434;ACCURACY:3;CPU:9;HEAP:5024
     * array[2] = ALIAS;func1:func2;P1:P2
     * array[3] = ALIAS;func3:func4;P1:P2
     * ...
     *
     * Pi is one of (JADE4, JADE3_7, JADE3_5, JADE2, GH2_2)
     *
     * TODO: Add validations. This method is very brittle!
     *
     * @param specsList
     * @return
     */
    protected CapabilitySpec[] buildCapabilitySpecs(String[] specsList) {

        int size = specsList.length - 2;
        String[] constraints = specsList[1].split(";");
        CapabilitySpec[] results = new CapabilitySpec[size];
        for (int i = 0; i < size; i++) {
            String specStr = specsList[i + 2];
            String[] triple = specStr.split(";");
            String alias = triple[0];
            String[] functions = triple[1].split(":");
            String[] platforms = triple[2].split(":");
            AGENT_PLATFORMS[] agentPlatforms = new AGENT_PLATFORMS[platforms.length];
            for (int j = 0; j < platforms.length; j++) {
                if (platforms[j].equals(GH2_2)) {
                    agentPlatforms[j] = AGENT_PLATFORMS.GH2_2;
                } else if (platforms[j].equals(JADE2)) {
                    agentPlatforms[j] = AGENT_PLATFORMS.JADE2;
                } else if (platforms[j].equals(JADE3_5)) {
                    agentPlatforms[j] = AGENT_PLATFORMS.JADE3_5;
                } else if (platforms[j].equals(JADE3_7)) {
                    agentPlatforms[j] = AGENT_PLATFORMS.JADE3_7;
                } else if (platforms[j].equals(JADE4)) {
                    agentPlatforms[j] = AGENT_PLATFORMS.JADE4;
                }
            }

            CapabilitySpec spec = new CapabilitySpec();
            spec.setAlias(alias);
            spec.setFunctions(functions);
            spec.setAgentPlatforms(agentPlatforms);
            spec.setConstraints(constraints);
            results[i] = spec;
        }

        return results;
    }

}
