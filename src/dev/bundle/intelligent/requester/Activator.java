/*
 * VERSAG project
 */
package bundle.intelligent.requester;

import jade.util.Logger;

import mma2.bundles.common.RequesterIF;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the Intelligent Capability Requester
 *
 * @author kutila
 * created on 09/11/2009
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private RequesterIF requester;
    
    @Override
    public void start(BundleContext context) throws Exception {
        requester = new IntelligentRequester(context);
        serviceRegistration =context.registerService(
                RequesterIF.class.getName(),
                requester, null);
        logger.fine("Service started: " + IntelligentRequester.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        requester.requestStop();

        logger.fine("Service stopped: " + IntelligentRequester.class.getName());
    }

}
