/*
 * VERSAG project
 */
package bundle.intelligent.requester;

import bundle.intelligent.common.ConstantsIF;
import bundle.intelligent.common.IRequesterIF;
import bundle.intelligent.common.Message;
import bundle.intelligent.common.Match;
import bundle.intelligent.common.Endpoint;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

import jade.util.Logger;
import java.util.Collection;
import mma2.AgentIF;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import mma2.bundles.common.RequesterIF;

import mma2.capability.Capability;
import mma2.capability.CapabilityDetails;
import mma2.capability.CapabilitySpec;
import mma2.capability.Repository;

/**
 * The "intelligent" Requester implements findBySpec() method.
 * Uses simple direct capability requests over TCP.
 *
 * @author kutila
 * created on 19/05/2010
 */
public class IntelligentRequester implements RequesterIF, IRequesterIF, ConstantsIF {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private Repository repository;
    private BundleContext context;

    private boolean useYellowPages = true;
    private String ownerName = null;
    
    /** Constructor */
    public IntelligentRequester(BundleContext context) {
        this.context = context;
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        repository = (Repository) context.getService(ref);
        ownerName = (String) repository.get("NAME");
        
        if (repository.get(KEY_PROVIDER_LIST) != null) {
            useYellowPages = false;
        }

        logger.fine(" created");
    }

    /**
     * Search for and acquire a Capability by its Id. First looks in the local
     * repository whether it is already available. If not, requests remote peers
     * for the Capability. The first response received is accepted and added to
     * the agent's repository.
     *
     * This method is asynchronous in that it spawns a new Thread to do the work
     * and returns. Upon completion, the thread adds an entry to the Repository
     * CMD_FIND=EXECUTED indicating that the task is completed and sends a 
     * wakeUp signal to the agent kernel. The kernel uses this entry to identify
     * that it can run the next itinerary command.
     * 
     * @param capabilityId The identifier of the capability needed
     */
    @Override
    public void findById(final String capabilityId) {
        //2010Aug experiments (Service Use Vs Component Use)
        repository.set("STARTEDAT", System.currentTimeMillis());
        
        if (repository.containsEntry(capabilityId)) {
            logger.fine(capabilityId + " is already available");
            doCleanup();
            return;
        }
        logger.info(ownerName + " (iRequester) searching for " + capabilityId);
                    
        //String providerList = (String) repository.get(Repository.KEY_PROVIDER_LIST);
        String providerList = getProviderAgentListAsString();
        final Vector<Endpoint> peers = new RequesterHelper().getProviders(providerList.split(","));

        new Thread() {
            @Override
            public void run() {
                try {
                    boolean result = true;
                    long startTime = System.currentTimeMillis(); //STATS
                    Capability capability = fetchCapabilityFromRemote(capabilityId, peers);
                    if (capability != null) {
                        repository.setCapability(capabilityId, capability);
                    } else {
                        repository.set(Repository.STATE_PREFIX + capabilityId,
                                Repository.UNAVAILABLE);
                        result = false;
                    }
                    repository.set(Repository.COMMAND_PREFIX + "FIND", "EXECUTED");
                     doCleanup();
                    long fetchTime = (System.currentTimeMillis() - startTime); //STATS
                    logger.fine("Found = " + result + ", Time (ms) = " + fetchTime);
                } catch (Exception e) {
                    logger.severe("Failed to fetch " + capabilityId + e.toString());
                }
            }
        }.start();
    }

    /**
     * Search for and acquire details of capabilities matching the given
     * CapabilitySpecs. No capability instances are acquired as a result of
     * invoking this method.
     *
     * This method is asynchronous in that it spawns a new Thread to do the work
     * and returns. Upon completion, the thread adds an entry to the Repository
     * CMD_FIND=EXECUTED indicating that the task is completed and sends a
     * wakeUp signal to the agent kernel. The kernel uses this entry to identify
     * that it can run the next itinerary command.
     *
     * @param specsList
     */
    @Override
    public void findBySpec(final String[] specsList) {
        logger.info(ownerName + " (iRequester) searching by spec");
        repository.set("BEGIN T_decide", System.currentTimeMillis());  //Instrumenting
        new Thread() {
            @Override
            public void run() {

                RequesterHelper helper = new RequesterHelper();
                CapabilitySpec[] specs = helper.buildCapabilitySpecs(specsList);

                HashMap<CapabilitySpec, Vector<Match>> matchesMap = new HashMap<CapabilitySpec, Vector<Match>>();

                findMatchingCapabilitiesLocally(specs, matchesMap);

                //String providerList = (String) repository.get(Repository.KEY_PROVIDER_LIST);
                String providerList = getProviderAgentListAsString();
                Vector<Endpoint> peers = helper.getProviders(providerList.split(","));
                logger.finest("From [" + providerList + "] following peers alive = " + peers.toString());

                findMatchingCapabilitiesRemotely(specs, matchesMap, peers);

//                logger.fine(" **Matches found are: " + matchesMap.toString());

                repository.set("FindBySpec_Results", matchesMap);
                doCleanup();
            }
        }.start();
    }

    @Override
    public Capability findById(String capabilityId, Serializable provider) {
        if (repository.containsEntry(capabilityId)) {
            logger.fine(capabilityId + " is already available");
            return null;
        }
        logger.info(ownerName + " (iRequester) searching for " + capabilityId);

        //String providerList = (String) repository.get(Repository.KEY_PROVIDER_LIST);
        String providerList = getProviderAgentListAsString();
        final Vector<Endpoint> peers = new RequesterHelper().getProviders(providerList.split(","));
        if (provider instanceof Endpoint) {
            peers.add(0, (Endpoint) provider);
        }
        try {
            Capability capability = fetchCapabilityFromRemote(capabilityId, peers);
            if (capability != null) {
                return capability;
            }
        } catch (Exception e) {
            logger.severe("Failed to find " + capabilityId + e.toString());
        }
        return null;
    }


    public void requestStop() {
        //nothing to stop here
    }

    //------------- private helper methods --------------

    private String getProviderAgentListAsString() {
        if (useYellowPages) {
            ServiceReference ref = context.getServiceReference(AgentIF.class.getName());
            AgentIF agent = (AgentIF) context.getService(ref);
            String[] provs = agent.searchInYellowPages(YELLOWP_TYPE_PROVIDER);
            StringBuilder b = new StringBuilder();
            for (String provStr : provs) {
                if (provStr != null && provStr.trim().length() > 0) {
                    b.append(provStr);
                    b.append(',');
                }
            }
            if (b.length() > 0) {
                return b.substring(0, b.length()-1);
            }
            return b.toString();
        } else {
            return (String) repository.get(KEY_PROVIDER_LIST);
        }
    }

    /**
     * Indicates the completion of the "find" by adding an entry to the
     * repository and sends a wake up signal to the kernel.
     *
     * Fixes BUG 010: "Find command blocks itinerary execution"
     */
    private void doCleanup() {
        repository.set(Repository.COMMAND_PREFIX + "FIND", "EXECUTED");

        ServiceReference ref = context.getServiceReference(AgentIF.class.getName());
        AgentIF agent = (AgentIF) context.getService(ref);
        agent.wakeUpKernel();
    }

    /**
     * Fetches a specific Capability from one of the specified peers.
     * Requests are sent to remote peers until one of them responds with the
     * Capability.
     *
     * @id Identifier of the Capability to fetch
     * @return Capability if successfully fetched, null otherwise
     */
    protected Capability fetchCapabilityFromRemote(String id, Vector<Endpoint> peers) {
        if (peers == null || peers.size() < 1) {
            return null;
        }

        Message capabilityRequest = new Message();
        capabilityRequest.setType(Message.REQUEST_CAPABILITY);
        capabilityRequest.setContent(id);
        RequesterHelper helper = new RequesterHelper();

        for (Endpoint endpoint : peers) {
            Message result = helper.sendRequestAndGetResponse(capabilityRequest, endpoint);
            if (result == null) {
                continue; //no result from that Peer
            }
            Object content = result.getContent();
            if (content != null && (content instanceof Capability)) {
                return (Capability) content;
            }
            logger.fine("Received content did not contain a Capability");
        }
        return null;
    }


    /**
     * Search local repository for capabilities matching the given Specs.
     *
     * @param specsToFind Specs for which matching capabilities are needed
     * @param map Map to populate with found matches
     */
    private void findMatchingCapabilitiesLocally(CapabilitySpec[] specsToFind,
            HashMap<CapabilitySpec, Vector<Match>> map) {

        Collection<Capability> coll = repository.getCapabilities();
        for (CapabilitySpec spec : specsToFind) {
            for (Capability localCapability : coll) {
                if (localCapability.details.match(spec)) {
                    Vector<Match> v4Spec = map.get(spec);
                    if (v4Spec == null) {
                        v4Spec = new Vector<Match>();
                        map.put(spec, v4Spec);
                    }
                    Match m = new Match();
                    m.spec = spec;
                    m.details = localCapability.details;
                    m.provider = Match.LOCAL;
                    v4Spec.add(m);
                }
            }
        }
    }
 
    private void findMatchingCapabilitiesRemotely(
            CapabilitySpec[] specsToFind,
            HashMap<CapabilitySpec, Vector<Match>> aggregatedMatchesMap,
            Vector<Endpoint> peers) {

/*
        StringBuilder builder = new StringBuilder("Find Matching Capabilities for ");
        for (CapabilitySpec spec : specsToFind) {
            builder.append(spec.toString());
            builder.append(" ");
        }
        builder.append(", from ");
        builder.append(peers.size());
        builder.append(" peers.");
        logger.fine(builder.toString());
*/
        Message searchRequest = new Message();
        searchRequest.setType(Message.REQUEST_SEARCH);
        searchRequest.setContent(specsToFind);

        //this method will block until results are received from Peers or a timeout
        Vector<Message> results = new RequesterHelper().sendRequestsAndGetResponses(searchRequest, peers);

        for (Message response : results) {

            if (response == null) {
                continue;
            }
            //retrieve the response contents
            Object content = response.getContent();
            if (content == null || !(content instanceof Vector)) {
                logger.fine("Received content did not contain a Vector");
                continue; //move to next result
            }
            Vector<CapabilityDetails> peerResponsesVector = (Vector<CapabilityDetails>) content;
            if (peerResponsesVector == null || peerResponsesVector.isEmpty()) {
                logger.fine("Received Vector is null/empty");
                continue; //move to next result
            }

            //match and add the results to our Map
            for (CapabilitySpec spec : specsToFind) {

                Vector<Match> v4Spec = aggregatedMatchesMap.get(spec);
                if (v4Spec == null) {
                    v4Spec = new Vector<Match>();
                    aggregatedMatchesMap.put(spec, v4Spec);
                }

                for (CapabilityDetails capabilityDetails : peerResponsesVector) {
                    if (capabilityDetails.match(spec)) {
                        logger.fine("Match found! " + capabilityDetails + " and " + spec);
                        Match m = new Match();
                        m.spec = spec;
                        m.details = capabilityDetails;
                        m.provider = response.getFrom();
                        v4Spec.add(m);
                    }
                }
                logger.fine(v4Spec.size() + " matches found for " + spec);
                logger.info(ownerName + " (iRequester) found " + v4Spec.size() + " remote matches.");
            }
        }
    }
}