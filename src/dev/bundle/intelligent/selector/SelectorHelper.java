/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.selector;

import bundle.intelligent.common.Group;
import bundle.intelligent.common.Node;
import bundle.intelligent.common.Match;
import java.util.HashMap;
import java.util.Stack;
import java.util.Vector;
import mma2.capability.CapabilitySpec;

/**
 * This Helper class allows easily unit testing utility methods without having
 * to build up the OSGi/JADE components required to run the IntelligentSelector
 * class itself.
 * 
 * @author kutila
 * created on 7/06/2010
 */
public class SelectorHelper {

    /**
     * Build the set of possible groups of capability instances to fulfil the
     * given task. It is assumed that the task can be fulfilled by running each
     * "capability spec" instance one after the other in sequence (i.e. there is
     * only one combination of capability specs for a given task).
     *
     * @param data Details of required capability specs and available instances
     * @return A Vector containing the possible groups
     */
    protected Vector<Group> buildGroups(HashMap<CapabilitySpec, Vector<Match>> data) {
        Vector<Group> groupsVec = new Vector<Group>(); //to hold the results

        //order is not guaranteed, and is not important
        Object[] values = data.values().toArray();

        int depth = data.size() + 1; //including the ROOT node

        //initialize ROOT node
        Node rootNode = new Node(values);
        rootNode.setLevel(0);
        rootNode.setValue(null);
        rootNode.setChildren((Vector<Match>) values[0]);

        Stack<Match> groupStack = new Stack<Match>();
        Stack<Node> stack = new Stack<Node>();

        //traverse
        stack.push(rootNode);

        while (!stack.isEmpty()) {
            Node n = (Node) stack.peek();
            Node child = n.getUnvisitedChildNode();
            if (child != null) {
                stack.push(child);
                groupStack.push(child.getValue());
            } else {
                if (n.getLevel() == (depth - 1)) {
                    //a group complete, add it to results Vector
                    Match[] group = new Match[groupStack.size()];
                    for (int j = 0; j < groupStack.size(); j++) {
                        group[j] = groupStack.get(j);
                    }
                    //build group and add to Vector
                    Group g = new Group();
                    g.setContents(group);
                    StringBuilder b = new StringBuilder('(');
                    for (int j = 0; j < group.length; j++) {
                        b.append(group[j].details.getId());
                        b.append(group[j].provider.toString());
                        b.append(' ');
                    }
                    b.append(')');
                    g.setName(b.toString());
                    groupsVec.add(g);
                }
                stack.pop();
                if (!groupStack.isEmpty()) {
                    groupStack.pop();
                }
            }
        }
        return groupsVec;
    }
    
}