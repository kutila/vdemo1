/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.selector;

import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for the Intelligent Capability Selector
 *
 * @author kutila
 * created on 03/06/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private IntelligentSelector selector;
    
    @Override
    public void start(BundleContext context) throws Exception {
        selector = new IntelligentSelector(context);
        selector.start();
        logger.fine("Service started: " + IntelligentSelector.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        selector.requestStop();

        logger.fine("Service stopped: " + IntelligentSelector.class.getName());
    }

}
