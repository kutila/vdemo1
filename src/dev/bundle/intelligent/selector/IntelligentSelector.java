/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.selector;

import bundle.intelligent.common.CostValue.CRITERION;
import bundle.intelligent.common.ConstraintIF;
import bundle.intelligent.common.CostModelIF;
import bundle.intelligent.common.CostModelIF.*;
import bundle.intelligent.common.Group;
import bundle.intelligent.common.IRequesterIF;
import bundle.intelligent.common.IntelligentException;
import bundle.intelligent.common.Match;
import bundle.intelligent.common.RelativeConstraint;

import jade.util.Logger;

import java.io.Serializable;
import java.util.Enumeration;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import mma2.AgentIF;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import mma2.bundles.common.AbstractOneShotClass;
import mma2.bundles.common.RequesterIF;
import mma2.capability.Capability;
import mma2.capability.CapabilityDetails;
import mma2.capability.CapabilitySpec;
import mma2.capability.Repository;
import mma2.itinerary.Command;
import mma2.itinerary.ItineraryService;
import mma2.util.Utility;
 
/**
 * This is a capability with Active one shot execution semantics. The task
 * allocated is to "select" the best available set of capability instances
 * to fulfil the task at hand. The task can be broken down into the following
 * steps:
 *  - build capability instance groups out of the ones identified as being 
 *    available
 *  - build the constraints that apply to current task
 *  - invoke the Cost Evaluation bundle to sort the groups according to cost
 *  - acquire the capability instances for the least cost group
 *  - update the itinerary with concrete tasks to execute
 *
 *
 * @author kutila
 * created on 3/06/2010
 */
class IntelligentSelector extends AbstractOneShotClass {
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private Repository repository;
    private AgentIF agent;

    /** Constructor */
    public IntelligentSelector(BundleContext context) {
        super(context);
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        repository = (Repository) context.getService(ref);
        ref = context.getServiceReference(AgentIF.class.getName());
        agent = (AgentIF) context.getService(ref);
        logger.fine(" created");
    }

    @Override
    public void work() {
        try {
            HashMap<CapabilitySpec, Vector<Match>> resultsMap = (HashMap) repository.get(
                    "FindBySpec_Results");
            //debug1(resultsMap);

            //TODO: validate that the results are not null

            Vector<Group> groups = new SelectorHelper().buildGroups(resultsMap);
            debug2(groups);

            Vector<ConstraintIF> constraints = buildConstraints(resultsMap.keySet());

            Vector<Group> orderedGroups = sortGroups(groups, constraints);
            logger.info(agent.getAgentName() + " (iSelector) has " 
                    + orderedGroups.size() + " sorted alternatives");

            //Instrumenting: time to make decision
            long t1 = (Long) repository.remove("BEGIN T_decide");
            long t2 = System.currentTimeMillis();
            Utility.log("T_decide  (ms) = " + (t2 - t1));

            Match[] loadedMatches = acquireSelectedCapabilities(orderedGroups);
            //resultsMap.keySet();
            updateItinerary(loadedMatches);
            repository.set(Repository.COMMAND_PREFIX + "SELECT", "EXECUTED");

            //Instrumenting: time to acquire selected capabilities
            Utility.log("T_acquire (ms) = " + (System.currentTimeMillis() - t2)); 

        } catch (Exception e) {
            logger.log(Level.SEVERE, "iselector failed. ", e);
            repository.set(Repository.COMMAND_PREFIX + "SELECT", "FAILED");
        }

    }

    /**
     * Relative constraints are specified in the itinerary and piggybacks inside
     * each CapabilitySpecs to this method. It comes in the form of a String array
     * such as the one shown below.
     * {const , TIME:4 , LOAD:3 , ACCURACY:3 , CPU:9 , HEAP:5}
     *
     * TODO:
     * - constraints found with capability instances
     * - implicit constraints identified by sensing the environment
     *
     * @param specs
     * @return
     * @throws IntelligentException
     */
    private Vector<ConstraintIF> buildConstraints(Set<CapabilitySpec> specs) throws IntelligentException {
        Vector<ConstraintIF> vector = new Vector<ConstraintIF>();

        //-build relative constraints
        if (specs == null || specs.isEmpty()) {
            return vector; //no specs, should fail at some point
        }
        for (CapabilitySpec capabilitySpec : specs) {
            String[] conStr = (String[]) capabilitySpec.getConstraints();
            if (conStr == null || conStr.length < 2) {
                return vector; //looks like relative constraint is empty
            }
            RelativeConstraint relCons = new RelativeConstraint();
            for (String str : conStr) {
                String key = str.split(":")[0];
                CRITERION theCriterion;
                if (key.equals("TIME")) {
                    theCriterion = CRITERION.TIME;
                } else if (key.equals("LOAD")) {
                    theCriterion = CRITERION.LOAD;
                } else if (key.equals("ACCURACY")) {
                    theCriterion = CRITERION.ACCURACY;
                } else if (key.equals("CPU")) {
                    theCriterion = CRITERION.CPU;
                } else if (key.equals("HEAP")) {
                    theCriterion = CRITERION.HEAP;
                } else {
                    logger.fine("Unsupported criterion in relative constraints: " + key);
                    continue; //move to next one
                }
                relCons.setPriority(theCriterion, new Float(str.split(":")[1]));
            }
            vector.add(relCons);
            break;//same constraint is duplicated in each CapabilitySpec. No need to loop
        }

        //TEST absolute constraint
//        AbsoluteConstraint absCons = new AbsoluteConstraint();
//        absCons.setType(ConstraintIF.TYPE.ABSOLUTE);
//        absCons.setCriterion1(CRITERION.ACCURACY);
//        absCons.setMin(new CostValue(CRITERION.ACCURACY, 5));
//        absCons.setMax(new CostValue(CRITERION.ACCURACY, 10));
//        vector.add(absCons);

//        AbsoluteConstraint absCons2 = new AbsoluteConstraint();
//        absCons2.setType(ConstraintIF.TYPE.ABSOLUTE);
//        absCons2.setCriterion1(CRITERION.HEAP);
//        absCons2.setMin(new CostValue(CRITERION.HEAP, 0));
//        absCons2.setMax(new CostValue(CRITERION.HEAP, 5096));
//        vector.add(absCons2);

//        AbsoluteConstraint absCons3 = new AbsoluteConstraint();
//        absCons3.setType(ConstraintIF.TYPE.ABSOLUTE);
//        absCons3.setCriterion1(CRITERION.CPU);
//        absCons3.setMin(new CostValue(CRITERION.CPU, 0));
//        absCons3.setMax(new CostValue(CRITERION.CPU, 6));
//        vector.add(absCons3);

//        AbsoluteConstraint absCons4 = new AbsoluteConstraint();
//        absCons4.setType(ConstraintIF.TYPE.ABSOLUTE);
//        absCons4.setCriterion1(CRITERION.LOAD);
//        absCons4.setMin(new CostValue(CRITERION.LOAD, 0));
//        absCons4.setMax(new CostValue(CRITERION.LOAD, 256551));
//        vector.add(absCons4);

//        AbsoluteConstraint absCons5 = new AbsoluteConstraint();
//        absCons5.setType(ConstraintIF.TYPE.ABSOLUTE);
//        absCons5.setCriterion1(CRITERION.TIME);
//        absCons5.setMin(new CostValue(CRITERION.TIME, new Float(0)));
//        absCons5.setMax(new CostValue(CRITERION.TIME, new Float(295.45)));
//        vector.add(absCons5);

//        RelativeConstraint relCons = new RelativeConstraint();
//        relCons.setPriority(CRITERION.TIME, 2.0f);
//        relCons.setPriority(CRITERION.LOAD, 0f);
//        relCons.setPriority(CRITERION.ACCURACY, 0f);
//        relCons.setPriority(CRITERION.CPU, 0f);
//        relCons.setPriority(CRITERION.HEAP, 0f);
//        vector.add(relCons);
        
        return vector;
    }

    /**
     * This method calls a "cost model" bundle to do the actual
     * ordering of the groups according to cost.
     * 
     * @param groups
     * @param constraints
     * @return
     * @throws IntelligentException
     */
    private Vector<Group> sortGroups(
            Vector<Group> groups,
            Vector<ConstraintIF> constraints) throws IntelligentException {

        ServiceReference ref = context.getServiceReference(CostModelIF.class.getName());
        CostModelIF costModel = (CostModelIF) context.getService(ref);

        if (costModel == null) {
            logger.warning("Could not get a CostModelIF bundle. Using random sort");
            return groups;
        }
        costModel.setParameters(getEnvParameters());
        Vector<Group> sorted = costModel.sort(groups, constraints);
        if (sorted == null || sorted.size() == 0) {
            throw new IntelligentException("All alternatives failed to meet constraints");
        } 
        return sorted;
    }

    private Match[] acquireSelectedCapabilities(Vector<Group> orderedGroups)
            throws IntelligentException {
//        System.out.println("We have " + orderedGroups.size() + " to try out.");
        ServiceReference ref = context.getServiceReference(RequesterIF.class.getName());
        IRequesterIF requester = (IRequesterIF) context.getService(ref);

        Match[] obtainedGroup = null;

        HashMap<String, Capability> capabilityMap = new HashMap<String, Capability>();
        boolean success = false;
        for (Group g : orderedGroups) {
            Match[] group = g.getContents();
            success = true;
            if (group == null || group.length == 0) {
                continue; //try the next group
            }
//            StringBuilder b = new StringBuilder();
            for (int i = 0; i < group.length; i++) {
                CapabilityDetails details = group[i].details;
                Serializable provider = group[i].provider;
//                b.append("\r\n i: " + i + " capability: " + details.getId());
                if (capabilityMap.containsKey(details.getId())
                        || (provider instanceof String && provider.equals(Match.LOCAL))) {
//                    b.append(" is locally available.\r\n");
                    continue; //already available, lets go to next capability
                }
                Capability instance = requester.findById(details.getId(), provider);
                if (instance == null) {
//                    b.append(" could not be found. FAIL\r\n");
                    success = false;
                    break; //failed this group, try the next
                } else {
//                    b.append(" found and added to map.\r\n");
                    capabilityMap.put(details.getId(), instance);
                }
            }
//            System.out.println(b.toString());
            if (success) {
                //acquired all capabilities to fulfil this group!
                obtainedGroup = group;
                logger.fine("SUCCESS! " + capabilityMap.toString());
                break;
            }
        }
        if (success) {
            Iterator<String> iterator = capabilityMap.keySet().iterator();
            while (iterator.hasNext()) {
                String capabilityId = iterator.next();
                repository.setCapability(capabilityId, capabilityMap.get(capabilityId));
            }
            logger.fine("Populated repository with capability instances ");
        } else {
            throw new IntelligentException("Failed to acquire all required capabilities.");
        }
        return obtainedGroup;
    }

    /**
     * Retrieves the current itinerary,
     * extracts the itinerary actions to be executed at the current location,
     * discards tasks up to "start iselector"
     * identify START, STOP, UNLOAD actions which match the capability specs
     * replace their parameters with matching capabilit instance names
     * update the itinerary again
     * 
     * @throws IntelligentException
     */
    private void updateItinerary(Match[] loadedMatches) throws IntelligentException {
        String[][] arr = new String[loadedMatches.length][2];
        for (int i = 0; i < loadedMatches.length; i++) {
            arr[i][0] = loadedMatches[i].spec.getAlias();
            arr[i][1] = loadedMatches[i].details.getId();
        }

        String here = agent.isAt();
        ItineraryService service = agent.getItineraryService();
        Command[] commands = (Command[]) service.getItineraryCommands(here);
        logger.fine("updateItinerary() at " + here);

        for (int i = 0; i < commands.length; i++) {
            Command cmd = commands[i];
            switch (cmd.getType()) {
                case START:
                case STOP:
                case UNLOAD:
                    if (cmd.getStatus() == Command.STATUS.CREATED) {
                        String cmdStr = cmd.toString();
                        for (int j = 0; j < arr.length; j++) {
                            String specName = cmd.getAssociatedBundleName();
                            if (specName.equals(arr[j][0])) {
                                //only the associated bundle name is changed to
                                //the actual name. other places still refer to the
                                //Spec's alias. This works for now, but may cause
                                //issues later if these others are also used.
                                cmd.setAssociatedBundleName(arr[j][1]);
                                System.out.println(" Updated command is " + cmd.toString());
                            }
                        }
                        service.setItineraryCommands(here, commands);
                    }
                    break;
                case MOVE:
                case FIND:
                case TERMINATE:
                case NULL:
                default:
            }
        }
    }

    /**
     * Retrieve environmental parameters. These could be accessed from another
     * context client capability, read from system properties, or read from some
     * external source such as a socket.
     *
     * @return
     */
    private HashMap getEnvParameters() {
        //TEMPORARY: read from repository
//        HashMap map = new HashMap();
//        map.put("localhost:LATENCY", new Long((String) repository.get("LATENCY")));
//        map.put("localhost:BANDWIDTH", new Float((String) repository.get("BANDWIDTH")));
//        map.put("130.194.70.98:LATENCY", new Long((String) repository.get("LATENCY")));
//        map.put("130.194.70.98:BANDWIDTH", new Float((String) repository.get("BANDWIDTH")));
//        return map;

//        ServiceReference ref = context.getServiceReference(ContextClient.class.getName());
//        ContextClient contextClient = (ContextClient) context.getService(ref);
//        return contextClient.getContextParameters();

        //read environmental parameters from the System Properties [2010-07-12]
        HashMap map = new HashMap();
        Properties p = System.getProperties();
        Enumeration en = p.propertyNames();
        while (en.hasMoreElements()) {
            String key = (String) en.nextElement();
            if (key.startsWith("BANDWIDTH:")) {
                Float f = new Float(p.getProperty(key, "0"));
                map.put(key, f);
            } else if (key.startsWith("LATENCY:")) {
                Long l = new Long(p.getProperty(key, "0"));
                map.put(key, l);
            }
            //TO DO - add more as other context parameters are put in
        }

        //read agent parameters from repository
        map.put(CostModelIF.REQUEST_SIZE, new Integer((String) repository.get(CostModelIF.REQUEST_SIZE))); //bytes
        map.put(CostModelIF.MSG_OVERHEAD_SIZE, new Integer((String) repository.get(CostModelIF.MSG_OVERHEAD_SIZE))); //bytes
        map.put(CostModelIF.TIME_TO_START, new Long((String) repository.get(CostModelIF.TIME_TO_START))); //ms

        return map;

    }

    private void debug1(HashMap<CapabilitySpec, Vector<Match>> resultsMap) {
        StringBuilder builder = new StringBuilder();
        Set<CapabilitySpec> specs = resultsMap.keySet();
        for (CapabilitySpec spec : specs) {
            Vector<Match> match = resultsMap.get(spec);
            builder.append(spec.toString());
            builder.append(" : ");
            builder.append(match.toString());
            builder.append("\r\n");
        }
        logger.fine("Retrieved " + builder.toString());

//        try {
//            Thread.sleep(20);
//        } catch (InterruptedException e) {
//        }
    }
    private void debug2(Vector<Group> groups) {
        for (Group group : groups) {
            System.out.println(" " + group.toString());
        }
    }


}
