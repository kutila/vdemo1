/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

/**
 * Represents a constraint which applies to an itinerary.
 *
 * @author kutila2
 * created on 09/06/2010
 */
public interface ConstraintIF {

    public static enum TYPE {RELATIVE, ABSOLUTE};

    public TYPE getType();

}
