/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import java.util.HashMap;
import java.util.Vector;

/**
 * Interface to be implemented when creating concrete cost model bundles.
 *
 * @author kutila
 * created on 13/06/2010
 */
public interface CostModelIF {

    public static final String MSG_OVERHEAD_SIZE = "MSG_OVERHEAD_SIZE";
    public static final String REQUEST_SIZE = "REQUEST_SIZE";
    public static final String TIME_TO_START = "TIME_TO_START";

    /**
     * Sort the given set of alternatives in decreasing order of suitability.
     * Use the specified set of environmental parameters to aid the sorting.
     *
     * @param alternatives The alternatives to be sorted
     * @param constraints Constraints to be used in the ordering
     * @param envParams Environmental parameters to aid the sorting
     * @return the sorted alternatives, best first.
     */
    public Vector sort(Vector<Group> alternatives, Vector<ConstraintIF> constraints);

    /**
     * Used to set any environmental (and other) parameters which maybe useful in 
     * the sorting process. Should be called prior to invoking the sort() method.
     * 
     * @param params
     */
    public void setParameters(HashMap params);

}
