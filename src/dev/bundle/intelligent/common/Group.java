/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

/**
 * Holds a group of matches. Represents a possible alternative for the 
 * agent's decision making process.
 *
 * @author kutila
 * created on 21/06/2010
 */
public class Group {

    private String name;
    private Match[] contents;

    public Match[] getContents() {
        return contents;
    }

    public void setContents(Match[] contents) {
        this.contents = contents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

}
