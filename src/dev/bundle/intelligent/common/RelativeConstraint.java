/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import bundle.intelligent.common.CostValue.CRITERION;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;

/**
 *
 * @author kutila2
 * created on 21/06/2010
 */
public class RelativeConstraint implements ConstraintIF {

    private TYPE type = ConstraintIF.TYPE.RELATIVE;
    private Hashtable<CRITERION, Float> priorities = new Hashtable<CRITERION, Float>();

    public RelativeConstraint() {
    }

    public TYPE getType() {
        return type;
    }

    public Float getPriority(CRITERION criterion) {
        return priorities.get(criterion);
    }

    public void setPriority(CRITERION criterion, Float priority) {
        if (priority != null) {
            priorities.put(criterion, priority);
        }
    }

    /**
     * Retrieve the criteria which are identified as relevant.
     * @return
     */
    public Set<CRITERION> getCriteria() {
        return priorities.keySet();
    }


    public void normalize() {
        float sum = 0f;
        Enumeration<CRITERION> en = priorities.keys();
        while (en.hasMoreElements()) {
            CRITERION c = en.nextElement();
            sum = sum + priorities.get(c);
        }
        en = priorities.keys();
        while (en.hasMoreElements()) {
            CRITERION c = en.nextElement();
            float f = priorities.get(c)/sum;
            priorities.put(c, f);
        }
    }

    public void reset() {
        priorities.clear();
    }

}
