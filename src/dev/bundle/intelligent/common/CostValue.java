/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

/**
 *
 *
 * @author kutila
 * created on 14/06/2010
 */
public class CostValue {

    public static enum CRITERION {TIME, LOAD, HEAP, CPU, ACCURACY};

    private CRITERION type;

    private Object value;

    /** Time when the cost value was estimated */
    private long timestamp;

    public CostValue() {
    }

    public CostValue(CRITERION type, Object value) {
        this.type = type;
        this.value = value;
        this.timestamp = System.currentTimeMillis();
    }


    public CRITERION getCriterion() {
        return type;
    }

    public void setCriterion(CRITERION type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


    /**
     * This method has a signature similar to the compareTo() method in the
     * Comparable interface.
     * However, if the two values are of different types, the return value is
     * -9999.
     * 
     * @param another
     * @return
     */
    public int compareTo(CostValue another) {
        if (value instanceof Integer && another.value instanceof Integer) {
            Integer me = (Integer) value;
            return me.compareTo((Integer) another.value);
        } else if (value instanceof Long && another.value instanceof Long) {
            Long me = (Long) value;
            return me.compareTo((Long) another.value);
        } else if (value instanceof Float && another.value instanceof Float) {
            Float me = (Float) value;
            return me.compareTo((Float) another.value);
        } else {
            return -9999;
        }
    }

    public String toString() {
        StringBuilder b = new StringBuilder('(');
        b.append(type);
        b.append(':');
        b.append(value);
        b.append(')');

        return b.toString();
    }

}
