/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import java.io.Serializable;
import mma2.capability.CapabilityDetails;
import mma2.capability.CapabilitySpec;

/**
 *
 * @author kutila
 * created on 4/06/2010
 */
public class Match implements Serializable {

    public static final String LOCAL = "local";

    public CapabilityDetails details;
    public Serializable provider;
    public CapabilitySpec spec;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Match{");
        builder.append(spec.toString());
        builder.append(" , ");
        builder.append(details.toString());
        builder.append(" , ");
        builder.append(provider.toString());
        builder.append("} ");

        return builder.toString();
    }

}