/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

/**
 *
 * @author kutila
 * created on 10/06/2010
 */
public class IntelligentException extends Exception {

    public IntelligentException(String msg) {
        super(msg);
    }

}
