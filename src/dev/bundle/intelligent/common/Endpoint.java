/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import java.io.Serializable;
import java.net.Socket;

/**
 *
 * @author kutila
 * created on 31/05/2010
 */
public class Endpoint implements Serializable {

    private int port;
    private String address;

    public Endpoint() {
    }

    public Endpoint(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    /**
     * This method makes a socket connection to the end-point to check whether
     * it is alive.
     *
     * @return
     */
    public boolean isAlive() {
        boolean result = false;
        Socket sock = null;
        try {
            sock = new Socket(address, port);
            result = true;
        } catch (Exception ex) {
            System.err.println(address + ":" + port + " is not Alive. " + ex.toString()); //DEBUG
            //ignore
        } finally {
            try {
                if (sock != null) {
                    sock.close();
                }
            } catch (Exception e) {}
        }
        return result;
    }
    
    @Override
    public String toString() {
        StringBuffer b = new StringBuffer("[");
        b.append(address);
        b.append(":");
        b.append(port);
        b.append("]");
        return b.toString();
    }

}
