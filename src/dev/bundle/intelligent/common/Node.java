/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import bundle.intelligent.common.Match;
import java.util.Vector;

/**
 * A Node is used in the process of building groups from the available
 * CapabilityDetails. The group building process uses a depth-first-traversal.
 * 
 * @author kutila
 * created on 7/06/2010
 */
public class Node {

    //information about the tree
    private int depth;
    private Object[] values;


    private Match value; //this node's value

    private int level; //this node's level

    //all elements at the next lower level
    private Vector<Match> children = null;

    //next element in the child level to visit
    private int ptr = 0;

    private boolean allChildrenVisited = false;

    /** Constructor */
    public Node(Object[] values) {
        this.values = values;
        this.depth = values.length + 1; //additional level due to ROOT node
    }

    public Node getUnvisitedChildNode() {
        if (children == null || allChildrenVisited) {
            return null;
        } 

        //make node for Child
        Node childNode = new Node(values);
        childNode.level = level + 1;
        childNode.value = children.get(ptr);
        if (childNode.level == (depth-1)) {
            childNode.children = null;
        } else {
            childNode.children = (Vector<Match>) values[childNode.level];
        }

        //update self for "next" child
        if (ptr == children.size() - 1) {
            allChildrenVisited = true;
        } else {
            ptr++;
        }

        return childNode;
    }

    public Vector<Match> getChildren() {
        return children;
    }

    public void setChildren(Vector<Match> children) {
        this.children = children;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Match getValue() {
        return value;
    }

    public void setValue(Match value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return value.toString();
    }
}
