/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * @author kutila2
 * created on 8/06/2010
 */
public class Activator implements BundleActivator  {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    @Override
    public void start(BundleContext context) throws Exception {
        logger.fine("Service started: icommon");
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        logger.fine("Service stopped: icommon");
    }

}