/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import java.io.Serializable;
import mma2.capability.Capability;

/**
 *
 * @author kutila
 * created on 09/06/2010
 */
public interface IRequesterIF {

    /**
     * Used to request a Capability instance be acquired and returned to the
     * caller rather than being added to the repository.
     *
     * @param id Identifier of the capability needed
     * @param provider Ask this provider first
     * @return The Capability instance or null if not found
     */
    Capability findById(String capabilityId, Serializable provider);

}
