/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

/**
 * A place holder for constant values that are used in the "bundle.intelligent"
 * package capabilities.
 *
 * @author kutila
 */
public interface ConstantsIF {

    /** Type of capability provider service used to register in platform yellow pages */
    public static final String YELLOWP_TYPE_PROVIDER = "provider_agent";

    /** Key used when provider agents are specified in the agent's Repository. */
    public static final String KEY_PROVIDER_LIST = "provider_list";

}
