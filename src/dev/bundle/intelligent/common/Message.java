/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import java.io.Serializable;

/**
 *
 * @author kutila
 * created on 26/05/2010
 */
public class Message implements Serializable {

    public static int REQUEST_SEARCH = 11;
    public static int RESPONSE_SEARCH = 21;
    public static int REQUEST_CAPABILITY = 12;
    public static int RESPONSE_CAPABILITY = 22;
    public static int RESPONSE_ERROR = 29;

    private int type;
    private Serializable content;

    private Endpoint from;

    public Serializable getContent() {
        return content;
    }

    public void setContent(Serializable content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Endpoint getFrom() {
        return from;
    }

    public void setFrom(Endpoint from) {
        this.from = from;
    }

    @Override
    public String toString() {
        StringBuffer b = new StringBuffer("[");
        b.append(type);
        b.append(",");
        if (from != null) {
            b.append(from.getAddress());
            b.append(":");
            b.append(from.getPort());
            b.append(",");
        }

        b.append(content.toString());
        b.append("]");
        return b.toString();
    }


}
