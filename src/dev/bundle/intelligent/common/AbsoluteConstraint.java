/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.intelligent.common;

import bundle.intelligent.common.CostValue.CRITERION;

/**
 *
 * @author kutila2
 * created on 17/06/2010
 */
public class AbsoluteConstraint implements ConstraintIF {

    private TYPE type;
    private CRITERION criterion1;

    private CostValue min;
    private CostValue max;

    public AbsoluteConstraint() {
    }

    public CostValue getMax() {
        return max;
    }

    public void setMax(CostValue max) {
        this.max = max;
    }

    public CostValue getMin() {
        return min;
    }

    public void setMin(CostValue min) {
        this.min = min;
    }

    public CRITERION getCriterion1() {
        return criterion1;
    }

    public void setCriterion1(CRITERION criterion1) {
        this.criterion1 = criterion1;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public TYPE getType() {
        return type;
    }

    public boolean satisfy(CostValue actual) {
        if (actual.getCriterion() != criterion1) {
            return false;
        }
        if ((min.compareTo(actual) > 0) || max.compareTo(actual) < 0) {
            return false;
        }
        return true;
    }


}
