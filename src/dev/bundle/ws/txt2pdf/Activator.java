/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ws.txt2pdf;

import mma2.capability.Repository;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Bundle Activator for "txt2pdf" conversion web service client bundle
 * Parameter needed for this is:
 *  - FILEDIR
 *
 * Execution type: ACTIVE_ONESHOT
 * - a new Thread is invoked. It completes and then terminates.
 *
 * @author kutila
 * created on 9/10/2009
 */
public class Activator implements BundleActivator {

    public static final String FILE_LOCATION = "FILEDIR";

    private String txtFileLocation = "D:\\dev\\workspace\\vdemo1\\conf\\testfiles";
    WSTxt2PdfWorker theThread;

    public void start(BundleContext context) throws Exception {
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        Repository repository = (Repository) context.getService(ref);
        txtFileLocation = (String) repository.get(FILE_LOCATION);
        if (txtFileLocation == null || txtFileLocation.length() == 0) {
            System.out.println(FILE_LOCATION + " was empty");
            //return;
            //to quicken running experiments [2009-10-12]
            txtFileLocation = "C:\\dev\\files";
        }

        theThread = new WSTxt2PdfWorker(context);
        theThread.setTxtFileLocation(txtFileLocation);
        theThread.start();
        System.out.println("Started " + this.getClass().getName());
    }

    public void stop(BundleContext context) throws Exception {
        if (theThread != null)
            theThread.requestStop();
        System.out.println("Stopped " + this.getClass().getName());
    }

}
