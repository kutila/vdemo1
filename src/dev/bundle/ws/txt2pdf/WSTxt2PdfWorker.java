/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ws.txt2pdf;

import jade.util.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import mma2.bundles.common.AbstractOneShotClass;

import mma2.util.Utility;

import org.osgi.framework.BundleContext;

/**
 * Refactored from WSTxt2PdfThread.java
 *
 * Thread reads and converts txt files to pdf by using
 * the web service
 *
 * @author kutila2
 * created on 24/08/2010
 */
public class WSTxt2PdfWorker extends AbstractOneShotClass {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    private String txtFileLocation = "";
    private boolean stopRequest = false;

    public WSTxt2PdfWorker(BundleContext context) {
        super(context);
    }

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    public void setTxtFileLocation(String t) {
        txtFileLocation = t;
    }

    @Override
    public void work() {
        long startTime = System.currentTimeMillis();

        File dir = new File(txtFileLocation);
        String[] files2Process = dir.list(
                new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.endsWith(".txt");
                    }
                }
        );

        if (files2Process == null || files2Process.length == 0) {
            logger.fine("No files to process");
        } else {
            for (String filename : files2Process) {
                workOne(txtFileLocation + "\\" + filename);
            }
        }
        Utility.log("WSTxt2Pdf: Processing time (ms) = "
                + (System.currentTimeMillis() - startTime));
    }


    private void workOne(String filename) {
        logger.finest("Client starting");
        byte[] data = fileToBytes(filename);
        if (data == null || data.length == 0) {
            logger.fine(filename + " was empty");
            return;
        }

        try { // Call Web Service Operation
            versag.BinaryWS service = new versag.BinaryWS();
            versag.BinaryWSPortType port = service.getBinaryWSHttpSoap12Endpoint();

            long l = 0;
            byte[] result = port.process(data, l);
            FileOutputStream fout = new FileOutputStream(filename + ".pdf");
            fout.write(result);
            fout.close();
            logger.finest("Result was written to " + filename);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private byte[] fileToBytes(String filename) {
        FileInputStream in = null;
        try {
            in = new FileInputStream(filename);
            int len = (int) new File(filename).length();
            byte[] b = new byte[len];

            int offset = 0;
            int numRead = 0;
            while (offset < b.length && (numRead = in.read(b, offset, b.length - offset)) >= 0) {
                offset += numRead;
            }
            if (offset < b.length) {
                return null;
            }
            return b;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
            }
        }
        return null;
    }


}
