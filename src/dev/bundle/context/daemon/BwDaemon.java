/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.daemon;


import jade.util.Logger;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;


/**
 *
 * @author kutila2
 * created on 28/06/2010
 */
public class BwDaemon extends Thread {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private boolean stop = false;
    private ServerSocket socket;

    private int port;
    static int bufSize = 1024*64; //64KB
    static byte[] responseBuf = new byte[bufSize];

    public BwDaemon(int port) {
        this.port = port;

        //populate buffer with random data
        java.util.Random random = new java.util.Random();
        random.nextBytes(responseBuf);
    }

    @Override
    public void run() {
        Socket sock = null;

        try {
            ServerSocketChannel svrChannel = ServerSocketChannel.open();
            ServerSocket server = svrChannel.socket();
            server.bind(new InetSocketAddress(port));

            logger.fine("Bandwidth daemon started on port " + port);
            //work in an infinite loop
            while (!isStopRequested()) {
                WorkingThread wp = new WorkingThread(null);

                SocketChannel socketChannel = svrChannel.accept();
                sock = socketChannel.socket(); //got a request

                //hand over serving the client to a new thread.
                wp.setSocket(sock);
                wp.start();
            }
        } catch (ClosedByInterruptException e) {
            //expected when the Thread is to be stopped
            //System.out.println("cbie received!!!");
        } catch (Exception e) {
            logger.warning("Bandwidth daemon stopping due to " + e.toString());
        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (Exception e) {
                }
            }
        }
        logger.fine("Bandwidth daemon shutting down");
    }

    public synchronized boolean isStopRequested() {
        return stop;
    }

    public synchronized void requestStop() {
        stop = true;
        this.interrupt();
    }


    public static void main(String[] args) {
        BwDaemon s = new BwDaemon(1111);
        s.start();
    }
}

/**
 * This class serves a single request and dies.
 */
class WorkingThread extends Thread {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private Socket sock;

    public WorkingThread(Socket s) {
        this.sock = s;
    }

    public void setSocket(Socket s) {
        this.sock = s;
    }

    @Override
    public void run() {
        OutputStream out = null;
        try {
            //ignore any incoming data, just send the response asap!
            out = new BufferedOutputStream(sock.getOutputStream());
            out.write(BwDaemon.responseBuf);
            out.write(-1);
            out.flush();
            try {Thread.sleep(4000); } catch (InterruptedException e) {}
        } catch (Exception e) {
            logger.warning("Error while handling request" + e.toString());
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (sock != null) {
                    sock.close();
                }
            } catch (IOException ioe) {/* ignore */}
        }
    }
}