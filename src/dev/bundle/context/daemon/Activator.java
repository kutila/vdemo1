/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.daemon;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for the Context daemons. Starts two threads which function
 * as Servers for calculating bandwidth and latency.
 *
 * @author kutila
 * created on 27/06/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private BwDaemon bandwidthDaemon;
    private PingDaemon pingDaemon;
    
    @Override
    public void start(BundleContext context) throws Exception {
        pingDaemon = new PingDaemon(1111);
        bandwidthDaemon = new BwDaemon(1112);

        pingDaemon.start();
        bandwidthDaemon.start();

        logger.fine("Service started: ContextDaemons");
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        pingDaemon.requestStop();
        bandwidthDaemon.requestStop();

        logger.fine("Service stopped: ContextDaemons");
    }

}
