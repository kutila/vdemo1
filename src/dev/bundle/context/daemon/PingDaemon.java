/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.daemon;

import jade.util.Logger;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.DatagramChannel;

/*
 * A daemon which runs as a "Ping" server to be used by others
 * to measure network latency.
 * 
 */
public class PingDaemon extends Thread {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private boolean stop = false;
    private int port;
    private DatagramSocket socket;

    public PingDaemon(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            DatagramChannel channel = DatagramChannel.open();
            socket = channel.socket();
            socket.bind(new InetSocketAddress(port));
            logger.fine("Ping daemon started on port " + port);
            while (!isStopRequested()) {
                ByteBuffer bbuf = ByteBuffer.wrap(new byte[1024]);
                // block until the host receives a UDP packet
                SocketAddress clientHost = channel.receive(bbuf);
                channel.send(bbuf, clientHost); //send the response
                
//                logger.fine("Responded to request from " + clientHost);
            }
            socket.close();
        } catch (ClosedByInterruptException e) {
            //expected when the Thread is to be stopped
            //System.out.println("cbie received!!!");
        } catch (Exception e) {
            logger.warning("Ping daemon stopping due to " + e.toString());
        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (Exception e) {
                }
            }
        }
        logger.fine("Ping daemon shutting down");
    }

    public synchronized boolean isStopRequested() {
        return stop;
    }

    public synchronized void requestStop() {
        this.interrupt();
        stop = true;
    }

    public static void main(String[] args) {
        PingDaemon s = new PingDaemon(1111);
        s.start();
    }

}
