/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.api;

import jade.util.Logger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import mma2.AgentIF;

import mma2.bundles.common.ServiceIF;
//import mma2.capability.Repository;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Main class of the contextapi bundle. This is an agent's context sensing
 * client API. It runs at periodic intervals and measures environmental
 * parameters.
 * Bandwidth and latency to all listed providers are measured at present.
 *
 * @author kutila
 * created on 30/06/2010
 */
public class ContextClient extends Thread  implements ServiceIF {

    public static final String BANDWIDTH = "BANDWIDTH";
    public static final String LATENCY = "LATENCY";
    public static final String PATH_SEP = ":";

    //TODO: CAPABILITY_PROVIDER is duplicated in multiple locations
    private static final String CAPABILITY_PROVIDER = "provider_agent";

    private static int SLEEP_INTERVAL = 5*60*1000; //5 minutes
    private static int PING_PORT = 1111;
    private static int BW_PORT = 1112;

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private BundleContext context;
//    private Repository repository;
    private boolean stop = false;

    //contain contextual information
    private HashMap<String, Object> contextCache;

    /** Constructor */
    ContextClient(BundleContext context) {
        this.context = context;
        //setupLogging();

//        ServiceReference ref = context.getServiceReference(Repository.class.getName());
//        repository = (Repository) context.getService(ref);
        contextCache = new HashMap<String, Object>();
    }

    @Override
    public void run() {
        logger.fine("STARTING");
        updatePathsOfInterest();
        updateSystemProperties();
        try {
            while (!isStopRequested()) {
                logger.fine("Context parameters are : " + contextCache.toString());
                //sleep before next update
                try {Thread.sleep(SLEEP_INTERVAL); } catch (InterruptedException e) {}

                //update context info for all the ones in the cache
                Set<String> keys = contextCache.keySet();
                for (String path : keys) {
                    String[] params = path.split(PATH_SEP); //e.g. 130.194.70.98:BANDWIDTH
                    String host = params[1];
                    String type = params[0];
                    if (type.equals(BANDWIDTH)) {
                        long latency = ContextHelper.calculateLatency(host, PING_PORT, 4);
                        long t1 = System.currentTimeMillis(); //Instrumenting
                        float bw = ContextHelper.calculateBandwidth(host, BW_PORT, latency, 4);
                       logger.config("Time to measure bw (ms) = " +
                               (System.currentTimeMillis() - t1)); //Instrumenting
                        contextCache.put(path, bw);
                        logger.fine(path + " updated to " + bw);
                    } else if (type.equals(LATENCY)) {
                        long latency = ContextHelper.calculateLatency(host, PING_PORT, 4);
                        contextCache.put(path, latency);
                        logger.fine(path + " updated to " + latency);
                    } else {
                        logger.fine("Unknown context information type: " + type);
                    }
                }
                updateSystemProperties();
            } //end while
        } catch (Exception e) {
            logger.log(Level.WARNING,"Error in context client", e);
        }
        logger.fine("STOPPING");
    }


    /**
     * @deprecated Context parameters should be passed via System.Properties
     * @return
     */
    public HashMap getContextParameters() {
        return contextCache;
    }

    public synchronized boolean isStopRequested() {
        return stop;
    }

    public synchronized void requestStop() {
        this.interrupt();
        stop = true;
    }

    private void updatePathsOfInterest() {
        String[] providers = searchInPlatformYellowPages();
        if (providers == null || providers.length == 0) {
            return;
        }
        for (String provider : providers) {
            String host = provider.split(":")[0];
            long latency;
            try {
                latency = ContextHelper.calculateLatency(host, PING_PORT, 4);
                contextCache.put(LATENCY + PATH_SEP + host, latency);

                long t1 = System.currentTimeMillis(); //Instrumenting
                float bw = ContextHelper.calculateBandwidth(host, BW_PORT, latency, 4);
                contextCache.put(BANDWIDTH + PATH_SEP + host, bw);
                logger.config("Time to measure bw (ms) = " + (System.currentTimeMillis() - t1)); //Instrumenting
                logger.fine("Completed initial measurement of bw and latency to " + host);
            } catch (ContextException ex) {
                logger.fine("Error in context estimation: " + ex.toString());
            }
        }
    }

    /**
     * Setup the logging to write to a separate log file. This is to reduce
     * cluttering the console and standard log file.
     */
    private void setupLogging() {
        try {
            Logger ctxLogger = Logger.getMyLogger("bundle.context");
            FileHandler f = new FileHandler("context.log", true);
            ctxLogger.addHandler(f);
            ctxLogger.setUseParentHandlers(false);
        } catch (Exception e) {
            logger.log(Level.WARNING, "adding a new FileHandler failed.", e);
        }
    }

    /**
     * Save contents of the contextCache as System.Properties
     */
    private void updateSystemProperties() {
        if (contextCache == null || contextCache.isEmpty()) {
            return;
        }
        Iterator it = contextCache.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            System.setProperty(key, contextCache.get(key).toString());
        }
    }

    private String[] searchInPlatformYellowPages() {
        ServiceReference ref = context.getServiceReference(AgentIF.class.getName());
        AgentIF agent = (AgentIF) context.getService(ref);
        return agent.searchInYellowPages(CAPABILITY_PROVIDER);
    }
}
