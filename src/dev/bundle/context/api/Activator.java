/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.api;

import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for Context client.
 * This bundle has CYCLIC execution semantics and periodically updates context
 * information it holds.
 * It is registered as a service and other bundles can call its methods to
 * retrieve context information.
 *
 * @author kutila
 * created on 30/06/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private ContextClient ctxClient;
    
    @Override
    public void start(BundleContext context) throws Exception {
        ctxClient = new ContextClient(context);
        ctxClient.start();
        serviceRegistration =context.registerService(
                ContextClient.class.getName(),
                ctxClient, null);
        logger.fine("Service started: " + ContextClient.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        try {
            serviceRegistration.unregister();
        } catch (Exception e) {
            //TODO: Fix the cause for throwing of this exception, possibly in Concierge
            //e.printStackTrace();
            logger.warning("Service de-registration threw an exception " + e.toString());
        }
        ctxClient.requestStop();

        logger.fine("Service stopped: " + ContextClient.class.getName());
    }

}
