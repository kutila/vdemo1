/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.api;

import java.io.Serializable;

/**
 *
 * @author kutila2
 * created on 30/06/2010
 */
public class ContextParameter {

    private String key;
    private Serializable value;
    private String unit;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Serializable getValue() {
        return value;
    }

    public void setValue(Serializable value) {
        this.value = value;
    }




}
