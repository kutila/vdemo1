/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.api;

/**
 *
 * @author kutila2
 * created on 30/06/2010
 */
public class ContextException extends Exception {

    public ContextException(String msg) {
        super(msg);
    }
}
