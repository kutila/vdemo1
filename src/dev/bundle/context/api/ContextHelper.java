/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.context.api;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 *
 * @author kutila2
 * created on 30/06/2010
 */
public class ContextHelper {

    public static final int EXPECTED_DATASIZE = 1024 * 64; //64KB
    private static int SOCKET_TIMEOUT = 12 * 1000; //12 seconds
    private static int MAX_TIMEOUTS = 4;

    /**
     * 
     *
     * @param pingServer host where Ping server is located
     * @param port port number that the Ping server is listening on
     * @param loopCount number of pings to use
     * @return the latency in milliseconds
     * @throws ContextException if an error occurs
     */
    public static long calculateLatency(String pingServer, int port, int loopCount)
            throws ContextException {
        try {
            InetAddress pingSvr = InetAddress.getByName(pingServer);
            String s = "PING" + System.nanoTime();

            byte[] buf = s.getBytes();
//            System.out.println("Starting: msg size is " + buf.length + " bytes");
            DatagramPacket request = new DatagramPacket(buf, buf.length, pingSvr, port);
            DatagramPacket response = new DatagramPacket(new byte[1024], 1024);

            DatagramSocket socket = new DatagramSocket();
            socket.setSoTimeout(SOCKET_TIMEOUT);
            long cumulativeRtt = 0;
            int timeoutCount = 0;
            int i = 0;
            while (i < loopCount && timeoutCount < MAX_TIMEOUTS) {
                long startTime = System.nanoTime();
                socket.send(request);
                try {
                    socket.receive(response);
                    cumulativeRtt += (System.nanoTime() - startTime);
                } catch (SocketTimeoutException ste) {
                    System.err.println("Time out!");
                    timeoutCount++;
                    if (timeoutCount == MAX_TIMEOUTS) {
                        throw new ContextException("Failed latency calculation: Too many timeouts");
                    }
                    continue;
                }
                i++;
            }
            long rtt = cumulativeRtt / loopCount;
//            System.out.println("RTT (ns) = " + rtt);
            return rtt / 2000000; //convert to milliseconds and halve for latency
        } catch (Exception e) {
            e.printStackTrace();
            throw new ContextException("Failed to calculate latency due to: " + e.getMessage());
        }
    }

    /**
     *
     * @param bwServer host/ip of server
     * @param port server port
     * @param latency previously calculated latency in milliseconds
     * @param loopCount number of iterations to run
     * @return calculated bandwidth
     * @throws ContextException
     */
    public static float calculateBandwidth(String bwServer, int port, long latency, int loopCount)
            throws ContextException {
        try {
            byte[] request = new String("PROBE").getBytes();
            byte[] response = new byte[1024 * 1024];

            Socket sock;
            OutputStream out;
            InputStream in;
            long startTime;

            float cumulativeBandwidth = 0;
            int i = 0;
            while (i < loopCount) {
                sock = new Socket(bwServer, port);
                sock.setSoTimeout(SOCKET_TIMEOUT);

                out = new BufferedOutputStream(sock.getOutputStream());
                in = new BufferedInputStream(sock.getInputStream());

                //System.currentTimeMillis() does not give the required accuracy.
                startTime = System.nanoTime();

                out.write(request); //send PROBE
                out.flush();

                //read incoming data
                int readCount = 0;
                int totalBytesReceived = 0;
                while (readCount > -1 && totalBytesReceived < (EXPECTED_DATASIZE)) {
                    readCount = in.read(response);
                    totalBytesReceived += readCount;
                    //System.out.println("total read " + totalRead);
                }
                float responseTime = (System.nanoTime() - startTime) / 1000000;
                if (responseTime > latency) {
                    responseTime = responseTime - latency;
                }
//                System.out.println("response time (ms) = " + responseTime);
//                System.out.println("total data    (B)  = " + totalBytesReceived);
                float bw = totalBytesReceived / responseTime;
//                System.out.println(i + " bandwidth        = " + bw);
                cumulativeBandwidth += bw;

                try {
                    sock.close();
                } catch (Exception e) {}
                i++;
            }

            float bandwidth = cumulativeBandwidth / loopCount;
            if (bandwidth < 0) {
                throw new ContextException("Calculation error: bw cannot be negative");
            }
            return bandwidth;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ContextException("Failed to calculate bandwidth due to: " + e.getMessage());
        }
    }
}
