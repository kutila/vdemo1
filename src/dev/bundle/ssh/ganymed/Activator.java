/*
 * VERSAG project
 */
package bundle.ssh.ganymed;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for SSH client bundle based on Ganymed
 *
 * @author kutila
 * created on 28/08/2009
 */
public class Activator implements BundleActivator  {


    private ServiceRegistration serviceRegistration;
    private SshShell theService;

    @Override
    public void start(BundleContext context) throws Exception {
        theService = new SshShell(context);
        serviceRegistration =context.registerService(
                SshShell.class.getName(),
                theService, null);
        theService.start();
        debug("Service started: " + SshShell.class.getName());
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        theService.requestStop();
        debug("Service stopped: " + SshShell.class.getName());
    }

    private void debug(String msg) {
        System.out.println(msg);
    }

}