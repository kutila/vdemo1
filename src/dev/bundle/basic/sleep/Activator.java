/*
 * VERSAG project
 */
package bundle.basic.sleep;

import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator Idler (i.e. sleep bundle)
 *
 * @author kutila
 * created on 20/09/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    @Override
    public void start(BundleContext context) throws Exception {
        Idler idler = new Idler(context);
        idler.start();
        logger.fine("Started: " + Idler.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        logger.fine("Stopped: " + Idler.class.getName());
    }

}
