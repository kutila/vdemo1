/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.basic.sleep;

import java.util.logging.Level;
import jade.util.Logger;
import org.osgi.framework.BundleContext;
import mma2.bundles.common.AbstractOneShotClass;
import mma2.capability.Repository;
import org.osgi.framework.ServiceReference;
 
/**
 *
 *
 * @author kutila
 * created on 20/09/2010
 */
class Idler extends AbstractOneShotClass {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    private long sleepSecs = 10;
    private static final String SLEEP_TIME = "SLEEP_TIME";

    /** Constructor */
    public Idler(BundleContext context) {
        super(context);
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        Repository repository = (Repository) context.getService(ref);
        try {
            sleepSecs = Long.parseLong((String) repository.get(SLEEP_TIME));
        } catch (Exception e) {
            logger.warning("Failed to get " + SLEEP_TIME);
        }
        logger.fine(" created");
    }

    @Override
    public void work() {
        try {
            logger.fine("Going to sleep " + sleepSecs);
            Thread.sleep(sleepSecs * 1000);
            logger.fine("Woke up");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "iselector failed. ", e);
        }
    }

}
