/*
 * VERSAG project
 */
package bundle.basic.random;

import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for Random
 *
 * @author kutila
 * created on 21/09/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    @Override
    public void start(BundleContext context) throws Exception {
        Randomizer idler = new Randomizer(context);
        idler.start();
        logger.fine("Started: " + Randomizer.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        logger.fine("Stopped: " + Randomizer.class.getName());
    }

}
