/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.basic.random;

import jade.util.Logger;

import mma2.AgentIF;
import mma2.capability.Repository;
import mma2.itinerary.ItineraryService;
import mma2.bundles.common.AbstractOneShotClass;

import org.osgi.framework.ServiceReference;
import org.osgi.framework.BundleContext;
 
/**
 *
 *
 * @author kutila
 * created on 21/09/2010
 */
class Randomizer extends AbstractOneShotClass {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private AgentIF agent;

    /** Constructor */
    public Randomizer(BundleContext context) {
        super(context);
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        ref = context.getServiceReference(AgentIF.class.getName());
        agent = (AgentIF) context.getService(ref);

        logger.fine(" created");
    }

    @Override
    public void work() {
        updateItinerary();
    }

    private void updateItinerary() {
        String newItinerary = 
                "loc2=move loc3\n" +
                "loc3=start idler#stop iprovider\n" +
                "\n";

        ItineraryService service = agent.getItineraryService();
        service.setItinerary(newItinerary);
        
        logger.fine("updated itinerary to: " + newItinerary);
    }

}
