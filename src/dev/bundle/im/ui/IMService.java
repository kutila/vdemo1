/*
 * IMService.java
 *
 * Created on February 17, 2006, 4:00 PM
 */
package bundle.im.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import java.util.HashMap;
import java.util.List;
import java.util.Collection;

import bundle.im.api.Contact;
import bundle.im.api.ContactsListener;
import bundle.im.api.IMChat;
import bundle.im.api.IMClientAPI;
import bundle.im.api.IMException;
import bundle.im.api.IMNewChatListener;

import java.util.Enumeration;
import java.util.Hashtable;
import mma2.bundles.common.ServiceIF;
import mma2.capability.Repository;

import jade.util.Logger;

/**
 * The main class of the Instant Messaging (IM) service front end.
 * This capability starts the IM service GUI. It requires an
 * instance of IMClientAPI which implements the backend connecting
 * to an actual service such as Google Talk or Yahoo Messenger.
 *
 * The capability is cyclic
 *
 */
public class IMService implements ServiceIF {

    /** reference to the Agent's repository */
    private Repository repository;
    /** refernce to a concrete IM Client API */
    private IMClientAPI api;
    private List<Contact> contacts;
    private HashMap<String, ChatWindow> chatWindows = new HashMap<String, ChatWindow>();
    private JFrame mainFrame;
    private JScrollPane scrollPane;
    private Container cp;
    protected Logger logger = Logger.getMyLogger(this.getClass().getName());

    public IMService(Repository repository, IMClientAPI api) {
        this.repository = repository;
        this.api = api;
    }

    /**
     * The Instant Messaging Client GUI is started in a separate
     * thread.
     */
    public void start() {
        Runnable doCreateAndShowGUI = new Runnable() {

            public void run() {
                try {
                    initialize();
                    show();
                    resumeOldChats();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        SwingUtilities.invokeLater(doCreateAndShowGUI);
    }

    /**
     * Request the Instant Messaging Client GUI to be stopped
     * and the user logged out from the service.
     */
    public void requestStop() {
        try {
            String me = "me";
            if (api != null) {
                me = api.whoAmI();
                api.logout();
            }
            Collection<ChatWindow> windows = chatWindows.values();
            Hashtable<String, String> chatHistories = new Hashtable<String, String>();
            for (ChatWindow chatWindow : windows) {
                if (chatWindow != null) {
                    String history = chatWindow.getHistory();
                    if (history != null && history.trim().length() > 0) {
                        chatHistories.put(chatWindow.getRecipient(), history);
                    }
                    chatWindow.dispose();
                }
            }
            if (chatHistories.size() > 0) {
                repository.set("history." + me, chatHistories);
                debug("saved [history." + me +
                        "] with " + chatHistories.size() + " histories");
            }
            if (mainFrame != null) {
                mainFrame.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //----- private methods -----
    private void initialize() {
        //--get config details and connect to IM service
        String[] parameters = {
            (String) repository.get(IMClientAPI.HOST),
            (String) repository.get(IMClientAPI.PORT),
            (String) repository.get(IMClientAPI.SERVICE_NAME),
            (String) repository.get(IMClientAPI.USERNAME),
            (String) repository.get(IMClientAPI.PASSWORD)
        };
        try {
            api.init(parameters);
            long startTime = System.currentTimeMillis();
            api.login();
            logger.info("[Tlogin] Login time = " + (System.currentTimeMillis() - startTime));

        } catch (IMException e) {
            e.printStackTrace();
        }

        api.registerContactsListener(new CListener());
        api.registerNewChatListener(new NewChatListener());
        JList buddyJList = populateContactList();

        Toolkit.getDefaultToolkit().setDynamicLayout(true);
        mainFrame = new JFrame("BuddyList " + api.whoAmI());
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addContactListToContentPane(buddyJList);
    }

    private JList populateContactList() {
        //--update contact list from backend
        contacts = api.getContactList(false);
        Contact contactList[] = contacts.toArray(new Contact[contacts.size()]);

        //--create a JList with custom CellRenderer and list of Contacts
        final JList buddyJList = new JList();
        ContactCellRenderer bcr = new ContactCellRenderer(new ContactAdapter());
        buddyJList.setCellRenderer(bcr);
        buddyJList.setListData(contactList);
        buddyJList.setPrototypeCellValue(api.getDummyContact());

        //--add listener for new Chat sessions
        buddyJList.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = buddyJList.locationToIndex(e.getPoint());
                    String user = contacts.get(index).getDisplayName();

                    ChatWindow chatWindow = new ChatWindow();
                    IMChat chat = api.startChatSession(user, chatWindow);
                    chatWindow.init(api.whoAmI(), user, chat);
                    chatWindow.setVisible(true);
                    chatWindows.put(user, chatWindow);
                }
            }
        });

        return buddyJList;
    }

    private void addContactListToContentPane(JList list) {
        cp = mainFrame.getContentPane();
        cp.setLayout(new BorderLayout());
        cp.removeAll();
        scrollPane = new JScrollPane(list);
        cp.add(scrollPane, BorderLayout.CENTER);
        cp.validate();
    }

    /**
     * Look in the repository for past Chat histories and if any exists
     * Recreate ChatWindows for them with the history put in.
     * The histories are removed from the repository.
     */
    private void resumeOldChats() {
        String me = api.whoAmI();
        Hashtable<String, String> chatHistories = (Hashtable<String, String>)
                repository.get("history." + me);
        debug("resuming " + "[history." + me + "] " + chatHistories);
        if (chatHistories == null || chatHistories.size() == 0) {
            debug("no chats to resume");
            return;
        }
        debug(chatHistories.size() + " old chats to resume");
        Enumeration<String> en = chatHistories.keys();
        while (en.hasMoreElements()) {
            String recipient = en.nextElement();
            String history = chatHistories.get(recipient);

            ChatWindow chatWindow = new ChatWindow();
            IMChat chat = api.startChatSession(recipient, chatWindow);
            chatWindow.addHistory(history);
            chatWindow.init(me, recipient, chat);
            chatWindow.setVisible(true);
            chatWindows.put(recipient, chatWindow);
            logger.info("[Tend] Resumed chat with " + recipient);
        }
        repository.remove("history." + me);
    }

    private void show() {
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    private void debug(String msg) {
        System.out.println(" [IMService] " + msg);
    }

    class CListener implements ContactsListener {

        public void entriesAdded(Collection<String> addresses) {
            System.out.println("entries added");
        }

        public void entriesDeleted(Collection<String> addresses) {
            System.out.println("entries deleted");
        }

        public void entriesUpdated(Collection<String> addresses) {
            System.out.println("entries updated");
        }

        public void presenceChanged(String user) {
            addContactListToContentPane(populateContactList());
        }
    }

    class NewChatListener implements IMNewChatListener {

        public void chatCreated(IMChat chat, boolean createdLocally) {
            String recipient = chat.getRecipient();

//        String s = recipient+ " new chat created, local=" + createdLocally;
//        System.out.println(s);

            ChatWindow oldWindow = chatWindows.get(recipient);
            if (oldWindow != null && oldWindow.isShowing()) {
                //chat already has a window, simply re-register the messagelistener
                api.registerMessageListener(chat, oldWindow);
//            System.out.println("this chat already has a window");
                return;
            } else {
                if (!createdLocally) {
                    ChatWindow chatWindow = new ChatWindow();
                    chatWindow.init(api.whoAmI(), recipient, chat);
                    chatWindow.setVisible(true);
                    chatWindows.put(recipient, chatWindow);
                    api.registerMessageListener(chat, chatWindow);
                }
            }
        }
    }
}