/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.ui;

import bundle.im.api.Contact;
import bundle.im.api.Contact.STATUS;
import bundle.im.ui.ContactCellRenderer.Adapter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 * Adapter class that converts "Contact" objects into the format required
 * by "ContactCellRenderer"
 *
 * Source: http://weblogs.java.net/blog/hansmuller/archive/2006/02/a_reusable_budd.html
 * @author Hans Muller (Hans.Muller@Sun.COM)
 * 
 * created on 19/09/2009
 */
public class ContactAdapter extends ContactCellRenderer.Adapter {

    private static Map<URL, ImageIcon> iconCache = new HashMap<URL, ImageIcon>();

    private Contact getContact() {
        return (Contact) getValue();
    }

    @Override
    public String getName() {
        return getContact().getDisplayName();
    }

    @Override
    public Status getStatus() {
        STATUS st = getContact().getStatus();
        if (st == STATUS.ONLINE) {
            return Adapter.Status.ONLINE;
        } else if (st == STATUS.OFFLINE) {
            return Adapter.Status.OFFLINE;
        } else {
            return Adapter.Status.AWAY;
        }
    }

    @Override
    public String getMessage() {
        return getContact().getMessage();

    }

    @Override
    public ImageIcon getBuddyIcon() {
        URL url = getContact().getContactIconURL();
        if (url == null) {
            return null;
        }
        ImageIcon icon = iconCache.get(url);
        if (icon != null) {
            return icon;
        }
        icon = new ImageIcon(url);
        iconCache.put(url, icon);
        return icon;
    }

    @Override
    public String getToolTipText() {
        return getContact().getToolTip();
    }
}
