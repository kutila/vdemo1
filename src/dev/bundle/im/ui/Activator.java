/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.ui;

import bundle.im.api.IMClientAPI;
import mma2.capability.Repository;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Bundle Activator for Instant Messaging (IM) service front end.
 * Requires a concrete IMClientAPI implementation
 *
 * @author kutila2
 * created on 21/09/2009
 */
public class Activator implements BundleActivator {

    private IMService main;

    public void start(BundleContext context) throws Exception {
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        Repository repository = (Repository) context.getService(ref);

        //NOTE: In future will need some mechanism to select a specific
        //client API instead of any
        ref = context.getServiceReference(IMClientAPI.class.getName());
        IMClientAPI api = (IMClientAPI) context.getService(ref);

        main = new IMService(repository, api);
        main.start();
        debug("Started bundle.im.ui.Activator");
    }

    public void stop(BundleContext arg0) throws Exception {
        if (main != null) {
            main.requestStop();
            debug("Stopped bundle.im.ui.Activator");
        }
    }

    private void debug(String string) {
        System.out.println(string);
    }
}
