/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

/**
 *
 * @author kutila2
 * created on 18/09/2009
 */
public interface IMChatMessageListener {

    public void processMessage(IMChat chat, String message);

}
