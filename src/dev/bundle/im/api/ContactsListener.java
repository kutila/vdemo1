/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

import java.util.Collection;

/**
 *
 * @author kutila2
 * created on 18/09/2009
 */
public interface ContactsListener {

    public void entriesAdded(Collection<String> addresses);

    public void entriesDeleted(Collection<String> addresses);

    public void entriesUpdated(Collection<String> addresses);

    public void presenceChanged(String user);
}
