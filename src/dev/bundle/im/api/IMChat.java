/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

import bundle.im.gtalk.*;

/**
 *
 * @author kutila2
 */
public interface IMChat {

    String getRecipient();

    void sendMessage(String message) throws IMException;

}
