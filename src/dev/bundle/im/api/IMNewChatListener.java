/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

import bundle.im.gtalk.*;

/**
 *
 * @author kutila2
 * created on 21/09/2009
 */
public interface IMNewChatListener {

    public void chatCreated(IMChat chat, boolean createdLocally);
    
}
