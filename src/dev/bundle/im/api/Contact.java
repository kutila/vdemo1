/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

import java.net.URL;

/**
 *
 * @author kutila2
 * created on 18/09/2009
 */
public class Contact {

    public enum STATUS {
        ONLINE, AWAY, BUSY, OFFLINE
    };

    private String userID; //unique user identifier
    private String displayName; //name to display
    private STATUS status;
    private String message; //status message
    private URL contactIconURL;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public URL getContactIconURL() {
        return contactIconURL;
    }

    public void setContactIconURL(URL buddyIconURL) {
        this.contactIconURL = buddyIconURL;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String screenName) {
        this.displayName = screenName;
    }

    public String getToolTip() {
        if (message != null && message.trim().length() > 0)
            return userID + " " + message;
        else
            return userID;
    }
}
