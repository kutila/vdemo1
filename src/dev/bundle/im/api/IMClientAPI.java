/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

//import bundle.im.gtalk.*;
import java.util.List;

/**
 *
 * @author kutila2
 */
public interface IMClientAPI {

    public static final String HOST = "im.HOST";
    public static final String PORT = "im.PORT";
    public static final String SERVICE_NAME = "im.SERVICE_NAME";
    public static final String USERNAME = "im.USERNAME";
    public static final String PASSWORD = "im.PASSWORD";

    Contact getContact(String user);

    List<Contact> getContactList(boolean offline);

    Contact getDummyContact();

    Contact getPresence(String user);

    /**
     * Initialize the IMService API with connection details for service
     * and user credentials.
     *
     * The parameters expected are
     * {host, port, serviceName, username, password}
     *
     * @param parameters
     * @throws IMException
     */
    void init(String[] parameters) throws IMException;

    void login() throws IMException;

    void logout();

    /**
     * Register a listener to be notified of changes in Contacts
     * (e.g. presence changes)
     *
     * @param listener
     */
    void registerContactsListener(final ContactsListener listener);

    /**
     * Register a new MessageListener to the given chat session. All previous
     * message listeners are removed from the chat.
     *
     * @param chat
     * @param listener
     */
    void registerMessageListener(final IMChat chat, final IMChatMessageListener listener);

    /**
     * Register to be notified when new Chat sessions are created.
     *
     * @param listener
     */
    void registerNewChatListener(final IMNewChatListener listener);

    IMChat startChatSession(String recipient, final IMChatMessageListener listener);

    String whoAmI();

}
