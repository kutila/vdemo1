/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle Activator for IM service backend
 * Simply makes the relevant classes available to other bundles
 *
 * @author kutila2
 * created on 21/09/2009
 */
public class Activator implements BundleActivator   {

    public void start(BundleContext arg0) throws Exception {
        System.out.println("Started bundle.im.api.Activator");
    }

    public void stop(BundleContext arg0) throws Exception {
        System.out.println("Stopped bundle.im.api.Activator");
    }

}
