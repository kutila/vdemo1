/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.api;

/**
 *
 * @author kutila2
 * created on 18/09/2009
 */
public class IMException extends Exception {

    public static enum ERRCODE {INVALID_PARAMS, LOGIN_FAILED, SEND_FAILED, NO_VALUE};

    private ERRCODE errCode = ERRCODE.NO_VALUE;

    public IMException(String message, ERRCODE errCode) {
        super(message);
        this.errCode = errCode;
    }

    public IMException(String message) {
        super(message);
    }

    public IMException(ERRCODE errCode) {
        this.errCode = errCode;
    }

    public IMException(Throwable t) {
        super(t);
    }

    public ERRCODE getErrCode() {
        return errCode;
    }

    public void setErrCode(ERRCODE errCode) {
        this.errCode = errCode;
    }


}
