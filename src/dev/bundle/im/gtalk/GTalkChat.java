/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.gtalk;

import bundle.im.api.IMException;
import bundle.im.api.IMChat;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.XMPPException;

/**
 *
 * @author kutila2
 * created on 18/09/2009
 */
public class GTalkChat implements IMChat {

    private Chat chat;

    public GTalkChat(Chat c) {
        this.chat = c;
    }

    public String getRecipient() {
        return chat.getParticipant();
    }

    public void sendMessage(String message) throws IMException {
        try {
            chat.sendMessage(message);
        } catch (XMPPException e) {
            IMException ex = new IMException(e);
            ex.setErrCode(IMException.ERRCODE.SEND_FAILED);
            throw ex;
        }
    }
 
    protected Chat getChat() {
        return chat;
    }


}
