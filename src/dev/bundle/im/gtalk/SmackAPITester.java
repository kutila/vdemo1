/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.gtalk;

import bundle.im.api.IMChatMessageListener;
import bundle.im.api.ContactsListener;
import bundle.im.api.IMException;
import bundle.im.api.Contact;
import bundle.im.api.IMChat;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author kutila2
 * created on 18/09/2009
 */
public class SmackAPITester {
    SmackAPI api;

    public void doTest() {
        String[] parameters = {
            "talk.google.com",
            "5222",
            "gmail.com",
            "dsse.mobility@gmail.com",
            "m0bilitydsse"
        };

        //--connect to Google Talk
        api = new SmackAPI();
        try {
            api.init(parameters);
            api.login();
        } catch (IMException e) {
            e.printStackTrace();
        }

        //--get contact list
        List<Contact> contacts = api.getContactList(true);
        for (Contact contact : contacts) {
            StringBuffer buf = new StringBuffer();
            buf.append("{ screen_name: ");
            buf.append(contact.getDisplayName());
            buf.append(" , status: ");
            buf.append(contact.getStatus());
            buf.append(" , msg: ");
            buf.append(contact.getMessage());
            buf.append(" , tooltip: ");
            buf.append(contact.getToolTip());
            buf.append("}");
            System.out.println(buf.toString());
        }

        //--send chat message
        IMChat chat = api.startChatSession("dsse.mobility@gmail.com",
            new IMChatMessageListener() {
                public void processMessage(IMChat chat, String message) {
                    System.out.println("received : " + message);
                }
            }
        );
        try {
            chat.sendMessage("hello kutila");
        } catch (IMException e) {
            e.printStackTrace();
        }

        //--listen to presence changes
        api.registerContactsListener(new CListener());
        System.out.println("sleeping...");
        try {Thread.sleep(60*1000); } catch (InterruptedException e) {}
    }


    public static void main(String[] args) {
        new SmackAPITester().doTest();
    }



class CListener implements ContactsListener {

    public void entriesAdded(Collection<String> addresses) {
        StringBuffer buf = new StringBuffer();
        buf.append("   Entries added: ");
        for (String string : addresses) {
            buf.append(string);
            buf.append(", ");
        }
        System.out.println(buf.toString());
    }

    public void entriesDeleted(Collection<String> addresses) {
        StringBuffer buf = new StringBuffer();
        buf.append("   Entries deleted: ");
        for (String string : addresses) {
            buf.append(string);
            buf.append(", ");
        }
        System.out.println(buf.toString());
    }

    public void entriesUpdated(Collection<String> addresses) {
        StringBuffer buf = new StringBuffer();
        buf.append("   Entries updated: ");
        for (String string : addresses) {
            buf.append(string);
            buf.append(", ");
        }
        System.out.println(buf.toString());
    }

    public void presenceChanged(String user) {
        Contact contact = api.getContact(user);
        if (contact != null) {
            System.out.println("   presence changed: " + contact.getToolTip() + " "
                    + contact.getMessage() + " " + contact.getStatus());
        } else {
            System.out.println("   presence changed for " + user);
        }
    }

}
}