/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.gtalk;

import bundle.im.api.IMClientAPI;
import mma2.bundles.common.ServiceIF;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle Activator for Google Talk (IM) service backend provided using the
 * Smack API.
 * Starts an instance of the API and registers it as an IMClientAPI service
 *
 * @author kutila
 * created on 21/09/2009
 */
public class Activator implements BundleActivator   {
    private ServiceRegistration serviceRegistration;
    private ServiceIF theService;

    public void start(BundleContext context) throws Exception {
        theService = new SmackAPI();
        serviceRegistration = context.registerService(
                IMClientAPI.class.getName(),
                theService, null);

        System.out.println("Started bundle.im.gtalk.Activator");
    }

    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        theService.requestStop();
        System.out.println("Stopped bundle.im.gtalk.Activator");
    }

}
