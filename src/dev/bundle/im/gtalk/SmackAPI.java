/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.im.gtalk;
 
import bundle.im.api.IMNewChatListener;
import bundle.im.api.IMChatMessageListener;
import bundle.im.api.ContactsListener;
import bundle.im.api.IMException;
import bundle.im.api.Contact;
import bundle.im.api.IMChat;
import bundle.im.api.IMClientAPI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import mma2.bundles.common.ServiceIF;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.packet.Presence.Type;
 
/**
 *
 * @author kutila2
 * created on 18/09/2009
 */
public class SmackAPI implements IMClientAPI, ServiceIF {

    private String host = "talk.google.com";
    private int port = 5222;
    private String serviceName = "gmail.com";
    private String username = null;
    private String password = null;
    private XMPPConnection connection = null;

    public SmackAPI() {
    }

    /**
     * Initialize the IMService API with connection details for service
     * and user credentials.
     * 
     * The parameters expected are
     *    {host, port, serviceName, username, password}
     *
     * @param parameters
     * @throws IMException
     */
    public void init(String[] parameters) throws IMException {
        if (parameters == null || parameters.length != 5) {
            throw new IMException("Incorrect connection details", IMException.ERRCODE.INVALID_PARAMS);
        }
        host = parameters[0];
        port = Integer.parseInt(parameters[1]);
        serviceName = parameters[2];
        username = parameters[3];
        password = parameters[4];
    }

    public void login() throws IMException {
//        XMPPConnection.DEBUG_ENABLED = true;
        ConnectionConfiguration config = new ConnectionConfiguration(host, port, serviceName);
        connection = new XMPPConnection(config);
        try {
            connection.connect();
            connection.login(username, password);
        } catch (Exception e) {
            IMException ex = new IMException(e);
            ex.setErrCode(IMException.ERRCODE.LOGIN_FAILED);
            throw ex;
        }
    }

    public void logout() {
        if (connection == null) {
            return;
        }
        try {
            connection.disconnect();
        } catch (Exception e) {
            //ignore
            System.out.println("logout threw an exception " + e.toString());
        }
    }

    public IMChat startChatSession(String recipient, final IMChatMessageListener listener) {
        Chat chat = connection.getChatManager().createChat(recipient,
                new MessageListener() {

                    public void processMessage(Chat chat, Message message) {
                        listener.processMessage(new GTalkChat(chat), message.getBody());
                    }
                });
        return new GTalkChat(chat);
    }

    public List<Contact> getContactList(boolean offline) {
        List<Contact> contactList = new ArrayList<Contact>();
        Contact contact;
        Roster roster = connection.getRoster();
        Collection<RosterEntry> entries = roster.getEntries();
        for (RosterEntry entry : entries) {
            contact = rosterEntry2Contact(entry, roster);

            //update the Presence value
            Presence presence = roster.getPresence(contact.getUserID());
            if (presence != null) {
                Type type = presence.getType();

                switch (type) {
                    case available:
                        Mode mode = presence.getMode();
                        if (mode == Mode.available) {
                            contact.setStatus(Contact.STATUS.ONLINE);
                        } else if (mode == Mode.away) {
                            contact.setStatus(Contact.STATUS.AWAY);
                        } else if (mode == Mode.dnd) {
                            contact.setStatus(Contact.STATUS.BUSY);
                        }
                        contact.setMessage(presence.getStatus());
                        break;
                    case unavailable:
                        continue;
//                        contact.setStatus(Contact.STATUS.OFFLINE);
//                        contact.setMessage(presence.getStatus());
//                        break;
                    default:
                        //TODO: handle the other "types"
                        contact.setStatus(Contact.STATUS.OFFLINE);
                }
            }

            contactList.add(contact);
        }
        return contactList;
    }

    public Contact getContact(String user) {
        Roster roster = connection.getRoster();
        RosterEntry entry = roster.getEntry(user);
        if (entry != null) {
            return rosterEntry2Contact(entry, roster);
        }
        return null;
    }

    public Contact getDummyContact() {
        Contact c = new Contact();
        c.setUserID("someID");
        c.setDisplayName("Kutila Gunasekera");
        c.setStatus(Contact.STATUS.ONLINE);
        c.setMessage("Busy at work");
        return c;
    }

    public String whoAmI() {
        return username; //connection.getUser();
    }

    public Contact getPresence(String user) {
        Contact contact = new Contact();

        Roster roster = connection.getRoster();
        Presence presence = roster.getPresence(user);

        if (presence != null) {
            Type type = presence.getType();

            switch (type) {
                case available:
                    Mode mode = presence.getMode();
                    if (mode == Mode.available) {
                        contact.setStatus(Contact.STATUS.ONLINE);
                    } else if (mode == Mode.away) {
                        contact.setStatus(Contact.STATUS.AWAY);
                    } else if (mode == Mode.dnd) {
                        contact.setStatus(Contact.STATUS.BUSY);
                    }
                    contact.setMessage(presence.getStatus());
                    break;
                case unavailable:
                    contact.setStatus(Contact.STATUS.OFFLINE);
                    contact.setMessage(presence.getStatus());
                    break;
                default:
                    //TODO: handle the other "types"
                    contact.setStatus(Contact.STATUS.OFFLINE);
            }
        }
        return contact;
    }

    /**
     * Register a listener to be notified of changes in Contacts
     * (e.g. presence changes)
     * 
     * @param listener
     */
    public void registerContactsListener(final ContactsListener listener) {
        final Roster roster = connection.getRoster();
        roster.addRosterListener(new RosterListener() {

            public void entriesAdded(Collection<String> addresses) {
                listener.entriesAdded(addresses);
            }

            public void entriesDeleted(Collection<String> addresses) {
                listener.entriesDeleted(addresses);
            }

            public void entriesUpdated(Collection<String> addresses) {
                listener.entriesUpdated(addresses);
            }

            public void presenceChanged(Presence presence) {
                String user = presence.getFrom();
                listener.presenceChanged(user);
            }
        });
    }

    /**
     * Register to be notified when new Chat sessions are created.
     * 
     * @param listener
     */
    public void registerNewChatListener(final IMNewChatListener listener) {
        connection.getChatManager().addChatListener(new ChatManagerListener() {

            public void chatCreated(Chat chat, boolean createdLocally) {
                listener.chatCreated(new GTalkChat(chat), createdLocally);
            }
        });
    }

    /**
     * Register a new MessageListener to the given chat session. All previous
     * message listeners are removed from the chat.
     *
     * @param chat
     * @param listener
     */
    public void registerMessageListener(final IMChat chat,
            final IMChatMessageListener listener) {

        Chat parent = ((GTalkChat) chat).getChat();

        //remove all previous listeners
        Collection<MessageListener> previous = parent.getListeners();
        for (MessageListener messageListener : previous) {
            parent.removeMessageListener(messageListener);
        }

        parent.addMessageListener(new MessageListener() {

            public void processMessage(Chat smackChat, Message message) {
                listener.processMessage(chat, message.getBody());
            }
        });
    }

    /*
     * Convert a SmackAPI RosterEntry to our Contact
     */
    private Contact rosterEntry2Contact(RosterEntry entry, Roster roster) {
        Contact contact = new Contact();
        contact.setUserID(entry.getUser());

        String displayName = entry.getName();
        if (displayName == null || displayName.trim().length() == 0) {
            displayName = entry.getUser();
        }
        contact.setDisplayName(displayName);

        Presence p = roster.getPresence(entry.getUser());
//        System.out.println(entry + " " + p.toString());
        if (p.isAvailable()) {
            if (p.isAway()) {
                contact.setStatus(Contact.STATUS.AWAY);
            } else {
                contact.setStatus(Contact.STATUS.ONLINE);
            }
        } else {
            contact.setStatus(Contact.STATUS.OFFLINE);
        }
        contact.setMessage(p.getStatus());
        return contact;
    }

    public void requestStop() {
        logout();
    }
}


