/*
 * VERSAG project
 */
package bundle.ir.result;


import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for Test2 Result Handler
 *
 * @author kutila
 * created on 27/01/2009
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ResultHandler theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        theService = new ResultHandler(context);
        theService.start();
        logger.fine("Service started: " + ResultHandler.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        theService.requestStop();
        logger.fine("Service stopped: " + ResultHandler.class.getName());
    }

}
