/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.result;


import jade.util.Logger;
import mma2.AgentIF;
import mma2.bundles.common.ServiceIF;
import mma2.capability.Repository;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;


/**
 * Requestes the agent to move to "home" (if not already there) and
 * displays the result from the Repository
 * 
 * @author kutila
 * created 27/01/2009
 */
public class ResultHandler extends Thread implements ServiceIF {

    public static final String RESULT = "test2.RESULT";
    public static final String HOME = "test2.HOME";

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private BundleContext context;

    public ResultHandler(BundleContext context) {
        this.context = context;
    }
    
    @Override
    public void run() {
        doResultDisplay();
    }
    
    private void doResultDisplay() {
        Repository repository = (Repository) getOSGiService(Repository.class.getName());
        AgentIF agent = (AgentIF) getOSGiService(AgentIF.class.getName());

        String homeLoc = (String) repository.get(HOME);
        String currentLoc = agent.isAt();
        logger.fine("Home: " + homeLoc + ", Now at: " + currentLoc);
        
        if (!homeLoc.equalsIgnoreCase(currentLoc)) {
            String symbolicName = (String) context.getBundle().getHeaders().get("Bundle-Name");
            String itinerary = currentLoc + "=move " + homeLoc + "\n" + 
                    homeLoc + "=start " + symbolicName;
            agent.getItineraryService().setItinerary(itinerary);    
            logger.fine("Updated itinerary: " + itinerary);
        } else {
            //Display the result
            System.out.println("*******************************");
            System.out.println((String) repository.get(RESULT));
            System.out.println("*******************************");
        }
        stopAndUninstall();
    }

    
    private Object getOSGiService(String className) {
        ServiceReference ref = context.getServiceReference(className);
        return context.getService(ref);
    }

    public void requestStop() {
        
    }

    private void stopAndUninstall() {
        try {
            context.getBundle().uninstall();
        } catch (Exception e) {
            System.out.println("Exception when trying to stop/uninstall bundle. <" + 
                    e.toString() + ">");
        }
    }

}
