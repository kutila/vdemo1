/*
 * VERSAG project
 */
package bundle.ir.pdfbox;

import bundle.ir.common.PdfReaderIF;
import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator Test2 PDF Reader
 *
 * @author kutila
 * created on 26/01/2009
 */
public class Activator implements BundleActivator {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private ServiceRegistration serviceRegistration;
    private PdfReaderPdfbox theService;

    @Override
    public void start(BundleContext context) throws Exception {
        String[] classes = {PdfReaderPdfbox.class.getName(), PdfReaderIF.class.getName()};
        theService = new PdfReaderPdfbox();
        serviceRegistration = context.registerService(classes, theService, null);
        logger.fine("Service started: " + PdfReaderPdfbox.class.getName());
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        logger.fine("Service stopped: " + PdfReaderPdfbox.class.getName());
    }
}
