/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.pdfbox;

import bundle.ir.common.FileObject;
import bundle.ir.common.FileObject.FILE_TYPE;
import bundle.ir.common.PdfReaderIF;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;

import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;

/**
 *
 * @author kutila
 * created 26/01/2009
 */
public class PdfReaderPdfbox implements PdfReaderIF {

    /**
     * Read a file into a FileObject
     * 
     * @param filename
     * @return
     */
    @Override
    public FileObject read(String filename, String path) {
        FileObject fObj = new FileObject();
        fObj.setName(filename);
        fObj.setFileType(FILE_TYPE.PDF);
        File f = new File(path + File.separatorChar + filename);
        return internalRead(fObj, f);
    }

    @Override
    public FileObject read(File file) {
        FileObject fObj = new FileObject();
        fObj.setName(file.getName());
        fObj.setFileType(FILE_TYPE.PDF);
        return internalRead(fObj, file);
    }
    
    private FileObject internalRead(FileObject fObj, File file) {
        try {
            PDDocument pdfDoc = PDDocument.load(
                    new BufferedInputStream(new FileInputStream(file)));
            PDFTextStripper textStripper = new PDFTextStripper();
            System.out.println("2");
            StringWriter text = new StringWriter();
            textStripper.writeText(pdfDoc, text);
            pdfDoc.close();
            fObj.setText(text.toString());
        } catch (Exception e) {
            fObj.setError(e);
        }
        return fObj;
    }
    
    
}
