/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.common;

import java.util.Vector;

/**
 * Internal representation of a file that has been parsed and read
 * 
 * @author kutila
 * created on 22/01/2009
 */
public class FileObject {

    public static enum FILE_TYPE {PDF, DOC, TXT}
    
    /** The content of the file */
    private String text;

    /** File name including extension */
    private String name;

    /** Type of the file */
    private FILE_TYPE fileType;
    
    private Vector<Throwable> errors = new Vector<Throwable>();
    
    //-- methods --
    
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public FILE_TYPE getFileType() {
        return fileType;
    }

    public void setFileType(FILE_TYPE fileType) {
        this.fileType = fileType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector<Throwable> getErrors() {
        return errors;
    }

    public void setError(Throwable error) {
        errors.add(error);
    }
    
    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append('<');
        b.append(getName());
        b.append(',');
        b.append(getFileType());
        b.append(',');
        if (text != null) {
            b.append(text.length());
            b.append(',');
        }
        b.append(getErrors().toString());
        b.append('>');
        return b.toString();
    }
    
}
