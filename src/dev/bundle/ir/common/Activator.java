/*
 * VERSAG project
 */
package bundle.ir.common;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Test2 Common classes bundle activator
 *
 * @author kutila
 * created on 23/01/2009
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    @Override
    public void start(BundleContext context) throws Exception {
        logger.fine("Service started: ");
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        logger.fine("Service stopped: ");
    }

}
