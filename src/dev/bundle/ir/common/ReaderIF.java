/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.common;

import java.io.File;

/**
 *
 * @author kutila
 * created 22/01/2009
 */
public interface ReaderIF {

    public FileObject read(String filename, String path);
    public FileObject read(File file);

}
