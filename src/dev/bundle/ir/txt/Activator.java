/*
 * VERSAG project
 */
package bundle.ir.txt;

import bundle.ir.common.TxtReaderIF;
import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator test 2 Text Reader
 *
 * @author kutila
 * created on 23/01/2009
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private TxtReader theService;
    
    @Override
    public void start(BundleContext context) throws Exception {
        String[] classes = {TxtReader.class.getName(), TxtReaderIF.class.getName()};
        theService = new TxtReader();
        serviceRegistration =context.registerService(classes, theService, null);
        logger.fine("Service started: " + TxtReader.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        logger.fine("Service stopped: " + TxtReader.class.getName());
    }

}
