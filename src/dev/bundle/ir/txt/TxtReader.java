/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.txt;

import bundle.ir.common.FileObject;
import bundle.ir.common.FileObject.FILE_TYPE;
import bundle.ir.common.TxtReaderIF;

import java.io.File;
import java.io.FileReader;

/**
 *
 * @author kutila
 * created 23/01/2009
 */
public class TxtReader implements TxtReaderIF {

    public TxtReader() {
    }

    /**
     * Read a file into a FileObject
     * 
     * @param filename
     * @return
     */
    @Override
    public FileObject read(String filename, String path) {
        FileObject fObj = new FileObject();
        fObj.setName(filename);
        fObj.setFileType(FILE_TYPE.TXT);
        try {
            FileReader in = new FileReader(path + File.separatorChar + filename);
            StringBuffer sbuf = new StringBuffer();
            char[] cbuf = new char[1024];
            int count = in.read(cbuf);
            while (count > 0) {
                sbuf.append(cbuf, 0, count);
                count = in.read(cbuf);
            }
            in.close();

            fObj.setText(sbuf.toString());
        } catch (Exception e) {
            fObj.setError(e);
        }
        return fObj;
    }

    @Override
    public FileObject read(File file) {
        FileObject fObj = new FileObject();
        fObj.setName(file.getName());
        fObj.setFileType(FILE_TYPE.TXT);
        try {
            FileReader in = new FileReader(file);
            StringBuffer sbuf = new StringBuffer();
            char[] cbuf = new char[1024];
            int count = in.read(cbuf);
            while (count > 0) {
                sbuf.append(cbuf, 0, count);
                count = in.read(cbuf);
            }
            in.close();

            fObj.setText(sbuf.toString());
        } catch (Exception e) {
            fObj.setError(e);
        }
        return fObj;
    }
    
}
