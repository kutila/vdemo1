/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.matcher;

import bundle.ir.common.ReaderIF;
import bundle.ir.common.FileObject;
import jade.util.Logger;

import java.io.File;
import java.util.Hashtable;
import java.util.logging.Level;
import mma2.bundles.common.AbstractOneShotClass;

import mma2.capability.Repository;
import mma2.util.Utility;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Main class of "Matcher" capability
 * ONESHOT execution semantics
 * 
 * This class "processes" files contained in the given "searchLocation". A file
 * is included only if it "matches" a given "matchingCriteria". At present the
 * matching process simply checks whether a file contains a given text string.
 * Reading files requires special ReaderIF objects. Two sub-types of Readers
 * are supported based on the file extension.
 *  pdf - PdfReaderIF
 *  txt - TxtReaderIF
 * "Processing" is simply counting the number of words in the file.
 * The results are written to the agent's Repository.
 *
 * @author kutila
 * created on 22/01/2009
 */
public class MatcherMain extends AbstractOneShotClass {

    public static final String MATCHING_CRITERIA = "test2.MATCHING_CRITERIA";
    public static final String SEARCH_LOCATION = "test2.SEARCH_LOCATION";
    public static final String RESULT = "test2.RESULT";

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    private static String bundleName;
    private String matchingCriteria;
    private Repository repository;
    private Hashtable<String, ReaderIF> readers = new Hashtable<String, ReaderIF>();
    private boolean stopRequest = false;
//    private boolean doneHere = false;
    private String resultString;
    
    

    /** Constructor */
    public MatcherMain(BundleContext context, String bundleName) {
        super(context);
        MatcherMain.bundleName = bundleName;

        repository = (Repository) getOSGiService(Repository.class.getName());
        resultString = (String) repository.get(RESULT);
    }

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    @Override
    public void work() {
        logger.finest("Matcher.work() starting");
        long t1 = System.currentTimeMillis();
//        if (doneHere) {
//            log(bundleName + " already ran here.");
//            return;
//        }

        //--get parameters 
        matchingCriteria = (String) repository.get(MATCHING_CRITERIA);
        String searchLocation = (String) repository.get(SEARCH_LOCATION);

        File dir = new File(searchLocation);
        File[] fileList = dir.listFiles();
        for (File f : fileList) {
            String name = f.getName();
            String type = name.substring(name.lastIndexOf('.') + 1);
            if (!isSupportedFileType(type)) {
                logger.finest(name + " is not of a supported type");
                continue;
            }
            ReaderIF reader = getReader(type);
            if (reader == null) {
                logger.fine("Could not find reader");
                continue;
                //TODO: "search for Reader" and wait until found
            }

            FileObject fObj = reader.read(f);
            logger.finest("Parsed: " + fObj.toString());
            if (isMatch(fObj)) {
                process(fObj);
            }
        }
        //--update agent's result
        repository.set(RESULT, resultString);
        logger.fine("Updated result is " + resultString);
        
        //--finishing up
//        doneHere = true;
        long t2 = System.currentTimeMillis();
        Utility.log("MatcherMain.work() took (ms) = " + (t2 - t1));
        logger.fine(bundleName + " finishing");
    }

    private ReaderIF getReader(String type) {
        ReaderIF reader = readers.get(type);
        if (reader == null) {
            String className = "bundle.ir.common.ReaderIF";
            if (type.equalsIgnoreCase("txt")) {
                className = "bundle.ir.common.TxtReaderIF";
            } else if (type.equalsIgnoreCase("pdf")) {
//                className = "bundle.ir.pdfbox.PdfReaderPdfbox";
                className = "bundle.ir.common.PdfReaderIF";
            }
            try {
                Object o = (ReaderIF) getOSGiService(className);
                if (o != null && o instanceof ReaderIF) {
                    reader = (ReaderIF) o;
                    readers.put(type, reader);
                } else {
                    logger.warning("Failed to get a Reader for: " + className);
                }
            } catch (Exception e) {
                //logger.log(Level.WARNING, "Failed to get a matching Reader", e);
            }
        }
        return  reader;
    }
    
    protected Object getOSGiService(String className) {
        ServiceReference ref = context.getServiceReference(className);
        return context.getService(ref);
    }
 
    private boolean isSupportedFileType(String item) {
        if ((item != null && item.trim().length() > 2) &&
                (item.equalsIgnoreCase("pdf") || 
                item.equalsIgnoreCase("doc") || item.equalsIgnoreCase("txt"))) {
            return true;
        }
        return false;
    }

    /**
     * Compares a FileObject against the "matchingCriteria"
     * 
     * @param fObj The FileObject to be matched
     * @return true if it is a match, false otherwise
     */
    private boolean isMatch(FileObject fObj) {
        String text = fObj.getText().toLowerCase();
        //TODO - add support for more detailed comparisons
        //SIMPLY SEES IF THE GIVEN TEXT IS IN THE FILE!
        if (text != null && text.contains(matchingCriteria)) {
            return true;
        }
        return false;
    }

    private void process(FileObject fObj) {
        //TODO: some meaningful processing here
        String text = fObj.getText();
        int wordCount = 0;
        if (text != null && text.length() > 0) {
            String[] words = text.split("\\W");
            wordCount = words.length;
        }
        resultString = resultString + "\n" + fObj.getName() + ":" + wordCount;
    }
}