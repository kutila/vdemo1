/*
 * VERSAG project
 */
package bundle.ir.matcher;

import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Bundle activator for: Matcher
 * Execution semantics: Active OneShot
 *
 * @author kutila
 * created on 22/01/2009
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private MatcherMain matcher;
    private static final String bundleName = "Matcher";
    
    @Override
    public void start(BundleContext context) throws Exception {
        matcher = new MatcherMain(context, bundleName);
        matcher.start();
        logger.fine("Started bundle: " + bundleName);
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        matcher.requestStop();
        logger.fine("Stopped bundle: " + bundleName);
    }

}
