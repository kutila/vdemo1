/*
 * VERSAG project
 */
package bundle.ir.jpod;

import bundle.ir.common.PdfReaderIF;
import jade.util.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for PDF 2 Text reader using the jPod library.
 *
 * @author kutila
 * created on 26/01/2009
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private PdfReaderJpod theService;
    private ServiceRegistration serviceRegistration;

    @Override
    public void start(BundleContext context) throws Exception {
        String[] classes = {PdfReaderJpod.class.getName(), PdfReaderIF.class.getName()};
        theService = new PdfReaderJpod();
        serviceRegistration = context.registerService(classes, theService, null);

        logger.fine("Service started: " + PdfReaderJpod.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        logger.fine("Service stopped: " + PdfReaderJpod.class.getName());
    }

}
