/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.ir.jpod;

import bundle.ir.common.FileObject;
import bundle.ir.common.FileObject.FILE_TYPE;
import bundle.ir.common.PdfReaderIF;

import de.intarsys.pdf.content.CSDeviceBasedInterpreter;
import de.intarsys.pdf.content.CSException;
import de.intarsys.pdf.content.text.CSTextExtractor;
import de.intarsys.pdf.pd.PDDocument;
import de.intarsys.pdf.pd.PDPage;
import de.intarsys.pdf.pd.PDPageNode;
import de.intarsys.pdf.pd.PDPageTree;
import de.intarsys.tools.locator.FileLocator;

import java.io.File;
import java.util.Iterator;

/**
 * http://opensource.intarsys.de/home/en/index.php?n=JPod.HomePage
 *
 *
 * @author kutila
 * created on 13/07/2010
 */
class PdfReaderJpod implements PdfReaderIF {

    /**
     * Read a file into a FileObject
     *
     * @param filename
     * @return
     */
    @Override
    public FileObject read(String filename, String path) {
        FileObject fObj = new FileObject();
        fObj.setName(filename);
        fObj.setFileType(FILE_TYPE.PDF);
        File f = new File(path + File.separatorChar + filename);
        return internalRead(fObj, f);
    }

    @Override
    public FileObject read(File file) {
        FileObject fObj = new FileObject();
        fObj.setName(file.getName());
        fObj.setFileType(FILE_TYPE.PDF);
        return internalRead(fObj, file);
    }

    private FileObject internalRead(FileObject fObj, File f) {
        try {

            FileLocator locator = new FileLocator(f.getAbsolutePath());
            PDDocument doc = PDDocument.createFromLocator(locator);
            StringBuilder sb = new StringBuilder();
            extractText(doc.getPageTree(), sb);
            fObj.setText(sb.toString());
            doc.close();
        } catch (Exception e) {
            fObj.setError(e);
        }
        return fObj;
    }

    protected void extractText(PDPageTree pageTree, StringBuilder sb) {
        for (Iterator it = pageTree.getKids().iterator(); it.hasNext();) {
            PDPageNode node = (PDPageNode) it.next();
            if (node.isPage()) {
                try {
                    CSTextExtractor extractor = new CSTextExtractor();
                    PDPage page = (PDPage) node;
                    CSDeviceBasedInterpreter interpreter = new CSDeviceBasedInterpreter(
                            null, extractor);
                    interpreter.process(page.getContentStream(), page.getResources());
                    sb.append(extractor.getContent());
                } catch (CSException e) {
                    e.printStackTrace();
                }
            } else {
                extractText((PDPageTree) node, sb);
            }
        }
    }
}
