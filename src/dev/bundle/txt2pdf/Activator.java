/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.txt2pdf;

import mma2.capability.Repository;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Bundle Activator for "local txt2pdf" conversion bundle
 * Parameter needed for this is:
 *  - FILEDIR
 *
 * Execution type: ACTIVE_ONESHOT
 * - a new Thread is invoked. It completes and then terminates.
 *
 * @author kutila
 * created on 10/10/2009
 */
public class Activator implements BundleActivator {
    public static final String FILE_LOCATION = "FILEDIR";

    private String txtFileLocation = null;
    LocalTxt2PdfWorker worker;

    public void start(BundleContext context) throws Exception {
        ServiceReference ref = context.getServiceReference(Repository.class.getName());
        Repository repository = (Repository) context.getService(ref);

        txtFileLocation = (String) repository.get(FILE_LOCATION);
        if (txtFileLocation == null || txtFileLocation.length() == 0) {
            System.out.println(FILE_LOCATION + " was empty");
            return;
        }
        
        worker = new LocalTxt2PdfWorker(context);
        worker.setTxtFileLocation(txtFileLocation);

        //XXXX AAMAS 2 experiment time calculation
        try {
            long startedAt = Long.valueOf(repository.get("STARTEDAT").toString());
            worker.setStartedAt(startedAt);
        } catch (Exception e) {
            System.out.println("start time getting failed with " + e.toString());
        }

        worker.start();
        System.out.println("Started " + this.getClass().getName());
    }

    public void stop(BundleContext context) throws Exception {
        if (worker != null)
            worker.requestStop();
        System.out.println("Stopped " +  this.getClass().getName());
    }

}
