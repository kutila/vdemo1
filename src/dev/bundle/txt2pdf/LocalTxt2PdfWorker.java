/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.txt2pdf;

import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import jade.util.Logger;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;

import mma2.bundles.common.AbstractOneShotClass;
import mma2.util.Utility;

import org.osgi.framework.BundleContext;

/**
 * Local txt2pdf conversion OneShot capability. uses the iText API
 *
 * @author kutila2
 * created on 24/08/2010
 */
public class LocalTxt2PdfWorker extends AbstractOneShotClass {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    private String txtFileLocation = "";
    private boolean stopRequest = false;

    //--------FOR total time consumption calculation AAMAS experiment 2
    private long startedAt;

    public LocalTxt2PdfWorker(BundleContext context) {
        super(context);
    }


    public void setStartedAt(long startedAt) {
        this.startedAt = startedAt;
    }

    private static String tempfile = "temp.pdf";

    public synchronized boolean isStopRequested() {
        return stopRequest;
    }

    public void setTxtFileLocation(String t) {
        txtFileLocation = t;
    }

    @Override
    public void work() {
        long startTime = System.currentTimeMillis();

        File dir = new File(txtFileLocation);
        String[] files2Process = dir.list(
                new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.endsWith(".txt");
                    }
                }
        );

        if (files2Process == null || files2Process.length == 0) {
            System.out.println("No files to process");
        } else {
            for (String filename : files2Process) {
                workOne(txtFileLocation + "\\" + filename);
            }
        }
        Utility.log("LocalTxt2Pdf: Processing time (ms) = "
                + (System.currentTimeMillis() - startTime));
        Utility.log("LocalTxt2Pdf: Complete time (ms)   = "
                + (System.currentTimeMillis() - startedAt));
    }


    private void workOne(String filename) {
        System.out.println("Client starting");
        byte[] data = fileToBytes(filename);
        if (data == null || data.length == 0) {
            System.out.println(filename + " was empty");
            return;
        }
        try {
            long l = 0;
            byte[] result = process(data, l);
            FileOutputStream fout = new FileOutputStream(filename + ".pdf");
            fout.write(result);
            fout.close();
            System.out.println("Result was written to " + filename + ".pdf");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Takes in a byte array representing a stream of text and converts it
     * into a PDF. The file is returned as a byte array.
     */
    public byte[] process(byte[] data, long delay) {
        logger.fine("--start process()");
        byte[] b = null;
        if (data == null || data.length < 1) {
            return null;
        }
        try {
            int result = convertToPDF(data, tempfile);
            if (result == 0) {
                logger.fine("Conversion was successful.");
                b = fileToBytes(tempfile);
            }
        } catch (Exception ex) {
            logger.warning(ex.toString());
        }
        if (delay > 0) {
            delay(delay);
        }

        logger.fine("--end process()");
        return b;
    }

    /**
     * Converts the given text file "infile" to a PDF file and
     * writes it to "outfile".
     * Returns 0 if the proces was successful and -1 if it failed.
     */
    private int convertToPDF(byte[] in, String outfile) {
        try {
            //reader for the text file
            BufferedReader txtIn = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(in)));

            //prepare Document for PDF writing task
            Document document = new Document(PageSize.A4);
            PdfWriter.getInstance(document, new FileOutputStream(outfile));
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER);

            //read line by line from the text file and add to the PDF document
            String line = "";
            while (line != null) {
                line = txtIn.readLine();
                document.add(new Paragraph(line, font));
            }

            txtIn.close();
            document.close();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private byte[] fileToBytes(String filename) {
        FileInputStream in = null;
        try {
            in = new FileInputStream(filename);
            int len = (int) new File(filename).length();
            byte[] b = new byte[len];

            int offset = 0;
            int numRead = 0;
            while (offset < b.length && (numRead = in.read(b, offset, b.length - offset)) >= 0) {
                offset += numRead;
            }
            if (offset < b.length) {
                return null;
            }
            return b;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
            }
        }
        return null;
    }


    private void delay(long t) {
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {}
    }


}
