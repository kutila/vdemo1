/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ws;


import java.text.NumberFormat;


/**
 * This class is adapted from the Jama Matrix class. We use Floats instead of
 * double.
 *
 * @author kutila
 * created on 22/06/2010
 */
public class Matrix {

    private float[][] A; //array of contents
    private int m; //no. of rows
    private int n; //no. of columns

    //constructor
    public Matrix(int m, int n) {
        this.m = m;
        this.n = n;
        A = new float[m][n];
    }

    //constructor
    public Matrix(float[][] A) {
        m = A.length;
        n = A[0].length;
        for (int i = 0; i < m; i++) {
            if (A[i].length != n) {
                throw new IllegalArgumentException("All rows must have the same length.");
            }
        }
        this.A = A;
    }


    public float[][] getArray() {
        return A;
    }


    public Matrix multiply(Matrix B) {
        if (B.m != n) {
            throw new IllegalArgumentException("Matrix inner dimensions must agree.");
        }
        Matrix X = new Matrix(m, B.n);
        float[][] C = X.getArray();
        float[] Bcolj = new float[n];
        for (int j = 0; j < B.n; j++) {
            for (int k = 0; k < n; k++) {
                Bcolj[k] = B.A[k][j];
            }
            for (int i = 0; i < m; i++) {
                float[] Arowi = A[i];
                float s = 0;
                for (int k = 0; k < n; k++) {
                    s += Arowi[k] * Bcolj[k];
                }
                C[i][j] = s;
            }
        }
        return X;
    }

    public Matrix normalize() {
        Matrix x = new Matrix(m, n);
        for (int j = 0; j < n; j++) { //columns
            float colSum = 0;
            for (int i = 0; i < m; i++) { //rows
                colSum = colSum + A[i][j];
            }
            for (int i = 0; i < m; i++) { //rows
                x.set(i, j, A[i][j]/colSum);
            }
        }
        return x;
    }

   public void set(int i, int j, float s) {
      A[i][j] = s;
   }

   public float get(int i, int j) {
       return A[i][j];
   }

    public String getStringRepresentation(int w, int d) {
        StringBuilder b = new StringBuilder();
        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(d);
        format.setMinimumFractionDigits(d);
        //format.setMinimumIntegerDigits(2);
        
        for (int i = 0; i < m; i++) { //each row
            String row = "";
            for (int j = 0; j < n; j++) { //columns
                b.append(" ");
                b.append(format.format(A[i][j]));
            }
            b.append("\r\n");
        }
        return b.toString();
    }

    public static void main(String[] args) {
        float[][] aa = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}; //3x3 matrix
        Matrix a = new Matrix(aa);

        a.normalize().getStringRepresentation(4, 3);

//        float[][] bb = {{2}, {2}, {2}}; //1x3 column matrix
//        Matrix b = new Matrix(bb);
//
//        Matrix c = a.multiply(b);
//        a.print(4, 0);
//        b.print(4, 0);
//        c.print(4, 0);
    }
}
