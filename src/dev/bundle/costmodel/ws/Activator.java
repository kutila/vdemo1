/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ws;

import bundle.intelligent.common.CostModelIF;
import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for a Scheme-Selector cost model that
 * selects between web service use and component use
 *
 * @author kutila
 * created on 16/10/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private SchemeSelectorCostModel costModel;
    
    @Override
    public void start(BundleContext context) throws Exception {
        costModel = new SchemeSelectorCostModel(context);
        serviceRegistration =context.registerService(
                CostModelIF.class.getName(),
                costModel, null);
        logger.fine("Service started: " + SchemeSelectorCostModel.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        costModel.requestStop();
        logger.fine("Service stopped: " + SchemeSelectorCostModel.class.getName());
    }

}
