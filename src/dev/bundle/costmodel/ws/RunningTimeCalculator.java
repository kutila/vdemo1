/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ws;

import jade.util.Logger;
import java.io.File;
import java.io.FilenameFilter;
import mma2.capability.CapabilityDetails;
import mma2.capability.Repository;

/**
 * Hacking class to estimate web service execution time.
 * For use in the CSAMS2010 Special Issue Journal paper
 *
 * @author kutila2
 * created on 16/10/2010
 */
public class RunningTimeCalculator {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());


    public long estimateRuntime(float bandwidth, long latency, long msgOverheadSize,
            Repository repository, CapabilityDetails details) {
        long estimate = 0;
        long dataToProcess = 0; //byte value

        long time2ProcessOneKB = details.getEstimatedRunningTime(); //locally or at web service

        //calculate amount of data to process
        String txtFileLocation = (String) repository.get("FILEDIR");
        File dir = new File(txtFileLocation);
        String[] files2Process = dir.list(
                new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.endsWith(".txt");
                    }
                }
        );

        if (files2Process == null || files2Process.length == 0) {
            logger.fine("No files to process");
        } else {
            for (String filename : files2Process) {
                File f = new File(txtFileLocation + "\\" + filename);
                dataToProcess += f.length();
            }
        }
        String id = details.getId();
        logger.fine("### " + id + " ###");
        logger.fine("msg overhead               = " + msgOverheadSize);
        logger.fine("bandwidth                  = " + bandwidth);
        logger.fine("latency                    = " + latency);
        logger.fine("data to process (KB)       = " + (dataToProcess/1024));
        logger.fine("time to process 1 KB       = " + time2ProcessOneKB );
        estimate = (dataToProcess/1024) * time2ProcessOneKB; //add processing time
        logger.fine("process time estimate (ms) = " + estimate );
        if (id.contains("ws")) {
            //add time taken to transfer data to WS
            //multiply it by 2 to include result receiving time
            //assumes result size = data size
            float transferTime = 2 * (latency + (msgOverheadSize + dataToProcess) / bandwidth);
            logger.fine("data transfer time    (ms) = " + transferTime );
            estimate += transferTime;
        }

        logger.fine(id + " estimated execution time = " + estimate);
        return estimate;
    }


}
