/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ahp1;

import bundle.intelligent.common.CostModelIF;
import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the AHP cost model
 *
 * @author kutila
 * created on 14/06/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private AhpCostModel costModel;
    
    @Override
    public void start(BundleContext context) throws Exception {
        costModel = new AhpCostModel(context);
        serviceRegistration =context.registerService(
                CostModelIF.class.getName(),
                costModel, null);
        logger.fine("Service started: " + AhpCostModel.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        costModel.requestStop();
        logger.fine("Service stopped: " + AhpCostModel.class.getName());
    }

}
