/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ahp1;

import bundle.intelligent.common.AbsoluteConstraint;
import bundle.intelligent.common.CostValue.CRITERION;
import bundle.intelligent.common.ConstraintIF;
import bundle.intelligent.common.CostModelIF;
import bundle.intelligent.common.CostValue;
import bundle.intelligent.common.Endpoint;
import bundle.intelligent.common.Group;
import bundle.intelligent.common.Match;
import bundle.intelligent.common.RelativeConstraint;

import jade.util.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;

import mma2.capability.CapabilityDetails;
import mma2.util.Utility;

import org.osgi.framework.BundleContext;

/**
 * This cost model implementation uses an AHP based cost aggregation method.
 *
 * @author kutila
 * created on 14/06/2010
 */
public class AhpCostModel implements CostModelIF {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    //private HashMap<String, CostValue> estimatesCache = new HashMap<String, CostValue>();
    private HashMap paramMap = new HashMap();

    private int requestSize = 0;
    private int msgOverheadSize = 0;
    private long time2Start = 0;
    private long latency = 0;
    private float bandwidth = 0;
//    private BundleContext context;

    AhpCostModel(BundleContext context) {
    }

     public void setParameters(HashMap params) {
        this.paramMap = params;

        requestSize = (Integer) paramMap.get(REQUEST_SIZE);
        msgOverheadSize = (Integer) paramMap.get(MSG_OVERHEAD_SIZE);
        time2Start = (Long) paramMap.get(TIME_TO_START);
     }

 
    /**
     * identify relevant cost criteria from constraints
     * evaluate cost estimates for the relevant alternatives
     *  - capability wise estimates aggregated to get groupwise values
     * discard alternatives which fail to meet absolute constraints
     * sort remaining alternatives in increasing order of cost
     *
     * @param groups
     * @param constraints
     * @return
     */
    public Vector sort(Vector<Group> groups, Vector<ConstraintIF> constraints) {
        logger.fine("<<<<<<<<<<<<<<<<<<<<<<<<<Sort started>>>>>>>>>>>>>>>>>>>" +
                "\r\n  No. of groups = " + groups.size() +
                "\r\n  No. of consts = " + constraints.size());

        HashMap<String, CostValue> estimatesCache = new HashMap<String, CostValue>();

        // separate absolute and relative constraints
        ArrayList<ConstraintIF> absoluteCons = new ArrayList<ConstraintIF>();
        ArrayList<ConstraintIF> relativeCons = new ArrayList<ConstraintIF>();
        for (Iterator<ConstraintIF> it = constraints.iterator(); it.hasNext();) {
            ConstraintIF constraint = it.next();
            if (constraint.getType() == ConstraintIF.TYPE.ABSOLUTE) {
                absoluteCons.add(constraint);
            } else {
                relativeCons.add(constraint);
            }
        }
        logger.fine(absoluteCons.size() + " Absolute constraints = " + absoluteCons.toString());
        logger.fine(relativeCons.size() + " Relative constraints = " + relativeCons.toString());

        // evaluate the absolute constraints
        for (ConstraintIF constraint : absoluteCons) {
            AbsoluteConstraint absoluteConst = (AbsoluteConstraint) constraint;
            Iterator<Group> it = groups.iterator();
            while (it.hasNext()) {
                Group group = it.next();
                CostValue c = estimateGroupCost(group, absoluteConst.getCriterion1());
                Utility.log("Group cost is " + c.toString());
                if (absoluteConst.satisfy(c) == false) {
                    logger.finest("Group FAILED to meet absolute constraint");
                    it.remove();
                } else {
                    estimatesCache.put(group.getName() + absoluteConst.getCriterion1(), c);
                }
            }
        }
        if (groups.size() == 0) {
            return null; //failure, all failed the absolute constraints
        } else if (groups.size() == 1) {
           return groups; //only 1 group remaining, no need of sorting
        }

        //4) identify cost elements included in relative constraints
        Set<CRITERION> criteria = null;
        RelativeConstraint relativeConst = null;
        for (ConstraintIF constraint : relativeCons) {
            relativeConst = (RelativeConstraint) constraint;
            criteria = relativeConst.getCriteria();
            break;//there is only 1 Relative Constraint (may change later)
        }
        if (relativeConst == null) {
            return groups; //no sorting criteria, return as is
        }
        CRITERION[] criteriaArray = criteria.toArray(new CRITERION[1]);
        int noOfGroups = groups.size(); //m - rows
        int noOfCriteria = criteriaArray.length; //n - columns

        //5) build normalized cost criterion preference matrix
        Matrix preferenceMatrix = new Matrix(noOfCriteria, 1);
        for (int j = 0; j < noOfCriteria; j++) {
            preferenceMatrix.set(j, 0, relativeConst.getPriority(criteriaArray[j]));
        }
        preferenceMatrix = preferenceMatrix.normalize();

        //6) estimate costs for relevant cost elements
        Group[] grpArray = groups.toArray(new Group[1]);
        for (int i = 0; i < noOfGroups; i++) {
            for (int j = 0; j < noOfCriteria; j++) {
                String key = grpArray[i].getName() + criteriaArray[j];
                if (estimatesCache.get(key) == null) {
                    estimatesCache.put(
                            key,
                            estimateGroupCost(grpArray[i], criteriaArray[j])
                            );
                }
            }
        }
        logger.fine(" --- Estimated values --- \r\n" + estimatesCache.toString() + "\r\n");

        //7) Build utility matrix from cost estimates [Group x Criterion]
        Matrix uMatrix = new Matrix(noOfGroups, noOfCriteria);
        for (int i = 0; i < noOfGroups; i++) {
            Group g = grpArray[i];
            for (int j = 0; j < noOfCriteria; j++) {
                String key = g.getName() + criteriaArray[j];
                CostValue cv = estimatesCache.get(key);
                if (cv != null) {
                    float utilityValue;
                    if (criteriaArray[j] == CRITERION.ACCURACY) {
                        //accuracy value is a direct utility indicator
                        utilityValue = (new Float(cv.getValue().toString()));
                    } else {
                        //inverse cost value to get a utility indicator
                        utilityValue = 1 / (new Float(cv.getValue().toString()));
                    }

                    uMatrix.set(i, j, utilityValue);
                } else {
                    logger.fine(key + "CostValue[" + i + "," + j + "] NULL");
                }
            }
        }
        uMatrix = uMatrix.normalize();

        StringBuilder b = new StringBuilder();
        b.append("Preference Matrix\r\n");
        b.append(preferenceMatrix.getStringRepresentation(3, 3));

        b.append("Utility Matrix\r\n");
        b.append(uMatrix.getStringRepresentation(3, 3));
        Utility.log(b.toString());

        //7) use AHP to calculate utilitiy of each group
        Matrix resultMatrix = uMatrix.multiply(preferenceMatrix);
        Float[] resultsArray = new Float[noOfGroups];
        for (int i = 0; i < noOfGroups; i++) {
//            System.out.println(grpArray[i].getName() + " = " +resultMatrix.get(i, 0));
            resultsArray[i] = resultMatrix.get(i, 0);
        }

        AhpHelper.sortDescending(resultsArray, grpArray);

        Vector<Group> sortedGroups = new Vector<Group>();
        b = new StringBuilder("After sorting: \r\n");
        for (int i = 0; i < noOfGroups; i++) {
            b.append(grpArray[i].getName());
            b.append(" = ");
            b.append(resultsArray[i]);
            b.append("\r\n");
            sortedGroups.add(grpArray[i]);
        }
        logger.fine(b.toString());
        
        logger.fine("<<<<<<<<<<<<<<<<<<<<<<<<<Sort done>>>>>>>>>>>>>>>>>>>");
        return sortedGroups;
    }

    void requestStop() {
        //nothing to do here
    }

    /**
     * Calculates the Group cost estimate for the specified criterion.
     * See Chapter 4.2 for formulae used in estimates.
     *
     * @param group
     * @param criterion
     * @return
     */
    private CostValue estimateGroupCost(Group group, CRITERION criterion) {
//        logger.fine("Estimate " + criterion + " cost for " + group.toString());
        StringBuilder b = new StringBuilder("AhpCostModel: ");
        b.append(group.getName());
        b.append("\r\n");
        Match[] matchArray = group.getContents();
        CostValue costValue = new CostValue();
        costValue.setCriterion(criterion);
        switch (criterion) {
            case TIME:
                int n = matchArray.length;
                float totalTime = 0;
                for (int i = 0; i < n; i++) {
                    CapabilityDetails details = matchArray[i].details;
                    b.append("  ");
                    b.append(details.getId());
                    if (matchArray[i].provider instanceof String &&
                            matchArray[i].provider.equals(Match.LOCAL)) {
                        //0 additional time to acquire
                        b.append(" is local. ");
                    } else {
                        try {
                            String host = ((Endpoint) matchArray[i].provider).getAddress();
                            bandwidth = (Float) paramMap.get("BANDWIDTH:" + host);
                            latency = (Long) paramMap.get("LATENCY:" + host);
                            b.append(" Latency=");
                            b.append(latency);
                            b.append(" Bandwidth=");
                            b.append(bandwidth);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, "error reading network params", e);
                        }
                        //Add time to acquire capability (T_cap_req + T_cap_res)
//                        logger.fine("     %% total = " + totalTime + " + 2*" + latency +
//                                " + (" + requestSize + " + 2*" + msgOverheadSize + " + " +
//                                details.getSize() + ")/" + bandwidth);
                        b.append(" B_cap = " + details.getSize());
                        totalTime = totalTime + 2*latency +
                                ((requestSize + 2*msgOverheadSize + details.getSize())/bandwidth);
                    }
                    //Add time to start (T^start_p and execute (Tp^loc)
                    totalTime += details.getEstimatedStartingTime();
                    totalTime += details.getEstimatedRunningTime();
                    b.append(" T^start_p (ms) = " + details.getEstimatedStartingTime());
                    b.append(" Tp^run (ms) = " + details.getEstimatedRunningTime());
                }
                b.append(" Estimated time = " + totalTime);
                costValue.setValue(new Float(totalTime));
                break;

            case LOAD:
                int total = 0;
                for (int i = 0; i < matchArray.length; i++) {
                    CapabilityDetails details = matchArray[i].details;
                    b.append(details.getId());
                    if (matchArray[i].provider instanceof String &&
                            matchArray[i].provider.equals(Match.LOCAL)) {
                        //0 load due to acquisition
                        b.append(" is local. ");
                    } else {
                        //B_creq + 2B_ovrhd + B_cap
//                        logger.fine("     %% total = " + total + " + " + requestSize +
//                                " + 2*" + msgOverheadSize + " + " +
//                                details.getSize());
                        b.append(" B_cap = " + details.getSize());
                        total += requestSize + 2*msgOverheadSize + details.getSize();
                    }
//                    logger.fine("     %% total = " + total + " + " +
//                            + details.getRunningNwLoad());
                    total += details.getRunningNwLoad(); //B_exec
                    b.append(" B_exec = " + details.getRunningNwLoad());
                }
                b.append(" Estimated load = " + total);
                costValue.setValue(new Integer(total));
                break;

            case HEAP:
                int max = 0;
                int current = 0;
                for (int i = 0; i < matchArray.length; i++) {
                    current = matchArray[i].details.getHeapSizeRating();
                    if (current > max) {
                        max = current;
                    }
                }
                costValue.setValue(new Integer(max));
                break;

            case CPU:
                max = 0;
                current = 0;
                for (int i = 0; i < matchArray.length; i++) {
                    current = matchArray[i].details.getCpuRating();
                    if (current > max) {
                        max = current;
                    }
                }
                costValue.setValue(new Integer(max));
                break;

            case ACCURACY:
                int min = 10;
                current = 10;
                for (int i = 0; i < matchArray.length; i++) {
                    current = matchArray[i].details.getAccuracyRating();
                    if (current < min) {
                        min = current;
                    }
                }
                costValue.setValue(new Integer(min));
                break;

            default:
                logger.warning(criterion + " is not a suppported cost criterion.");
        }
        Utility.log(b.toString());
        costValue.setTimestamp(System.currentTimeMillis());
        return costValue;
    }

    private String printGroup(Match[] group) {
        StringBuilder b = new StringBuilder("(");
        for (Match match : group) {
            b.append(match.details.getId());
            b.append(',');
        }
        b.append(')');
        return b.toString();
    }

}
