/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ahp1;

import bundle.intelligent.common.CostValue;
import bundle.intelligent.common.CostValue.CRITERION;
import java.util.HashMap;
import mma2.capability.CapabilityDetails;

/**
 * Simple class to hold the cost estimates for a Capability/Group.
 *
 * @author kutila
 * created on 17/06/2010
 */
public class CostEstimateContainer {

    private HashMap<CostValue.CRITERION, CostValue> estimates = new HashMap<CostValue.CRITERION, CostValue>();

    protected CapabilityDetails capability;


    public CostValue getEstimate(CRITERION criterion) {
        return estimates.get(criterion);
    }

    public void setEstimate(CRITERION criterion, CostValue value) {
        estimates.put(criterion, value);
    }

}
