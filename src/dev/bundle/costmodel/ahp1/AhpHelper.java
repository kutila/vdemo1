/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.ahp1;

/**
 *
 * @author kutila2
 * created on 23/06/2010
 */
public class AhpHelper {

    
    /**
     * Sorts the given array a in ascending order. At the same time
     * the second array elements are also swapped in the same order such that
     * the corresponding elements of the two arrays for a given index are not
     * changed.
     *
     * Insertion sort algorithm is used for sorting.
     *
     * @param a The array whose elements are to be sorted
     * @param b The corresponding non-comparable array to be rearranged
     */
    public static void sortDescending(Comparable[] a, Object[] b) {
        for (int p = 1; p < a.length; p++) {
            Comparable tmp = a[p];
            Object bTmp = b[p];
            int j = p;

            for (; j > 0 && tmp.compareTo(a[j - 1]) > 0; j--) {
                a[j] = a[j - 1];
                b[j] = b[j - 1];
            }
            a[j] = tmp;
            b[j] = bTmp;
        }
    }
}
