/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.dummy;

import bundle.intelligent.common.ConstraintIF;
import bundle.intelligent.common.CostModelIF;
import bundle.intelligent.common.Group;
import jade.util.Logger;
import java.util.HashMap;
import java.util.Vector;
import org.osgi.framework.BundleContext;

/**
 * This cost model implementation does nothing. It simply returns the given
 * alternatives in the same order.
 *
 * @author kutila
 * created on 13/06/2010
 */
public class NoSortCostModel implements CostModelIF {

    private Logger logger = Logger.getMyLogger(this.getClass().getName());

    NoSortCostModel(BundleContext context) {
        //nothing to do here
    }


    public Vector sort(Vector<Group> alternatives, Vector<ConstraintIF> constraints) {
        logger.fine("<<<<<<<<<<<<<<<SORTING.... :-) >>>>>>>>>>>>>>>>");
        return alternatives;
    }

     public void setParameters(HashMap params) {
         
     }


    void requestStop() {
        //nothing to do here
    }

}
