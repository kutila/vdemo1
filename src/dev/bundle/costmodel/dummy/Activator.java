/*
 * VERSAG project
 * DSSE Centre, Monash University, Australia
 */
package bundle.costmodel.dummy;

import bundle.intelligent.common.CostModelIF;
import jade.util.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Bundle activator for the Intelligent Capability Selector
 *
 * @author kutila
 * created on 03/06/2010
 */
public class Activator implements BundleActivator  {
    
    private Logger logger = Logger.getMyLogger(this.getClass().getName());
    
    private ServiceRegistration serviceRegistration;
    private NoSortCostModel costModel;
    
    @Override
    public void start(BundleContext context) throws Exception {
        costModel = new NoSortCostModel(context);
        serviceRegistration =context.registerService(
                CostModelIF.class.getName(),
                costModel, null);
        logger.fine("Service started: " + NoSortCostModel.class.getName());
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
        costModel.requestStop();
        logger.fine("Service stopped: " + NoSortCostModel.class.getName());
    }

}
