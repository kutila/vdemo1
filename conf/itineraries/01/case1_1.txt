#
# Create and move Working agents
#
create W1 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W1
itinerary
loc1=move loc3
loc3=start contextapi

create W2 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W2
itinerary
loc1=move loc4
loc4=start contextapi

create W3 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W3
itinerary
loc1=move loc5
loc5=start contextapi

create W4 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W4
itinerary
loc1=move loc6
loc6=start contextapi

create W5 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W5
itinerary
loc1=move loc7
loc7=start contextapi

create W6 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W6
itinerary
loc1=move loc8
loc8=start contextapi

create W7 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W7
itinerary
loc1=move loc9
loc9=start contextapi

create W8 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W8
itinerary
loc1=move loc10
loc10=start contextapi

create W9 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W9
itinerary
loc1=move loc11
loc11=start contextapi

create W10 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W10
itinerary
loc1=move loc12
loc12=start contextapi

create W11 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W11
itinerary
loc1=move loc13
loc13=start contextapi

create W12 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W12
itinerary
loc1=move loc14
loc14=start contextapi

create W13 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W13
itinerary
loc1=move loc15
loc15=start contextapi

create W14 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W14
itinerary
loc1=move loc16
loc16=start contextapi

create W15 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W15
itinerary
loc1=move loc17
loc17=start contextapi

create W16 mma2.jade.JadeAgent loc1 agent1.properties
pause 5
to:W16
itinerary
loc1=move loc18
loc18=start contextapi

pause 10
#
# Ask each agent to start working
#
to:W1
itinerary
loc3=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W2
itinerary
loc4=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W3
itinerary
loc5=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W4
itinerary
loc6=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W5
itinerary
loc7=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W6
itinerary
loc8=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W7
itinerary
loc9=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W8
itinerary
loc10=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W9
itinerary
loc11=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W10
itinerary
loc12=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W11
itinerary
loc13=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W12
itinerary
loc14=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W13
itinerary
loc15=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W14
itinerary
loc16=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W15
itinerary
loc17=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA

to:W16
itinerary
loc18=start icommon#start irequester#start ahpcostmodel#find spec const;TIME:5 ALPHA;a_gui;JADE3_7#start iselector#start ALPHA
