echo off
REM ***************************************************************************
REM Windows batch file to start JADE peripheral container
REM ***************************************************************************

set VERSAG_HOME=.
set LIB=%VERSAG_HOME%\lib

REM add libraries to classpath
set CLASSPATH=%LIB%\JadeLeap.jar;%LIB%\logformatter.jar;%VERSAG_HOME%\conf

REM auto-generated JAXB classes for txt2pdfws capability (2009-10-10)
REM set CLASSPATH=%CLASSPATH%;%LIB%\txt2pdfwsclient.jar

java -Djava.util.logging.config.file=conf\jadelog.properties jade.Boot -host @mainhost@ -container -container-name loc@index@
