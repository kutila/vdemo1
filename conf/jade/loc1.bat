echo off
REM ***************************************************************************
REM Windows batch file to start JADE test container
REM 
REM Tests for AAMAS 2010
REM
REM jade.log 	- prints log messages from JADE (uses Java Logging API)
REM 
REM Last updated 2009-10-01 by Kutila
REM ***************************************************************************

set VERSAG_HOME=.
set LIB=%VERSAG_HOME%\lib

REM add JADE libraries to classpath
set CLASSPATH=%LIB%\JadeLeap.jar

REM add auto-generated JAXB classes for use in txt2pdfws capability (2009-10-10)
set CLASSPATH=%CLASSPATH%;%LIB%\txt2pdfwsclient.jar

REM add other libraries and folders to classpath
set CLASSPATH=%CLASSPATH%;%LIB%\concierge-1.0.0.RC3.jar;%LIB%\versag.jar;%LIB%\logformatter.jar;%VERSAG_HOME%\conf

REM add "-Xms128m -Xmx512m" immediately after 'java' to increase memory allocation
java -Djava.util.logging.config.file=conf\jadelog.properties -Dmma.home=. jade.Boot -gui -container-name loc1

REM NB Profiling details included
REM java -agentpath:"D:\dev\NetBeans 6.7.1\profiler3\lib\deployed\jdk15\windows\profilerinterface.dll=\"D:\dev\NetBeans 6.7.1\profiler3\lib\"",5140 -Djava.util.logging.config.file=conf\jadelog.properties -Dmma.home=. jade.Boot -container-name loc1 BOB:mma2.jade.JadeAgent(agent1.properties)
