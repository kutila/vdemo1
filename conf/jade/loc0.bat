echo off
REM ***************************************************************************
REM  Windows batch file to start JADE peripheral container
REM  with Messaging Agent
REM ***************************************************************************

set VERSAG_HOME=.
set LIB=%VERSAG_HOME%\lib

REM add libraries to classpath
set CLASSPATH=%LIB%\JadeLeap.jar;%LIB%\tools.jar;%VERSAG_HOME%\conf

REM -host 130.194.70.183 rma:jade.tools.rma.rma
java -Djava.util.logging.config.file=conf\jadelog.properties jade.Boot -container -container-name loc0 Controller:tools.ca.ControllerAgent
