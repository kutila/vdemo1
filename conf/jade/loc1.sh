#/bin/bash
# ***************************************************************************
# Linux script to start JADE main container
#
# ***************************************************************************

VERSAG_HOME=.
LIB=$VERSAG_HOME/lib

# add libraries to classpath
CLASSPATH=$LIB/JadeLeap.jar:$LIB/concierge-1.0.0.RC3.jar:$LIB/versag.jar:$LIB/logformatter.jar:$VERSAG_HOME/conf:

# auto-generated JAXB classes for txt2pdfws capability (2009-10-10)
#CLASSPATH=$CLASSPATH:$LIB/txt2pdfwsclient.jar

java -cp $CLASSPATH -Djava.util.logging.config.file=conf/jadelog.properties jade.Boot -container-name loc1
